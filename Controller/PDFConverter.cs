﻿using TextParserModule.Models;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.Models;

namespace TextParserModule.Controller
{
    public class PDFConverter
    {
        public Globals Globals { get; private set; }
        public async Task<ResultItem<List<ConvertPdfToTextResult>>> ConvertPDFFileToText(ConvertPdfToTextRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<ConvertPdfToTextResult>>>(async() =>
           {

               var result = new ResultItem<List<ConvertPdfToTextResult>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<ConvertPdfToTextResult>()
               };

               var elasticAgent = new Services.ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo($"Get Model...");
               var modelResult = await elasticAgent.GetConvertPdfToTextModel(request);

               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have Model.");

               var usePaths = modelResult.Result.FolderConvert != null ? modelResult.Result.FolderConvert.Val_Bool.Value : false;

               var fileMappings = new List<ConvertPdfFileMapping>();

               request.MessageOutput?.OutputInfo($"Get File-Mappings...");
               if (usePaths)
               {
                   try
                   {
                       var files = System.IO.Directory.GetFiles(modelResult.Result.ConfigToSrcPath.Name_Other, "*.pdf");

                       fileMappings = files.Select(file =>
                       {
                           var mapping = new ConvertPdfFileMapping();

                           mapping.SrcFileName = file;

                           var fileName = System.IO.Path.GetFileName(file);
                           var fileExtension = System.IO.Path.GetExtension(file);
                           var dstFile = fileName.Replace(fileExtension, ".txt");
                           dstFile = System.IO.Path.Combine(modelResult.Result.ConfigToDstPath.Name_Other, dstFile);
                           mapping.DstFileName = dstFile;

                           return mapping;
                       }).ToList();
                   }
                   catch (Exception ex)
                   {

                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = ex.Message;
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   }
               }
               else
               {
                   fileMappings.Add(new ConvertPdfFileMapping
                   {
                       SrcFileName = modelResult.Result.ConfigToSrcPath.Name_Other,
                       DstFileName = modelResult.Result.ConfigToDstPath.Name_Other
                   });
               }
               request.MessageOutput?.OutputInfo($"Have {fileMappings.Count} File-Mappings.");

               foreach (var fileMapping in fileMappings)
               {
                   var resultItem = new ConvertPdfToTextResult
                   {
                       FileMapping = fileMapping,
                       Result = Globals.LState_Success.Clone()
                   };

                   result.Result.Add(resultItem);

                   try
                   {
                       var pdfReader = new PdfReader(fileMapping.SrcFileName);


                       request.MessageOutput?.OutputInfo($"Start parsing...");
                       using (var textWriter = new StreamWriter(fileMapping.DstFileName))
                           for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                           {
                               request.MessageOutput?.OutputInfo($"Output page {i}");

                               var text = PdfTextExtractor.GetTextFromPage(pdfReader, i);
                               textWriter.Write(text);
                           }

                       request.MessageOutput?.OutputInfo($"Finished parsing");
                   }
                   catch (Exception ex)
                   {
                       resultItem.Result = Globals.LState_Error.Clone();
                       resultItem.Result.Additional1 = ex.Message;
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   }
               }

               return result;
           });

            return taskResult;
        }

        public PDFConverter(Globals globals)
        {
            Globals = globals;
        }
    }
}
