﻿using ElasticSearchNestConnector;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;
using TextParserModule.Services;
using TextParserModule.Validation;

namespace TextParserModule.Controller
{
    public class PDFIndexer
    {
        public Globals Globals
        {
            get;
            private set;
        }

        public async Task<ResultItem<IndexPDFResult>> IndexPDFfiles(IndexPDFRequest request)
        {
            var taskResult = await Task.Run<ResultItem<IndexPDFResult>>(async () =>
            {
                var result = new ResultItem<IndexPDFResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new IndexPDFResult()
                };

                result.ResultState = ValidationController.ValidateIndexPDFRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var elasticAgent = new ServiceAgentElastic(Globals);

                if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Cancelled by User!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get Model...");
                var getModelResult = await elasticAgent.GetIndexPDFModel(request);
                request.MessageOutput?.OutputInfo("Have Model...");


                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Cancelled by User!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (getModelResult.Result.ConfigToPath != null)
                {

                }

                var pdfsToIndexPre = (from pdfFile in getModelResult.Result.PDFDocuments
                            join indexedFile in getModelResult.Result.SessionPDFs on pdfFile.IdMultimediaItem equals indexedFile.ID_Other into indexedFiles
                            from indexedFile in indexedFiles.DefaultIfEmpty()
                            where indexedFile == null
                            select pdfFile).ToList();

                var pdfsToIndex = (from pdfFile in pdfsToIndexPre
                                   join indexedFileNotExisting in getModelResult.Result.SessionPDFsNotExisting on pdfFile.IdMultimediaItem equals indexedFileNotExisting.ID_Other into notExistings
                                   from indexedFileNotExisting in notExistings.DefaultIfEmpty()
                                   where indexedFileNotExisting == null
                                   select pdfFile).ToList();

                pdfsToIndex = (from pdfFile in pdfsToIndex
                               join indexedFileError in getModelResult.Result.SessionPDFsError on pdfFile.IdMultimediaItem equals indexedFileError.ID_Other into indexedFileErrors
                               from indexedFileError in indexedFileErrors.DefaultIfEmpty()
                               where indexedFileError == null
                               select pdfFile).ToList();

                if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "Cancelled by User!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var indexCount = pdfsToIndex.Count();
                var checkedCount = 0;
                var savedCount = 0;
                request.MessageOutput?.OutputInfo($"Documents to index: {indexCount}");

                var stampStart = DateTime.Now;
                var relationConfig = new clsRelationConfig(Globals);

                var sessionItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = stampStart.ToString("yyyy-MM-dd hh:mm:ss"),
                    GUID_Parent = IndexPdf.Config.LocalData.ClassAtt_Index_Session__Index_PDF_Files__Datetimestamp__Create_.ID_Class,
                    Type = Globals.Type_Object
                };
                var objectsToSave = new List<clsOntologyItem>
               {
                   sessionItem
               };

                var attributesToSave = new List<clsObjectAtt>();

                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(sessionItem, IndexPdf.Config.LocalData.AttributeType_Datetimestamp__Create_, stampStart));

                request.MessageOutput?.OutputInfo($"Save Session-Item: {sessionItem.Name}");
                result.ResultState = await elasticAgent.SaveObjects(objectsToSave);


                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the objects!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Save create stamp");
                result.ResultState = await elasticAgent.SaveAttributes(attributesToSave);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the attributes!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var relationsToSave = new List<clsObjectRel>();
                var relationsNotExisting = new List<clsObjectRel>();
                var relationsError = new List<clsObjectRel>();

                var dbReader = new clsUserAppDBSelector(getModelResult.Result.ElasticSearchConfig.NameServer, getModelResult.Result.ElasticSearchConfig.Port, getModelResult.Result.ElasticSearchConfig.NameIndex, 5000, Globals.Session);
                var dbWriter = new clsUserAppDBUpdater(dbReader);
                var documents = new List<clsAppDocuments>();

                foreach (var pdfFile in pdfsToIndex)
                {
                    if (request.CancellationToken != null && request.CancellationToken.IsCancellationRequested)
                    {
                        result.ResultState.Additional1 = "Cancelled by User!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    checkedCount++;
                    var resultItem = new ResultItem<clsOntologyItem>
                    {
                        ResultState = Globals.LState_Success.Clone(),
                        Result = new clsOntologyItem
                        {
                            GUID = pdfFile.IdMultimediaItem,
                            Name = pdfFile.NameMultimediaItem,
                            GUID_Parent = IndexPdf.Config.LocalData.Class_PDF_Documents.GUID,
                            Type = Globals.Type_Object
                        }
                    };

                    result.Result.IndexedResult.Add(resultItem);

                    if (!System.IO.File.Exists(pdfFile.FileUrl))
                    {
                        resultItem.ResultState = Globals.LState_Error.Clone();
                        resultItem.ResultState.Additional1 = $"The File {pdfFile.FileUrl} does not exist!";
                        relationsNotExisting.Add(relationConfig.Rel_ObjectRelation(sessionItem, new clsOntologyItem
                        {
                            GUID = pdfFile.IdMultimediaItem,
                            Name = pdfFile.NameMultimediaItem,
                            GUID_Parent = IndexPdf.Config.LocalData.Class_PDF_Documents.GUID,
                            Type = Globals.Type_Object
                        }, IndexPdf.Config.LocalData.RelationType_not_existing));
                        continue;
                    }

                    try
                    {
                        

                        var pdfReader = new PdfReader(pdfFile.FileUrl);


                        for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                        {

                            var document = new clsAppDocuments
                            {
                                Id = $"{i}_{pdfFile.IdMultimediaItem}",
                                Dict = new Dictionary<string, object>()
                            };
                            documents.Add(document);
                            var text = PdfTextExtractor.GetTextFromPage(pdfReader, i);
                            document.Dict.Add(PDFIndex.FieldId, pdfFile.IdMultimediaItem);
                            document.Dict.Add(PDFIndex.FieldPage, i);
                            document.Dict.Add(PDFIndex.FieldContent, text);
                            
                        }
                        relationsToSave.Add(relationConfig.Rel_ObjectRelation(sessionItem, new clsOntologyItem
                        {
                            GUID = pdfFile.IdMultimediaItem,
                            Name = pdfFile.NameMultimediaItem,
                            GUID_Parent = IndexPdf.Config.LocalData.Class_PDF_Documents.GUID,
                            Type = Globals.Type_Object
                        }, IndexPdf.Config.LocalData.RelationType_contains));

                        if (documents.Count >= 500)
                        {
                            savedCount += documents.Count;
                            
                            result.ResultState = dbWriter.SaveDoc(documents, getModelResult.Result.ElasticSearchConfig.NameType);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error saving documents!";
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                            documents.Clear();

                            result.ResultState = await elasticAgent.SaveRelations(relationsToSave);
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while saving the relations!";
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }

                            relationsToSave.Clear();
                            request.MessageOutput?.OutputInfo($"Checked {checkedCount}/{indexCount}, Saved { savedCount}");
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        resultItem.Result = Globals.LState_Error.Clone();
                        resultItem.Result.Additional1 = ex.Message;
                        relationsError.Add(relationConfig.Rel_ObjectRelation(sessionItem, new clsOntologyItem
                        {
                            GUID = pdfFile.IdMultimediaItem,
                            Name = pdfFile.NameMultimediaItem,
                            GUID_Parent = IndexPdf.Config.LocalData.Class_PDF_Documents.GUID,
                            Type = Globals.Type_Object
                        }, IndexPdf.Config.LocalData.RelationType_error));
                    }
                }

                if (documents.Any())
                {
                    savedCount += documents.Count;
                    result.ResultState = dbWriter.SaveDoc(documents, getModelResult.Result.ElasticSearchConfig.NameType);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error saving documents!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    documents.Clear();
                    request.MessageOutput?.OutputInfo($"Checked {checkedCount}/{indexCount}, Saved { savedCount}");
                }

                if (relationsToSave.Any())
                {
                    request.MessageOutput?.OutputInfo($"Save last {relationsToSave.Count} relations.");
                    result.ResultState = await elasticAgent.SaveRelations(relationsToSave);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the relations!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    relationsToSave.Clear();
                }

                if (relationsNotExisting.Any())
                {
                    request.MessageOutput?.OutputInfo($"Save last {relationsNotExisting.Count} not existing.");
                    result.ResultState = await elasticAgent.SaveRelations(relationsNotExisting);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the not existing documents!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    relationsNotExisting.Clear();
                }

                if (relationsError.Any())
                {
                    request.MessageOutput?.OutputInfo($"Save last {relationsError.Count} errors.");
                    result.ResultState = await elasticAgent.SaveRelations(relationsError);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the error-documents!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    relationsError.Clear();
                }

                var stampFinished = DateTime.Now;

                attributesToSave = new List<clsObjectAtt>();
                request.MessageOutput?.OutputInfo($"Save finishing: {stampFinished}.");
                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(sessionItem, IndexPdf.Config.LocalData.AttributeType_Finished, stampFinished));

                result.ResultState = await elasticAgent.SaveAttributes(attributesToSave);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving the finishing stamp!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public PDFIndexer(Globals globals)
        {
            Globals = globals;
        }
    }
}
