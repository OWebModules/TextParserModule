﻿using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using TextParserModule.Models;
using TextParserModule.Relate.Config;
using TextParserModule.Services;
using System.Text.RegularExpressions;

namespace TextParserModule.Controller
{
    public class NameTransformController : INameTransform
    {
        public Globals Globals { get; private set; }

        public bool IsReferenceCompatible => false;

        public bool IsAspNetProject { get;  set; }

        public bool IsResponsible(string idClass)
        {
            return idClass == Relate.Config.LocalData.Class_Textparser_Details.GUID;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<System.Collections.Generic.List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = items
                };

                messageOutput?.OutputInfo("Check Class-Ids...");
                var idClasses = new System.Collections.Generic.List<string>();
                if (string.IsNullOrEmpty(idClass))
                {
                    idClasses = items.GroupBy(itm => itm.GUID_Parent).Select(grp => grp.Key).ToList();
                }
                else
                {
                    idClasses.Add(idClass);
                }

                var idClassesNotSupported = idClasses.Where(cls => cls != LocalData.Class_Textparser_Details.GUID).ToList();
                if (idClassesNotSupported.Count > 0)
                {
                    messageOutput?.OutputInfo($"There are {idClassesNotSupported.Count} invalid classes!");
                    return result;
                }
                messageOutput?.OutputInfo("Checked Class-Ids.");

                var elasticAgent = new ServiceAgentElastic(Globals);

                messageOutput?.OutputInfo("Get details...");
                var detailsResult = await elasticAgent.GetTextParserDetails(items);

                result.ResultState = detailsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                messageOutput?.OutputInfo("Have details.");
                foreach (var item in items)
                {
                    var name = (from textP in detailsResult.Result.TextParserDetailsToTextParsers.Where(textPars => textPars.ID_Object == item.GUID)
                                    join cfgToTextP in detailsResult.Result.ConfigToTextparser on textP.ID_Other equals cfgToTextP.ID_Other
                                    join cfgToTempl in detailsResult.Result.ConfigsToNameTemplate on cfgToTextP.ID_Object equals cfgToTempl.ID_Object
                                    select cfgToTempl.Val_String).FirstOrDefault();
                    var attributeFields = (from attr in detailsResult.Result.TextParserDetailsAttributes.Where(att => att.ID_Object == item.GUID).OrderBy(attr => attr.OrderID)
                                           join field in detailsResult.Result.TextParserDetailsToTextParserFields on new { attr.ID_Object, attr.OrderID } equals new { field.ID_Object, field.OrderID }
                                           select new { field, attr }).ToList();

                    if (name == null)
                    {
                        item.Name = string.Join(";", attributeFields.Select(attr => attr.attr.Val_Name));
                    }
                    else
                    {
                        foreach (var attrField in attributeFields)
                        {
                            name = name.Replace("@" + attrField.field.Name_Other + "@", attrField.attr.Val_Name);
                        }
                        if (encodeName)
                        {
                            item.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(name)); ;
                        }
                        else
                        {
                            item.Name = name;
                        }
                        
                    }
                    
                }

                return result;
            });

            return taskResult;
        }

        public NameTransformController(Globals globals)
        {
            Globals = globals;
        }
    }
}
