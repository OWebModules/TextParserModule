﻿using ElasticSearchConfigModule;
using ElasticSearchNestConnector;
using FileResourceModule;
using Nest;
using DataAccess;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextParserModule.Factories;
using TextParserModule.Models;
using TextParserModule.Services;
using System.Text.RegularExpressions;
using ImportExport_Module;
using ImportExport_Module.Models;
using System.Globalization;
using TextParserModule.Validation;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using OntoWebCore.Models;
using TextParserModule.Helper;

namespace TextParserModule
{
    public delegate clsOntologyItem SavedMeasureDocsHandler(List<clsAppDocuments> documents, object sender);
    public class TextParserController : AppController
    {

        public event SavedMeasureDocsHandler SavedMeasureDocs;

        private object sender;

        public clsOntologyItem ClassTextParser
        {
            get { return Config.LocalData.Class_Textparser; }
        }

        public async Task<ResultItem<List<TextParser>>> GetTextParser(clsOntologyItem parser = null)
        {
            var result = new ResultItem<List<TextParser>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<TextParser>()
            };
            var serviceAgent = new ServiceAgentElastic(Globals);
            var serviceResult = await serviceAgent.GetTextParser(parser);

            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                return result;
            }

            var fileResourceController = new FileResourceController(Globals);

            var resultFileResource = await fileResourceController.GetFileResource(parser, Config.LocalData.RelationType_belonging_Resource, Globals.Direction_LeftRight);
            if (resultFileResource.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                return result;
            }

            var elasticSearchConfigController = new ElasticSearchConfigController(Globals);

            var resultIndex = await elasticSearchConfigController.GetConfig(parser, Config.LocalData.RelationType_located_at,
                Config.LocalData.RelationType_belonging, Globals.Direction_LeftRight);
            if (resultIndex.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                return result;
            }


            var factory = new TextParserFactory();
            return await factory.CreateParserList(serviceResult.Result, resultFileResource.Result, resultIndex.Result, Globals);

        }

        public async Task<ResultItem<List<ParserField>>> GetParserFields(clsOntologyItem parser = null)
        {
            var result = new ResultItem<List<ParserField>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ParserField>()
            };

            var serviceAgent = new ServiceAgentElastic(Globals);
            var serviceResult = await serviceAgent.GetParserFields(parser);
            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                return result;
            }

            var factory = new ParserFieldFactory();
            result = await factory.CreateParserFieldList(serviceResult.Result, Globals);

            return result;
        }

        public async Task<ResultItem<ParseResult>> ExecuteTextParser(ExecuteTextParserRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ParseResult>>(async () =>
            {
                var result = new ResultItem<ParseResult>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                request.MessageOutput?.OutputInfo($"Validate request...");
                result.ResultState = ValidationController.ValidateExecuteTextParserRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Validated request.");

                if (request.TextParser == null || !request.TextParserFields.Any())
                {
                    request.MessageOutput?.OutputInfo($"Get Textparser...");

                    var parserOItem = await GetOItem(request.IdTextParser, Globals.Type_Object);

                    result.ResultState = parserOItem.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var textParserResult = await GetTextParser(parserOItem.Result);

                    result.ResultState = textParserResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Textparser";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    if (textParserResult.Result.Count != 1)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "No Textparser found";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;

                    }

                    request.MessageOutput?.OutputInfo($"Have Textparser: {textParserResult.Result.First().NameParser}");

                    request.MessageOutput?.OutputInfo($"Get Textparserfields");
                    var textParserFieldResult = await GetParserFields(new clsOntologyItem
                    {
                        GUID = textParserResult.Result.First().IdFieldExtractor,
                        Name = textParserResult.Result.First().NameFieldExtractor,
                        GUID_Parent = textParserResult.Result.First().IdClassFieldExtractor,
                        Type = Globals.Type_Object
                    });

                    result.ResultState = textParserFieldResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Fields";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo($"Have Textparserfields: {textParserFieldResult.Result.Count} (Count)");

                    request.TextParser = textParserResult.Result.First();
                    request.TextParserFields = textParserFieldResult.Result;
                }

                if (request.DeleteIndex)
                {
                    request.MessageOutput?.OutputInfo($"Delete index {request.TextParser.NameIndexElasticSearch}@{request.TextParser.NameServer}...");

                    result.ResultState = await DeleteIndex(request.TextParser);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                var parseTextRequest = new TextParserModule.Models.ParseTextRequest
                {
                    TextParser = request.TextParser,
                    ParserFields = request.TextParserFields,
                    MessageOutput = request.MessageOutput
                };

                request.MessageOutput?.OutputInfo($"Start parsing...");
                result = await ParseFiles(parseTextRequest, request.CancellationToken);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Finished parsing: {result.Result.DocCount}/{result.Result.DocParsed}");

                return result;
            });

            return taskResult;

        }

        public async Task<ResultItem<ReIndexTextParserFieldsResult>> ReIndexTextParserFields(ReIndexTextParserFieldsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ReIndexTextParserFieldsResult>>(async () =>
               {
                   var parseResult = new ParseResult();
                   var result = new ResultItem<ReIndexTextParserFieldsResult>
                   {
                       ResultState = Globals.LState_Success.Clone()
                   };

                   if (string.IsNullOrEmpty(request.IdTextParser))
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "You have to provide a Id for a TextParser!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   if (!Globals.is_GUID(request.IdTextParser))
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "You have to provide a valid Id for a TextParser!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   if (request.IdsFields.Any(id => string.IsNullOrEmpty(id) || !Globals.is_GUID(id)))
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "You have to provide an empty or a valid list of Field-ids!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }


                   request.MessageOutput?.OutputInfo($"Get Textparser...");
                   var oItemTextParserResult = await GetOItem(request.IdTextParser, Globals.Type_Object);
                   result.ResultState = oItemTextParserResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var textParserResult = await GetTextParser(oItemTextParserResult.Result);

                   result.ResultState = textParserResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   request.MessageOutput?.OutputInfo($"Have Textparser.");

                   var textParser = textParserResult.Result.FirstOrDefault();

                   request.MessageOutput?.OutputInfo($"Get Fields...");
                   var textParserFieldsResult = await GetParserFields(new clsOntologyItem { GUID = textParser.IdFieldExtractor, Name = textParser.NameFieldExtractor });
                   result.ResultState = textParserFieldsResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   request.MessageOutput?.OutputInfo($"Have {textParserFieldsResult.Result.Count} Fields.");

                   var dependendFields = textParserFieldsResult.Result.Where(field => !string.IsNullOrEmpty(field.IdReferenceField)).ToList();

                   if (!dependendFields.Any())
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "There are no dependend fields!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   if (request.IdsFields.Any())
                   {
                       dependendFields = dependendFields.Where(field => request.IdsFields.Contains(field.IdField)).ToList();
                   }

                   request.MessageOutput?.OutputInfo($"Found {dependendFields.Count}");



                   if (textParser == null)
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "No Textparser found!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   request.MessageOutput?.OutputInfo($"Connect to {textParser.NameIndexElasticSearch}@{textParser.NameServer}, Type {textParser.NameEsType}");
                   try
                   {
                       var dbReader = new clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);
                       var dbWriter = new clsUserAppDBUpdater(dbReader);

                       bool getDocs = true;
                       long count = 0;
                       int page = 0;
                       string scrollId = null;

                       var documentsToSave = new List<clsAppDocuments>();

                       while (getDocs)
                       {
                           if (request.CancellationToken.IsCancellationRequested)
                           {
                               break;
                           }
                           var docsResult = dbReader.GetData_Documents(5000, textParser.NameIndexElasticSearch, textParser.NameEsType, scrollId: scrollId, query: request.Query, page: page);
                           scrollId = docsResult.ScrollId;
                           page++;
                           if (!docsResult.IsOK)
                           {
                               result.ResultState = Globals.LState_Error.Clone();
                               result.ResultState.Additional1 = docsResult.ErrorMessage;
                               request.MessageOutput?.OutputError(result.ResultState.Additional1);
                               return result;
                           }
                           count += docsResult.Documents.Count;
                           request.MessageOutput?.OutputInfo($"Found {count} Docs");

                           if (count >= docsResult.TotalCount || docsResult.TotalCount == 0)
                           {
                               getDocs = false;
                           }

                           var dependendFieldsAndContentFields = from contentField in textParserFieldsResult.Result
                                                                 join dependendField in dependendFields on contentField.IdField equals dependendField.IdReferenceField
                                                                 select new { contentField, dependendField };


                           foreach (var document in docsResult.Documents)
                           {
                               if (request.CancellationToken.IsCancellationRequested)
                               {
                                   break;
                               }
                               var takeDoc = false;
                               foreach (var field in dependendFieldsAndContentFields)
                               {
                                   if (!document.Dict.ContainsKey(field.contentField.NameField)) continue;

                                   var value = document.Dict[field.contentField.NameField];
                                   parseResult.ResultText = value.ToString();
                                   parseResult.Parse(field.dependendField.RegexPre,
                                       field.dependendField.RegexMain,
                                       field.dependendField.RegexPost,
                                       true,
                                       field.dependendField.DoAll,
                                       field.dependendField.ReplaceList,
                                       field.dependendField.FieldListContained,
                                       field.dependendField.HtmlDecode);
                                   //request.ParseResult.ResultText = contentField.LastContent ?? "";
                                   //request.ParseResult.Parse(regexPre, regexMain, regexPost, request.ReplaceNewLine, field.DoAll, field.ReplaceList, field.FieldListContained, field.HtmlDecode);

                                   var fieldPresent = document.Dict.ContainsKey(field.dependendField.NameField);

                                   if (parseResult.ParseOk)
                                   {

                                       var textParse = parseResult.ResultText;
                                       if (field.dependendField.IdDataType == Config.LocalData.Object_string.GUID)
                                       {
                                           if (fieldPresent)
                                           {
                                               document.Dict[field.dependendField.NameField] = textParse;
                                           }
                                           else
                                           {
                                               document.Dict.Add(field.dependendField.NameField, textParse);
                                           }

                                       }
                                       else if (field.dependendField.IdDataType == Config.LocalData.Object_bit.GUID)
                                       {
                                           var valueCheck = true;

                                           if (bool.TryParse(textParse, out valueCheck))
                                           {
                                               if (fieldPresent)
                                               {
                                                   document.Dict[field.dependendField.NameField] = valueCheck;
                                               }
                                               else
                                               {
                                                   document.Dict.Add(field.dependendField.NameField, valueCheck);
                                               }
                                           }
                                           else
                                           {
                                               if (fieldPresent)
                                               {
                                                   document.Dict[field.dependendField.NameField] = !string.IsNullOrEmpty(textParse);
                                               }
                                               else
                                               {
                                                   document.Dict.Add(field.dependendField.NameField, !string.IsNullOrEmpty(textParse));
                                               }
                                           }
                                       }
                                       else if (field.dependendField.IdDataType == Config.LocalData.Object_int.GUID)
                                       {
                                           long valueCheck = 0;

                                           if (long.TryParse(textParse, out valueCheck))
                                           {
                                               if (fieldPresent)
                                               {
                                                   document.Dict[field.dependendField.NameField] = valueCheck;
                                               }
                                               else
                                               {
                                                   document.Dict.Add(field.dependendField.NameField, valueCheck);
                                               }
                                           }
                                           else
                                           {
                                               parseResult.ParseOk = false;
                                           }
                                       }
                                       else if (field.dependendField.IdDataType == Config.LocalData.Object_datetime.GUID)
                                       {
                                           DateTime valueCheck;
                                           if (DateTime.TryParse(textParse, out valueCheck))
                                           {
                                               if (fieldPresent)
                                               {
                                                   document.Dict[field.dependendField.NameField] = valueCheck;
                                               }
                                               else
                                               {
                                                   document.Dict.Add(field.dependendField.NameField, valueCheck);
                                               }
                                           }
                                           else
                                           {
                                               parseResult.ParseOk = false;
                                           }
                                       }
                                       else if (field.dependendField.IdDataType == Config.LocalData.Object_double.GUID)
                                       {
                                           textParse = textParse.Replace(request.NumberSeperator.Seperator, request.NumberSeperator.SeperatorReplace);
                                           double valueCheck;
                                           if (double.TryParse(textParse, out valueCheck))
                                           {
                                               if (fieldPresent)
                                               {
                                                   document.Dict[field.dependendField.NameField] = valueCheck;
                                               }
                                               else
                                               {
                                                   document.Dict.Add(field.dependendField.NameField, valueCheck);
                                               }
                                           }
                                           else
                                           {
                                               parseResult.ParseOk = false;
                                           }
                                       }

                                       if (parseResult.ParseOk)
                                       {
                                           takeDoc = true;
                                       }

                                   }
                                   else
                                   {
                                       parseResult.ParseOk = false;
                                   }

                               }

                               if (takeDoc)
                               {
                                   documentsToSave.Add(document);

                                   if (documentsToSave.Count > 5000)
                                   {

                                       var resultSave = dbWriter.SaveDoc(documentsToSave, textParser.NameEsType);
                                       result.ResultState = resultSave;
                                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                       {
                                           request.MessageOutput?.OutputError($"Error while saving documents. {result.ResultState.Additional1}");
                                           return result;
                                       }

                                       request.MessageOutput?.OutputInfo($"Saved {documentsToSave.Count} Documents.");
                                       documentsToSave.Clear();
                                   }
                               }
                           }


                       }

                       if (documentsToSave.Count > 0)
                       {
                           var resultSave = dbWriter.SaveDoc(documentsToSave, textParser.NameEsType);
                           result.ResultState = resultSave;
                           if (result.ResultState.GUID == Globals.LState_Error.GUID)
                           {
                               request.MessageOutput?.OutputError($"Error while saving documents. {result.ResultState.Additional1}");
                               return result;
                           }
                           request.MessageOutput?.OutputInfo($"Saved {documentsToSave.Count} Documents.");
                           documentsToSave.Clear();
                       }
                   }
                   catch (Exception ex)
                   {
                       result.ResultState = Globals.LState_Error.Clone();
                       result.ResultState.Additional1 = ex.Message;
                       return result;
                   }


                   return result;
               });

            return taskResult;
        }

        public async Task<ResultItem<ParseResult>> ParseFiles(ParseTextRequest request, CancellationToken cancellationToken)
        {
            var taskResult = await Task.Run<ResultItem<ParseResult>>(async () =>
            {
                var result = new ResultItem<ParseResult>
                {
                    Result = new ParseResult(false),
                    ResultState = Globals.LState_Success.Clone()
                };


                request.UserFields = request.ParserFields.Where(field => !field.IsMeta).ToList();
                request.MessageOutput?.OutputInfo($"Userfields: {request.UserFields.Count}");
                
                if (string.IsNullOrEmpty(request.TextParser.NameServer) ||
                    string.IsNullOrEmpty(request.TextParser.NameIndexElasticSearch) ||
                    request.TextParser.Port == 0)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The config of ES-Index is invalid!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                var dbReader = new clsUserAppDBSelector(request.TextParser.NameServer, request.TextParser.Port, request.TextParser.NameIndexElasticSearch.ToLower(), Globals.SearchRange, Globals.Session);
                var dbWriter = new clsUserAppDBUpdater(dbReader);
                var fileResourceConnector = new FileResourceController(Globals);

                if (request.TextParser.FileResource == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Fileresource provided!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Get Files...");
                var filesResult = await fileResourceConnector.GetFilesByFileResource(request.TextParser.FileResource);

                result.ResultState = filesResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo($"Have Files: {filesResult.Result.Count}");

                var excelImporter = new ExcelImporter(Globals);

                var fileLineInit = request.FileLine;
                foreach (var fileItem in filesResult.Result)
                {
                    request.FileLine = fileLineInit;
                    request.ParserFields.ForEach(field => {
                            field.LastValid = null;
                            field.LastContent = null; 
                            }) ;
                    if (request.ParseWindow != null)
                    {
                        request.ParseWindow.StartChecked = false;
                        request.ParseWindow.EndChecked = false;
                    }

                    var startField = request.ParserFields.FirstOrDefault(field => field.Start);
                    var endField = request.ParserFields.FirstOrDefault(field => field.End);

                    if (startField != null)
                    {
                        request.ParseWindow = new ParseWindow(Globals) { DoParsing = true, StartField = startField, EndField = endField };
                    }
                    var readAlwaysNewLIne = startField != null;

                    request.MessageOutput?.OutputInfo($"Parse file {fileItem}...");
                    if (cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }
                    var fileMeta = new FileMeta();
                    fileMeta.FilePath = fileItem;
                    fileMeta.FileName = System.IO.Path.GetFileName(fileItem);
                    fileMeta.CreateDate = File.GetCreationTime(fileItem).Date;
                    fileMeta.LastWriteDate = File.GetLastWriteTime(fileItem).Date;
                    fileMeta.CreateDatetime = File.GetCreationTime(fileItem);
                    fileMeta.LastWriteTime = File.GetLastWriteTime(fileItem);
                    request.ParserFields.ForEach(field => field.LastValid = null);
                    if (request.TextParser.IsCSVParser)
                    {

                        if (System.IO.Path.GetExtension(fileItem).ToLower() == ".xlsx")
                        {

                            var createRequest = new CreateExcelDataTableRequest
                            {
                                FilePathExcel = fileItem
                            };
                            var exportResult = await excelImporter.CreateExcelDataTable(createRequest);
                            result.ResultState = exportResult.ResultState;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }

                            if (!exportResult.ExcelDataTables.Any())
                            {
                                result.ResultState = Globals.LState_Error.Clone();
                                result.ResultState.Additional1 = "No Sheet could be found!";
                            }

                            var dataTable = exportResult.ExcelDataTables.First().DataTable;
                            var fileLine = request.FileLine;


                            var colDict = new Dictionary<string, object>();
                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {

                                var dict = new Dictionary<string, object>();
                                foreach (var field in request.ParserFields.Where(fieldItem => fieldItem.NameCSVField != null || fieldItem.IsInsert || fieldItem.IsMergeField || fieldItem.IsMeta))
                                {
                                    if (field.IsMeta)
                                    {
                                        if (field.IdMetaField == Config.LocalData.Object_Message.GUID)
                                        {
                                            dict.Add(field.NameMetaField, request.Text);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FilePath.GUID)
                                        {
                                            dict.Add(field.NameMetaField, request.FileMeta != null ? request.FileMeta.FilePath : "");
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileName.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.FileName : "");
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_Guid.GUID)
                                        {
                                            dict.Add(field.NameField, Globals.NewGUID.ToString());
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_Date.GUID)
                                        {
                                            dict.Add(field.NameField, DateTime.Now.Date);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_DateTimeStamp.GUID)
                                        {
                                            dict.Add(field.NameField, DateTime.Now);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Create_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.CreateDate : DateTime.Now.Date);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Last_Change_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.LastWriteDate : DateTime.Now.Date);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDateTime__Create_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.CreateDatetime : DateTime.Now);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Last_Change_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.LastWriteTime : DateTime.Now);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileLine.GUID)
                                        {
                                            dict.Add(field.NameField, fileLine);
                                        }

                                    }
                                    else if (field.IsInsert)
                                    {
                                        dict.Add(field.NameField, field.Insert);
                                    }
                                    else if (field.IsMergeField)
                                    {
                                        var value = field.MergeField.Value(dict);
                                        AddFieldToDict(dict, field, value);
                                    }
                                    else if (dataTable.Columns.Contains(field.NameCSVField))
                                    {
                                        var value = row[field.NameCSVField].ToString();

                                        if (field.ReplaceList != null && field.ReplaceList.Any())
                                        {
                                            field.ReplaceList.Where(rep => !string.IsNullOrEmpty(rep.RegExSearch)).ToList().ForEach(rep =>
                                            {
                                                value = Regex.Replace(value, rep.RegExSearch,
                                                    rep.ReplaceWith ?? "");
                                            });
                                        }
                                        AddFieldToDict(dict, field, value);
                                    }
                                }
                                if (dict.Any())
                                {
                                    request.DocList.Add(new clsAppDocuments
                                    {
                                        Id = Globals.NewGUID,
                                        Dict = dict
                                    });
                                }

                                if (request.DocList.Count > Globals.SearchRange)
                                {

                                    result.ResultState = dbWriter.SaveDoc(request.DocList, request.TextParser.NameEsType);
                                    var docCount = request.DocList.Count;
                                    request.DocList.Clear();
                                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                    {
                                        result.ResultState.Additional1 = "Error while saving Documents";
                                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                        break;
                                    }
                                    request.MessageOutput?.OutputInfo($"Saved Docs: {docCount}");
                                }

                                fileLine++;
                                result.Result.DocParsed++;
                                result.Result.DocCount++;


                            }
                        }
                        else
                        {
                            DataTable dt = DataTable.New.ReadCsv(fileItem);
                            var fileLine = request.FileLine;
                            foreach (Row row in dt.Rows)
                            {
                                var dict = new Dictionary<string, object>();
                                foreach (var field in request.ParserFields.Where(fieldItem => fieldItem.NameCSVField != null || fieldItem.IsInsert || fieldItem.IsMergeField || fieldItem.IsMeta))
                                {
                                    if (field.IsMeta)
                                    {
                                        if (field.IdMetaField == Config.LocalData.Object_Message.GUID)
                                        {
                                            dict.Add(field.NameMetaField, request.Text);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FilePath.GUID)
                                        {
                                            dict.Add(field.NameMetaField, request.FileMeta != null ? request.FileMeta.FilePath : "");
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileName.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.FileName : "");
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_Guid.GUID)
                                        {
                                            dict.Add(field.NameField, Globals.NewGUID.ToString());
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_Date.GUID)
                                        {
                                            dict.Add(field.NameField, DateTime.Now.Date);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_DateTimeStamp.GUID)
                                        {
                                            dict.Add(field.NameField, DateTime.Now);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Create_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.CreateDate : DateTime.Now.Date);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Last_Change_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.LastWriteDate : DateTime.Now.Date);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDateTime__Create_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.CreateDatetime : DateTime.Now);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Last_Change_.GUID)
                                        {
                                            dict.Add(field.NameField, request.FileMeta != null ? request.FileMeta.LastWriteTime : DateTime.Now);
                                        }
                                        else if (field.IdMetaField == Config.LocalData.Object_FileLine.GUID)
                                        {
                                            dict.Add(field.NameField, fileLine);
                                        }

                                    }
                                    else if (field.IsInsert)
                                    {
                                        dict.Add(field.NameField, field.Insert);
                                    }
                                    else if (field.IsMergeField)
                                    {
                                        var value = field.MergeField.Value(dict);
                                        if (!string.IsNullOrEmpty(value))
                                        {
                                            AddFieldToDict(dict, field, value);
                                        }

                                    }
                                    else if (row.ColumnNames.Contains(field.NameCSVField))
                                    {
                                        var value = row[field.NameCSVField];
                                        AddFieldToDict(dict, field, value);

                                    }
                                }

                                if (dict.Any())
                                {
                                    request.DocList.Add(new clsAppDocuments
                                    {
                                        Id = Globals.NewGUID,
                                        Dict = dict
                                    });
                                }

                                if (request.DocList.Count > Globals.SearchRange)
                                {

                                    result.ResultState = dbWriter.SaveDoc(request.DocList, request.TextParser.NameEsType);
                                    var docCount = request.DocList.Count;
                                    request.DocList.Clear();
                                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                                    {
                                        result.ResultState.Additional1 = "Error while saving Documents";
                                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                        break;
                                    }
                                    request.MessageOutput?.OutputInfo($"Saved Docs: {docCount}");
                                }
                                result.Result.DocParsed++;
                                result.Result.DocCount++;
                                fileLine++;
                            }
                        }




                        if (request.DocList.Any())
                        {
                            result.ResultState = dbWriter.SaveDoc(request.DocList, request.TextParser.NameEsType);
                            var docCount = request.DocList.Count;
                            request.DocList.Clear();
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                result.ResultState.Additional1 = "Error while saving Documents";
                                break;
                            }
                            request.MessageOutput?.OutputInfo($"Saved Docs: {docCount}");
                        }
                    }
                    else
                    {
                        request.FileMeta = fileMeta;
                        using (var fileStream = new FileStream(fileItem, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            using (var textReader = new StreamReader(fileStream))
                            {
                                while (!textReader.EndOfStream)
                                {
                                    if (cancellationToken.IsCancellationRequested)
                                    {
                                        break;
                                    }
                                    request.FileLine++;

                                    if (request.ParseWindow?.EndChecked ?? false)
                                    {
                                        break;
                                    }
                                    if (readAlwaysNewLIne || request.TextParser.TextSeperators == null ||
                                             (request.TextParser.TextSeperators.Any() && request.TextParser.TextSeperators.First().NameTextSeperator == "\\r\\n"))
                                    {
                                        request.Text = textReader.ReadLine();
                                    }
                                    else
                                    {
                                        request.ReplaceNewLine = true;
                                        request.Text = "";

                                        StringBuilder line = new StringBuilder();   
                                        var length = 1;
                                        var tester = "";
                                        var finished = false;
                                        while (!finished && !textReader.EndOfStream)
                                        {
                                            length = line.Length;
                                            char singleChar = (char)textReader.Read();
                                            tester += singleChar;
                                            if (request.TextParser.TextSeperators.Any(s => tester.EndsWith(s.NameTextSeperator)))
                                            {
                                                request.Text = tester.ToString();
                                                finished = true;
                                            }
                                        }
                                    }

                                    result.ResultState = ParseText(request);
                                    if (request.ParseWindow?.StartChecked ?? false)
                                    {
                                        readAlwaysNewLIne = false;
                                    }
                                    
                                    if (result.ResultState.GUID == Globals.LState_Success.GUID)
                                    {


                                        if ((request.DocCount > 0 && request.DocCount % Globals.SearchRange == 0))
                                        {
                                            var count = request.DocList.Count;

                                            result.ResultState = dbWriter.SaveDoc(request.DocList,
                                                 !string.IsNullOrEmpty(request.TextParser.NameEsType)
                                                     ? request.TextParser.NameEsType
                                                     : "Doc");
                                            request.MessageOutput?.OutputInfo($"Saved Docs: {request.DocList.Count}");
                                            if (result.ResultState.GUID == Globals.LState_Success.GUID)
                                            {
                                                request.DocCount = 0;
                                                result.Result.DocParsed += count;



                                            }

                                            request.DocList.Clear();
                                        }


                                        result.Result.DocParsed++;
                                    }

                                }

                                if (request.DocList.Any())
                                {
                                    result.ResultState = dbWriter.SaveDoc(request.DocList,
                                                 !string.IsNullOrEmpty(request.TextParser.NameEsType)
                                                     ? request.TextParser.NameEsType
                                                     : "Doc");
                                    request.MessageOutput?.OutputInfo($"Saved Docs: {request.DocList.Count}");
                                    request.DocList.Clear();
                                }
                            }
                        }
                    }
                }



                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ParseResult>> ParseWholeText(ParseTextRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ParseResult>>(() =>
           {
               var result = new ResultItem<ParseResult>
               {
                   Result = new ParseResult(false),
                   ResultState = Globals.LState_Success.Clone()
               };

               request.UserFields = request.ParserFields.Where(field => !field.IsMeta).ToList();

               var dbReader = new clsUserAppDBSelector(request.TextParser.NameServer, request.TextParser.Port, request.TextParser.NameIndexElasticSearch.ToLower(), Globals.SearchRange, Globals.Session);
               var dbWriter = new clsUserAppDBUpdater(dbReader);

               var textToRead = request.Text;

               using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(textToRead)))
               {
                   using (var textReader = new StreamReader(memStream))
                   {
                       while (!textReader.EndOfStream)
                       {
                           if (request.CancellationToken.IsCancellationRequested)
                           {
                               break;
                           }
                           request.FileLine++;

                           if (request.TextParser.TextSeperators == null ||
                                    (request.TextParser.TextSeperators.Any() && request.TextParser.TextSeperators.First().NameTextSeperator == "\\r\\n"))
                           {

                               request.Text = textReader.ReadLine();
                           }
                           else
                           {
                               request.ReplaceNewLine = true;
                               request.Text = "";

                               StringBuilder line = new StringBuilder();
                               var length = 1;
                               var tester = "";
                               var finished = false;
                               while (!finished && !textReader.EndOfStream)
                               {
                                   length = line.Length;
                                   char singleChar = (char)textReader.Read();
                                   tester += singleChar;
                                   if (request.TextParser.TextSeperators.Any(s => tester.EndsWith(s.NameTextSeperator)))
                                   {
                                       request.Text = tester.ToString();
                                       finished = true;
                                   }



                               }


                           }

                           result.ResultState = ParseText(request);
                           if (result.ResultState.GUID == Globals.LState_Success.GUID)
                           {


                               if ((request.DocCount > 0 && request.DocCount % Globals.SearchRange == 0))
                               {
                                   var count = request.DocList.Count;

                                   result.ResultState = dbWriter.SaveDoc(request.DocList,
                                        !string.IsNullOrEmpty(request.TextParser.NameEsType)
                                            ? request.TextParser.NameEsType
                                            : "Doc", request.TextParser.NameIndexElasticSearch.ToLower());
                                   if (result.ResultState.GUID == Globals.LState_Success.GUID)
                                   {
                                       request.DocCount = 0;
                                       result.Result.DocParsed += count;



                                   }

                                   request.DocList.Clear();
                               }


                               result.Result.DocParsed++;
                           }

                       }

                       if (request.DocList.Any())
                       {
                           result.ResultState = dbWriter.SaveDoc(request.DocList,
                                        !string.IsNullOrEmpty(request.TextParser.NameEsType)
                                            ? request.TextParser.NameEsType
                                            : "Doc");
                           request.DocList.Clear();
                       }
                   }
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<ReIndexPatternSourceFieldsResult>> ReIndexPatternSourceFields(ReIndexPatternSourceFieldsRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<ReIndexPatternSourceFieldsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ReIndexPatternSourceFieldsResult()
                };

                request.MessageOutput?.OutputInfo("Get the TextParser OItem...");
                var textParserItemResult = await GetOItem(request.IdTextParser, Globals.Type_Object);

                result.ResultState = textParserItemResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get TextParser...");
                var textParserResult = await GetTextParser(textParserItemResult.Result);
                result.ResultState = textParserResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var textParser = textParserResult.Result.FirstOrDefault();

                if (textParser == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"No valid Textparser: {textParserItemResult.Result.Name}";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var textParserFieldsResult = await GetParserFields(new clsOntologyItem
                {
                    GUID = textParser.IdFieldExtractor,
                    Name = textParser.NameFieldExtractor,
                    GUID_Parent = textParser.IdClassFieldExtractor,
                    Type = Globals.Type_Object
                });
                result.ResultState = textParserFieldsResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var textParserFields = textParserFieldsResult.Result;
                if (!textParserFields.Any())
                {
                    result.ResultState = Globals.LState_Nothing.Clone();
                    result.ResultState.Additional1 = "No Textparser-Fields!";
                    return result;
                }

                var reIndexRequest = new ReIndexPatternSourceFieldsByTextparserRequest(textParser, textParserFields, request.CancellationToken)
                {
                    MessageOutput = request.MessageOutput
                };

                result = await ReIndexPatternSourceFields(reIndexRequest);
                
                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ReIndexPatternSourceFieldsResult>> ReIndexPatternSourceFields(ReIndexPatternSourceFieldsByTextparserRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<ReIndexPatternSourceFieldsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ReIndexPatternSourceFieldsResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateReIndexPatternSourceFieldsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "User-Cancel!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                var serviceAgent = new ServiceAgentElastic(Globals);
                var modelResult = await serviceAgent.GetReIndexPatternSourceFieldsModel(request);
                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState.Additional1 = "User-Cancel!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                var dbReader = new clsUserAppDBSelector(request.TextParser.NameServer, request.TextParser.Port, request.TextParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);
                var dbWriter = new clsUserAppDBUpdater(dbReader);
                foreach (var patternSourceField in (from field in modelResult.Result.PatternSourceFields
                                                    join patternSourceToField in modelResult.Result.PatternSourcesToFields on field.IdField equals patternSourceToField.ID_Other
                                                    join patternSourceToValue in modelResult.Result.PatternSourceValues on patternSourceToField.ID_Object equals patternSourceToValue.ID_Object
                                                    select new { field, patternSourceToValue }).ToList())
                {
                    request.MessageOutput?.OutputInfo($"Field: {patternSourceField.field.NameField}");
                    object value = null;
                    if (patternSourceField.patternSourceToValue.ID_DataType == Globals.DType_Bool.GUID)
                    {
                        value = patternSourceField.patternSourceToValue.Val_Bit.Value;
                    }
                    else if (patternSourceField.patternSourceToValue.ID_DataType == Globals.DType_DateTime.GUID)
                    {
                        value = patternSourceField.patternSourceToValue.Val_Date.Value;
                    }
                    else if (patternSourceField.patternSourceToValue.ID_DataType == Globals.DType_Real.GUID)
                    {
                        value = patternSourceField.patternSourceToValue.Val_Double.Value;
                    }
                    else if (patternSourceField.patternSourceToValue.ID_DataType == Globals.DType_Int.GUID)
                    {
                        value = patternSourceField.patternSourceToValue.Val_Int.Value;
                    }
                    else
                    {
                        value = patternSourceField.patternSourceToValue.Val_String;
                    }
                    var pattern = (from patternSource in modelResult.Result.PatternSourcesToFields.Where(patSourceToField => patSourceToField.ID_Other == patternSourceField.field.IdField)
                                   join patternSourceToPatternItem in modelResult.Result.PatternSourceToPattern on patternSource.ID_Object equals patternSourceToPatternItem.ID_Object
                                   join patternPattern in modelResult.Result.PatternPattern on patternSourceToPatternItem.ID_Other equals patternPattern.ID_Object
                                   select patternPattern).First().Val_String;
                    bool getDocs = true;
                    long count = 0;
                    int page = 0;
                    string scrollId = null;

                    var documentsToSave = new List<clsAppDocuments>();

                    while (getDocs)
                    {
                        if (request.CancellationToken.IsCancellationRequested)
                        {
                            break;
                        }
                        var docsResult = dbReader.GetData_Documents(5000, request.TextParser.NameIndexElasticSearch, request.TextParser.NameEsType, scrollId: scrollId, query: pattern, page: page);
                        scrollId = docsResult.ScrollId;
                        page++;
                        if (!docsResult.IsOK)
                        {
                            result.ResultState = Globals.LState_Error.Clone();
                            result.ResultState.Additional1 = docsResult.ErrorMessage;
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }

                        foreach (var doc in docsResult.Documents)
                        {

                            if (doc.Dict.ContainsKey(patternSourceField.field.NameField))
                            {
                                doc.Dict[patternSourceField.field.NameField] = value;
                            }
                            else
                            {
                                doc.Dict.Add(patternSourceField.field.NameField, value);
                            }
                        }

                        result.ResultState = dbWriter.SaveDoc(docsResult.Documents, request.TextParser.NameEsType);
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError("Error while saving the documents!");
                            return result;
                        }

                        count += docsResult.Documents.Count;
                        request.MessageOutput?.OutputInfo($"Saved {count} of {docsResult.TotalCount} Docs");

                        if (count >= docsResult.TotalCount || docsResult.TotalCount == 0)
                        {
                            getDocs = false;
                        }
                    }

                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteIndex(string idTextParser)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                if (string.IsNullOrEmpty(idTextParser))
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "You must provide a id for the TextParser!";
                    return result;
                }

                var parserOItem = await GetOItem(idTextParser, Globals.Type_Object);

                result = parserOItem.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }


                var textParserResult = await GetTextParser(parserOItem.Result);

                result = textParserResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result = await DeleteIndex(textParserResult.Result[0]);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }
        public async Task<clsOntologyItem> DeleteIndex(TextParser textParser)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();

                if (string.IsNullOrEmpty(textParser.NameIndexElasticSearch))
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No Index provided!";
                    return result;
                }

                var dbConnector = new OntologyModDBConnector(Globals);

                result = dbConnector.DeleteIndex(textParser.NameIndexElasticSearch);

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = $"Error while deleting Index {textParser.NameIndexElasticSearch}";
                }
                return result;
            });
            return taskResult;
        }

        private clsOntologyItem ParseText(ParseTextRequest request)
        {

            if (request.ParseWindow == null)
            {
                request.ParseWindow = new ParseWindow(Globals) { DoParsing = true };
            }

            var dictMeta = new Dictionary<string, object>();
            var dictUser = new Dictionary<string, object>();

            var addMessageRest = false;

            var textParse = request.Text;
            var textParseBase = request.Text;
            var Id = Globals.NewGUID;

            var ixStart = 0;
            var ixStartLast = 0;
            var fieldNotFound = false;
            var dontAddUser = false;

            var result = Globals.LState_Success.Clone();


            var parseFields = request.ParserFields.Where(pField => !pField.Start && !pField.End).ToList();
            foreach (var field in parseFields.OrderBy(p => p.IsMeta).ThenBy(p => p.OrderId).ToList())
            {
                string regexPre = field.IdRegexPre != null &&
                                   field.IdRegexPre != Config.LocalData.Object_empty.GUID
                                   ? field.RegexPre : null;
                string regexPost = field.IdRegexPost != null &&
                                   field.IdRegexPost != Config.LocalData.Object_empty.GUID
                    ? field.RegexPost
                    : null;

                string regexMain = field.IdRegexMain != null &&
                                   field.IdRegexMain != Config.LocalData.Object_empty.GUID
                                   ? field.RegexMain : null;

                if (request.ParseWindow.CheckStart && !request.ParseWindow.StartChecked)
                {
                    request.ParseResult.ResultText = textParse;
                    request.ParseResult.Parse(request.ParseWindow.RegexPre_Start,
                        request.ParseWindow.RegexMain_Start,
                        request.ParseWindow.RegexPost_Start,
                        request.ReplaceNewLine,
                        request.ParseWindow.StartField.DoAll,
                        request.ParseWindow.StartField.ReplaceList,
                        request.ParseWindow.StartField.FieldListContained);
                    if (request.ParseResult.ParseOk)
                    {
                        textParse = textParse.Substring(request.ParseResult.IxStartMain + request.ParseResult.LengthMain);
                        request.ParseWindow.StartChecked = true;
                        request.ParseWindow.DoParsing = true;
                    }
                }

                if (request.ParseWindow.CheckEnd && !request.ParseWindow.EndChecked)
                {
                    request.ParseResult.ResultText = request.Text;
                    request.ParseResult.Parse(request.ParseWindow.RegexPre_End, request.ParseWindow.RegexMain_End, request.ParseWindow.RegexPost_End, request.ReplaceNewLine, request.ParseWindow.EndField.DoAll, request.ParseWindow.EndField.ReplaceList, request.ParseWindow.EndField.FieldListContained);
                    if (request.ParseResult.ParseOk)
                    {
                        request.ParseWindow.EndChecked = true;
                        request.ParseWindow.DoParsing = false;
                    }
                }

                if (request.ParseWindow.DoParsing)
                {
                    field.LastContent = "";
                    fieldNotFound = false;
                    var getIxStart = true;
                    if (field.IsMeta)
                    {
                        if (field.IdMetaField == Config.LocalData.Object_Message.GUID)
                        {
                            dictMeta.Add(field.NameMetaField, request.Text);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_FilePath.GUID)
                        {
                            dictMeta.Add(field.NameMetaField, request.FileMeta != null ? request.FileMeta.FilePath : "");
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_FileName.GUID)
                        {
                            dictMeta.Add(field.NameField, request.FileMeta != null ? request.FileMeta.FileName : "");
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_Guid.GUID)
                        {
                            Id = Globals.NewGUID;
                            dictMeta.Add(field.NameField, Id);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_Date.GUID)
                        {
                            dictMeta.Add(field.NameField, DateTime.Now.Date);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_DateTimeStamp.GUID)
                        {
                            dictMeta.Add(field.NameField, DateTime.Now);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Create_.GUID)
                        {
                            dictMeta.Add(field.NameField, request.FileMeta != null ? request.FileMeta.CreateDate : DateTime.Now.Date);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Last_Change_.GUID)
                        {
                            dictMeta.Add(field.NameField, request.FileMeta != null ? request.FileMeta.LastWriteDate : DateTime.Now.Date);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_FileDateTime__Create_.GUID)
                        {
                            dictMeta.Add(field.NameField, request.FileMeta != null ? request.FileMeta.CreateDatetime : DateTime.Now);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_FileDate__Last_Change_.GUID)
                        {
                            dictMeta.Add(field.NameField, request.FileMeta != null ? request.FileMeta.LastWriteTime : DateTime.Now);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_FileLine.GUID)
                        {
                            dictMeta.Add(field.NameField, request.FileLine);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_Custom.GUID)
                        {
                            dictMeta.Add(field.NameField, field.CustomContent);
                        }
                        else if (field.IdMetaField == Config.LocalData.Object_MessageRest.GUID)
                        {
                            addMessageRest = true;
                        }
                    }
                    else if (field.IsInsert)
                    {
                        dictUser.Add(field.NameField, field.Insert);
                    }
                    else if (field.IsMergeField)
                    {
                        var dictAll = dictMeta.Union(dictUser).ToDictionary(k => k.Key, v => v.Value);
                        var value = field.MergeField.Value(dictAll);
                        if (!string.IsNullOrEmpty(value))
                        {
                            AddFieldToDict(dictUser, field, value);
                        }

                    }
                    else
                    {



                        if (field.FieldListContained != null && field.FieldListContained.Any())
                        {
                            (from objField in request.ParserFields
                             join objContainedList in field.FieldListContained on objField.IdField equals objContainedList.IdField
                             select new { objField, objContainedList }).ToList().ForEach(fieldChange =>
                             {
                                 fieldChange.objContainedList.LastContent = fieldChange.objField.LastContent;
                             });
                        }

                        if (!string.IsNullOrEmpty(field.IdReferenceField))
                        {
                            var contentField = request.ParserFields.FirstOrDefault(fieldSource => fieldSource.IdField == field.IdReferenceField);
                            if (contentField != null)
                            {
                                request.ParseResult.ResultText = contentField.LastContent ?? "";
                                request.ParseResult.Parse(regexPre, regexMain, regexPost, request.ReplaceNewLine, field.DoAll, field.ReplaceList, field.FieldListContained, field.HtmlDecode);
                            }
                        }
                        if (string.IsNullOrEmpty(field.IdReferenceField))
                        {
                            request.ParseResult.ResultText = textParse ?? "";
                            request.ParseResult.Parse(regexPre, regexMain, regexPost, request.ReplaceNewLine, field.DoAll, field.ReplaceList, field.FieldListContained, field.HtmlDecode);
                            textParse = request.ParseResult.ResultText;
                        }

                        if (request.ParseResult.ParseOk)
                        {
                            if (string.IsNullOrEmpty(field.IdReferenceField))
                            {
                                field.LastContent = request.ParseResult.ResultText;
                            }
                            else
                            {
                                textParse = request.ParseResult.ResultText;
                                field.LastContent = textParse;
                            }

                            if (field.IdDataType == Config.LocalData.Object_string.GUID)
                            {
                                dictUser.Add(field.NameField, textParse);
                                if (field.UseLastValid)
                                {
                                    field.LastValid = textParse;
                                }

                            }
                            else if (field.IdDataType == Config.LocalData.Object_bit.GUID)
                            {
                                var value = true;

                                if (bool.TryParse(textParse, out value))
                                {
                                    dictUser.Add(field.NameField, value);
                                    if (field.UseLastValid)
                                    {
                                        field.LastValid = value;
                                    }
                                }
                                else
                                {
                                    dictUser.Add(field.NameField, !string.IsNullOrEmpty(textParse));
                                    if (field.UseLastValid)
                                    {
                                        field.LastValid = !string.IsNullOrEmpty(textParse);
                                    }
                                }
                            }
                            else if (field.IdDataType == Config.LocalData.Object_int.GUID)
                            {
                                long value = 0;

                                if (long.TryParse(textParse, out value))
                                {
                                    dictUser.Add(field.NameField, value);
                                    if (field.UseLastValid)
                                    {
                                        field.LastValid = value;
                                    }
                                }
                                else
                                {
                                    request.ParseResult.ParseOk = false;
                                }
                            }
                            else if (field.IdDataType == Config.LocalData.Object_datetime.GUID)
                            {
                                DateTime value;
                                if (DateTime.TryParse(textParse, field.CultureInfo, System.Globalization.DateTimeStyles.None, out value))
                                {
                                    dictUser.Add(field.NameField, value);
                                    if (field.UseLastValid)
                                    {
                                        field.LastValid = value;
                                    }
                                }
                                else
                                {
                                    request.ParseResult.ParseOk = false;
                                }
                            }
                            else if (field.IdDataType == Config.LocalData.Object_double.GUID)
                            {
                                double value;
                                if (double.TryParse(textParse, NumberStyles.AllowDecimalPoint, field.CultureInfo, out value))
                                {
                                    dictUser.Add(field.NameField, value);
                                    if (field.UseLastValid)
                                    {
                                        field.LastValid = value;
                                    }
                                }
                                else
                                {
                                    request.ParseResult.ParseOk = false;
                                }
                            }
                        }
                        else if (field.UseLastValid && field.LastValid != null)
                        {
                            dictUser.Add(field.NameField, field.LastValid);
                        }


                        if (!request.ParseResult.ParseOk)
                        {
                            field.LastContent = "";
                            dontAddUser = true;
                        }
                        if (field.RemoveFromSource)
                        {
                            ixStart = ixStartLast + (request.ParseResult.IxEndPre > -1 ? request.ParseResult.IxEndPre : 0) + request.ParseResult.IxStartMain + request.ParseResult.LengthMain;
                            if (request.Text.Length > ixStart)
                                textParseBase = request.Text.Substring(ixStart);
                            else
                                textParseBase = request.Text;

                            if (request.Text.Length - 1 > ixStart)
                                textParse = request.Text.Substring(ixStart);
                            else
                                textParse = request.Text;
                            ixStartLast = ixStart;
                        }
                        else
                        {
                            textParse = textParseBase;
                        }

                    }
                }
            }

            if (addMessageRest)
            {
                dictMeta.Add(Config.LocalData.Object_MessageRest.Name, textParse);
            }
            if (dictMeta.Any() || (!request.UserFields.Any() || dictUser.Any()))
            {
                //if (dontAddUser) dictUser.Clear();
                if (request.ParentField != null)
                {
                    dictUser.Add("GUID_Field_Parent", request.ParentField.GUID);
                    dictUser.Add("Name_Field_Parent", request.ParentField.Name);
                }
                dictUser = dictUser.Union(dictMeta).ToDictionary(pair => pair.Key, pair => pair.Value);
                var objDoc = new clsAppDocuments { Id = Id, Dict = dictUser };
                request.DocList.Add(objDoc);
                request.DocCount++;

            }


            if (!request.ParseResult.ParseOk)
            {
                result = Globals.LState_Error.Clone();
            }
            return result;



        }

        public bool AddFieldToDict(Dictionary<string, object> dictUser, ParserField field, string value)
        {
            if (field.IdDataType == Config.LocalData.Object_bit.GUID)
            {
                bool bValue;

                if (bool.TryParse(value, out bValue))
                {
                    dictUser.Add(field.NameField, bValue);

                }
                else
                {
                    return false;
                }
            }
            else if (field.IdDataType == Config.LocalData.Object_datetime.GUID)
            {
                DateTime dtValue;

                if (DateTime.TryParse(value, field.CultureInfo, System.Globalization.DateTimeStyles.None, out dtValue))
                {
                    dictUser.Add(field.NameField, dtValue);
                }
                else
                {
                    return false;
                }
            }
            else if (field.IdDataType == Config.LocalData.Object_int.GUID)
            {
                long lValue;

                if (long.TryParse(value, out lValue))
                {
                    dictUser.Add(field.NameField, lValue);
                }
                else
                {
                    return false;
                }
            }
            else if (field.IdDataType == Config.LocalData.Object_double.GUID)
            {
                double dblValue;

                if (double.TryParse(value, System.Globalization.NumberStyles.Any, field.CultureInfo, out dblValue))
                {
                    dictUser.Add(field.NameField, dblValue);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                dictUser.Add(field.NameField, value);
            }
            return true;
        }

        public async Task<ResultItem<Dictionary<string, object>>> GetGridConfig(List<ParserField> parserFields, string getDataUrl)
        {
            var factory = new Factories.KendoGridFactory();
            return await factory.CrateGridConfig(parserFields, getDataUrl, Globals);
        }

        public async Task<ResultGetDocuments> LoadData(TextParser textParser, List<ParserField> parserFields, int page, int size, string query = "*", string scrollId = null, List<KendoSortRequest> sortRequests = null)
        {
            var taskResult = await Task.Run<ResultGetDocuments>(() =>
            {
                var sortRequestFields = new List<Nest.SortField>();
                if (sortRequests != null)
                {
                    if (parserFields == null) parserFields = new List<ParserField>();
                    sortRequestFields = (from sortRequest in sortRequests
                                         join parserField in parserFields on sortRequest.field equals parserField.NameField
                                         select new { sortRequest, parserField }).ToList().Select(sort =>
                                         {
                                             var fieldName = sort.sortRequest.field;
                                             if (sort.parserField.DataType == "string")
                                             {
                                                 fieldName += ".keyword";
                                             }
                                             var sortField = new Nest.SortField { Field = fieldName, Order = sort.sortRequest.dir == "asc" ? SortOrder.Ascending : SortOrder.Descending };

                                             return sortField;
                                         }).ToList();

                }
                var dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(textParser.NameServer, textParser.Port, textParser.NameIndexElasticSearch.ToLower(), 5000, Globals.Session);
                var documents = dbReader.GetData_Documents(size, textParser.NameIndexElasticSearch.ToLower(), textParser.NameEsType, query, page, scrollId, sortFields: sortRequestFields);
                return documents;
            });

            return taskResult;

        }

        public async Task<ResultItem<RelateTextParserDocsResult>> RelateTextParserDocs(RelateTextParserDocsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<RelateTextParserDocsResult>>(async () =>
            {
                var result = new ResultItem<RelateTextParserDocsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new RelateTextParserDocsResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateRelateTextParserDocsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Connect to index...");
                var dbReader = new clsUserAppDBSelector(request.TextParser.NameServer, request.TextParser.Port, request.TextParser.NameIndexElasticSearch, Globals.SearchRange, Globals.Session);
                request.MessageOutput?.OutputInfo("Connected to index.");

                var query = "";
                if (request.IdsDocuments.Any())
                {
                    foreach (var id in request.IdsDocuments)
                    {
                        if (query != "")
                        {
                            query += " OR ";
                        }

                        query += $"_id:{id}";
                    }
                }
                else
                {
                    query = request.Query;
                }

                var documentsResult = dbReader.GetData_Documents(Globals.SearchRange, strType: request.TextParser.NameEsType, query: query);
                var page = 0;
                if (!documentsResult.IsOK)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = documentsResult.ErrorMessage;
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var total = documentsResult.TotalCount;
                long count = 0;

                var relationConfig = new clsRelationConfig(Globals);
                var textParserItem = new clsOntologyItem
                {
                    GUID = request.TextParser.IdParser,
                    Name = request.TextParser.NameParser,
                    GUID_Parent = Config.LocalData.Class_Textparser.GUID,
                    Type = Globals.Type_Object
                };

                var parserFields = request.ParserFields.Select(field => new clsOntologyItem
                {
                    GUID = field.IdField,
                    Name = field.NameField,
                    GUID_Parent = Config.LocalData.Class_Field.GUID,
                    Type = Globals.Type_Object
                }).ToList();

                var elasticAgent = new ServiceAgentElastic(Globals);

                count = documentsResult.Documents.Count;
                request.MessageOutput?.OutputInfo($"{count}/{total} items");
                count = 0;
                while (documentsResult.Documents.Count > 0)
                {
                    count += documentsResult.Documents.Count;
                    request.MessageOutput?.OutputInfo($"{count}/{total} items");

                    result.ResultState = await SaveDocuments(documentsResult.Documents, relationConfig, textParserItem, parserFields, elasticAgent);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    page++;
                    documentsResult = dbReader.GetData_Documents(Globals.SearchRange, strType: request.TextParser.NameEsType, query: query, scrollId: documentsResult.ScrollId, page: page);
                }

                return result;
            });

            return taskResult;
        }

        private async Task<clsOntologyItem> SaveDocuments(List<clsAppDocuments> documents, clsRelationConfig relationConfig, clsOntologyItem textParser, List<clsOntologyItem> parserFields, ServiceAgentElastic elsaticAgent)
        {

            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var objects = new List<clsOntologyItem>();
                var relations = new List<clsObjectRel>();
                var attributes = new List<clsObjectAtt>();

                var result = Globals.LState_Success.Clone();
                foreach (var doc in documents)
                {
                    var objectItem = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = doc.Id,
                        GUID_Parent = Relate.Config.LocalData.Class_Textparser_Details.GUID,
                        Type = Globals.Type_Object
                    };

                    relations.Add(relationConfig.Rel_ObjectRelation(objectItem, textParser, Relate.Config.LocalData.RelationType_belongs_to));
                    attributes.Add(relationConfig.Rel_ObjectAttribute(objectItem, Relate.Config.LocalData.AttributeType_Id_Value, doc.Id));
                    objects.Add(objectItem);
                    for (var ix = 0; ix < parserFields.Count; ix++)
                    {

                        var parserField = parserFields[ix];
                        relations.Add(relationConfig.Rel_ObjectRelation(objectItem, parserField, Relate.Config.LocalData.RelationType_contains, orderId: ix));

                        if (doc.Dict.ContainsKey(parserField.Name))
                        {
                            var value = doc.Dict[parserField.Name];
                            if (value.GetType() == typeof(bool))
                            {
                                attributes.Add(relationConfig.Rel_ObjectAttribute(objectItem, Relate.Config.LocalData.AttributeType_Bitvalue, value, orderId: ix));
                            }
                            else if (value.GetType() == typeof(long))
                            {
                                attributes.Add(relationConfig.Rel_ObjectAttribute(objectItem, Relate.Config.LocalData.AttributeType_Longvalue, value, orderId: ix));
                            }
                            else if (value.GetType() == typeof(double))
                            {
                                attributes.Add(relationConfig.Rel_ObjectAttribute(objectItem, Relate.Config.LocalData.AttributeType_Doublevalue, value, orderId: ix));
                            }
                            else if (value.GetType() == typeof(DateTime))
                            {
                                attributes.Add(relationConfig.Rel_ObjectAttribute(objectItem, Relate.Config.LocalData.AttributeType_Datetimevalue, value, orderId: ix));
                            }
                            else if (value.GetType() == typeof(string))
                            {
                                attributes.Add(relationConfig.Rel_ObjectAttribute(objectItem, Relate.Config.LocalData.AttributeType_Stringvalue, value, orderId: ix));
                            }
                            else
                            {
                                result = Globals.LState_Error.Clone();
                                result.Additional1 = $"Type of Field {parserField.Name} is {value.GetType()}! This type is not supported!";
                                return result;
                            }
                        }
                    }
                }

                if (objects.Any())
                {
                    result = await elsaticAgent.SaveObjects(objects);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the document-objects!";
                        return result;
                    }
                }

                if (attributes.Any())
                {
                    result = await elsaticAgent.SaveAttributes(attributes);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the attributes!";
                        return result;
                    }
                }

                if (relations.Any())
                {
                    result = await elsaticAgent.SaveRelations(relations);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the attributes!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<AutoCompleteItem>> GetAutoCompletes(TextParser textParser, List<ParserField> parserFields, string text)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<AutoCompleteItem>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new AutoCompleteItem()
                };

                var elasticAgent = new ServiceAgentElastic(Globals);

                var getAutoCompleteModel = await elasticAgent.GetAutoCompleteModel(textParser.IdParser);

                result.ResultState = getAutoCompleteModel.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var factory = new AutoCompleteFactory(Globals);

                var factoryResult = await factory.GetAutoComplete(getAutoCompleteModel.Result, parserFields, text);
                result.ResultState = factoryResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = factoryResult.Result;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SavePattern(TextParser textParser, string text)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var elasticAgent = new ServiceAgentElastic(Globals);

                if (string.IsNullOrEmpty(text))
                {
                    result = Globals.LState_Nothing.Clone();
                    result.Additional1 = "The query is empty! It will not be saved!";
                    return result;
                }

                var getAutoCompleteModel = await elasticAgent.GetAutoCompleteModel(textParser.IdParser);

                if (getAutoCompleteModel.Result.PatternPatterns.Any(pat => pat.Val_String == text))
                {
                    result = Globals.LState_Nothing;
                    result.Additional1 = "The Pattern is existing and will not be saved!";
                    return result;
                }

                var relationConfig = new clsRelationConfig(Globals);
                var patternObject = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = text.Length > 255 ? text.Substring(0, 254) : text,
                    GUID_Parent = Config.LocalData.Class_Pattern.GUID,
                    Type = Globals.Type_Object
                };
                var saveObjects = new List<clsOntologyItem>
                {
                    patternObject
                };

                var saveRelations = new List<clsObjectRel>
                {
                    relationConfig.Rel_ObjectRelation(new clsOntologyItem
                    {
                        GUID = textParser.IdParser,
                        Name = textParser.NameParser,
                        GUID_Parent = Config.LocalData.Class_Textparser.GUID,
                        Type = Globals.Type_Object
                    },
                    patternObject,
                    Config.LocalData.RelationType_contains
                    )
                };

                var saveAttributes = new List<clsObjectAtt>
                {
                    relationConfig.Rel_ObjectAttribute(patternObject, Config.LocalData.AttributeType_Pattern, text)
                };

                var objectSaveResult = await elasticAgent.SaveObjects(saveObjects);
                result = objectSaveResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Pattern-Object!";
                    return result;
                }

                var attributeSaveResult = await elasticAgent.SaveAttributes(saveAttributes);
                result = attributeSaveResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Pattern-Attribute!";
                    return result;
                }

                var relationSaveResult = await elasticAgent.SaveRelations(saveRelations);
                result = relationSaveResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while relation between Textparser and Pattern!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MeasurementRequest>> GetMeasurementRequestWithExecute(MeasureTextParserRequest request, bool executeMeasure)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<MeasurementRequest>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                request.MessageOutput?.OutputInfo("Validate Request...");

                result.ResultState = ValidationController.ValidateMeasureTextParserRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated Request.");

                request.MessageOutput?.OutputInfo("Get model...");

                var elasticAgent = new ServiceAgentElastic(Globals);

                var getModelResult = await elasticAgent.GetMeasureTextparserModel(request);

                result.ResultState = getModelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                TextParser textParser = null;
                List<ParserField> parserFields = new List<ParserField>();

                if (executeMeasure && getModelResult.Result.ExecuteTextparser.Val_Bit.Value)
                {
                    var executeRequest = new ExecuteTextParserRequest
                    {
                        CancellationToken = request.CancellationToken,
                        DeleteIndex = getModelResult.Result.DeleteTextparser.Val_Bit.Value,
                        IdTextParser = getModelResult.Result.Textparser.GUID,
                        MessageOutput = request.MessageOutput
                    };

                    var executeResult = await ExecuteTextParser(executeRequest);
                    result.ResultState = executeResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    textParser = executeRequest.TextParser;
                    parserFields = executeRequest.TextParserFields;
                }
                else
                {
                    request.MessageOutput?.OutputInfo($"Get Textparser: {getModelResult.Result.Textparser.Name}...");

                    var textParserResult = await GetTextParser(getModelResult.Result.Textparser);

                    result.ResultState = textParserResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo("Have Textparser.");

                    textParser = textParserResult.Result.First();

                    request.MessageOutput?.OutputInfo("Get Textparser-Fields...");
                    var textParserFieldsResult = await GetParserFields(new clsOntologyItem
                    {
                        GUID = textParser.IdFieldExtractor,
                        Name = textParser.NameFieldExtractor,
                        GUID_Parent = textParser.IdClassFieldExtractor,
                        Type = Globals.Type_Object
                    });

                    result.ResultState = textParserFieldsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    parserFields = textParserFieldsResult.Result;
                }

                foreach (var parserField in parserFields)
                {
                    request.MessageOutput?.OutputInfo($"Field: {parserField.NameField}");
                }

                request.MessageOutput?.OutputInfo("Have Textparser-Fields.");

                request.MessageOutput?.OutputInfo("Get Measurement Request...");
                result.Result = MeasureRequestFactory.CreateMeasurementRequest(getModelResult.Result,
                    parserFields,
                    textParser,
                    Globals,
                    request.CancellationToken,
                    request.MessageOutput);
                request.MessageOutput?.OutputInfo("Have Measurement Request.");

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> MeasureTextParser(MeasureTextParserRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var measurementRequest = await GetMeasurementRequestWithExecute(request, true);

                result = measurementRequest.ResultState;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result = await MeasureTextParser(measurementRequest.Result);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> MeasureTextParser(MeasurementRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = Globals.LState_Success.Clone();
                clsAppDocuments lastDocument = null;

                var sortField = request.SortFields.Select(sField => new Nest.SortField { Field = sField.Field.NameField + (sField.Field.DataType.ToLower() == "string" ? ".keyword" : ""), Order = sField.Sort == SortType.ASC ? SortOrder.Ascending : SortOrder.Descending }).ToList();

                var dbReader = new clsUserAppDBSelector(request.Parser.NameServer,
                    request.Parser.Port,
                    request.Parser.NameIndexElasticSearch,
                    Globals.SearchRange,
                    Globals.Session);

                var dbWriter = new clsUserAppDBUpdater(dbReader);

                long count = 0;
                long totalCount = 1;
                string scrollId = null;
                int page = 1;

                var savableMeasureFields = new List<MeasureFieldCalc>();
                var currentRecordCheck = "";
                while (count < totalCount)
                {
                    var documentsResult = dbReader.GetData_Documents(2000,
                        request.Parser.NameIndexElasticSearch,
                        request.Parser.NameEsType,
                        request.Query,
                        page, scrollId, sortField);

                    foreach (var document in documentsResult.Documents)
                    {
                        var openMeasureFields = savableMeasureFields.Where(f => f.EndDocument == null).ToList();
                        if (!document.Dict.ContainsKey(request.RecordSplitField.NameField) || document.Dict[request.RecordSplitField.NameField] == null) continue;
                        if (document.Dict[request.RecordSplitField.NameField].ToString() != currentRecordCheck)
                        {
                            var entryElement = savableMeasureFields.FirstOrDefault(mf => mf.MeasureField.OrderId == 1);

                            var firstElements = (from startEntry in savableMeasureFields
                                                 join endEntry in savableMeasureFields on startEntry.StartDocument equals endEntry.EndDocument into endEntries
                                                 from endEntry in endEntries.DefaultIfEmpty()
                                                 where endEntry == null && startEntry != entryElement
                                                 select startEntry).ToList();
                            if (firstElements.Any())
                            {

                                if (entryElement != null)
                                {
                                    var newElements = firstElements.Select(fe => new MeasureFieldCalc(fe.MeasureField)
                                    {
                                        Start = entryElement.Start,
                                        StartDocument = entryElement.StartDocument,
                                        EndDocument = fe.StartDocument
                                    }).ToList();

                                    foreach (var newElement in newElements)
                                    {
                                        switch (newElement.MeasureField.AggregationType)
                                        {
                                            case AggregationType.AVG:
                                            case AggregationType.Sum:
                                            case AggregationType.Count:
                                                newElement.AddValue(newElement.StartDocument.Dict[newElement.MeasureField.CalculationField.NameField]);
                                                if (newElement.EndDocument != null)
                                                {
                                                    newElement.Calculation(newElement.MeasureField.AggregationType);
                                                }
                                                break;
                                            case AggregationType.TimeDiffDays:
                                            case AggregationType.TimeDiffHours:
                                            case AggregationType.TimeDiffMilliSeconds:
                                            case AggregationType.TimeDiffMinutes:
                                            case AggregationType.TimeDiffMonths:
                                            case AggregationType.TimeDiffSeconds:
                                            case AggregationType.TimeDiffYears:
                                                if (newElement.EndDocument != null)
                                                {
                                                    newElement.Calculation(newElement.MeasureField.AggregationType, (DateTime)newElement.StartDocument.Dict[newElement.MeasureField.CalculationField.NameField]);
                                                }
                                                break;
                                        }
                                    }
                                    savableMeasureFields.AddRange(newElements);
                                }
                            }

                            if (lastDocument != null)
                            {
                                CalculateMeasureFields(lastDocument, request, openMeasureFields, savableMeasureFields, true);
                            }

                            var toSaveMeasureFields = savableMeasureFields.Where(doc => doc.EndDocument != null).ToList();
                            var documentsToSave = toSaveMeasureFields.Select(doc => doc.EndDocument).ToList();
                            if (documentsToSave.Any())
                            {
                                request.MessageOutput?.OutputInfo($"Save {documentsToSave.Count} documents for {currentRecordCheck}");
                                result = dbWriter.SaveDoc(documentsToSave, request.Parser.NameEsType);

                                if (result.GUID == Globals.LState_Error.GUID)
                                {
                                    result.Additional1 = "Error while writing the documents!";
                                    request.MessageOutput?.OutputError(result.Additional1);
                                    return result;
                                }
                                if (SavedMeasureDocs != null)
                                {
                                    result = SavedMeasureDocs(documentsToSave, sender);
                                    if (result.GUID == Globals.LState_Error.GUID)
                                    {
                                        request.MessageOutput?.OutputError(result.Additional1);
                                        return result;
                                    }
                                }
                            }
                            currentRecordCheck = document.Dict[request.RecordSplitField.NameField].ToString();
                            savableMeasureFields.Clear();
                        }
                        CalculateMeasureFields(document, request, openMeasureFields, savableMeasureFields);
                        lastDocument = document;
                    }

                    scrollId = documentsResult.ScrollId;

                    page++;

                    totalCount = documentsResult.TotalCount;
                    count += documentsResult.Documents.Count;
                    if (count > 0 && count % 2000 == 0)
                    {
                        request.MessageOutput?.OutputInfo($"Checked {count} documents.");
                    }
                }

                var documentsToSaveLast = savableMeasureFields.Where(doc => doc.EndDocument != null).Select(doc => doc.EndDocument).ToList();
                if (documentsToSaveLast.Any())
                {
                    request.MessageOutput?.OutputInfo($"Save {documentsToSaveLast.Count} documents for {currentRecordCheck}");
                    result = dbWriter.SaveDoc(documentsToSaveLast, request.Parser.NameEsType);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while writing the documents!";
                        request.MessageOutput?.OutputError(result.Additional1);
                        return result;
                    }
                    if (SavedMeasureDocs != null)
                    {
                        result = SavedMeasureDocs(documentsToSaveLast, sender);
                        if (result.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                    }
                    savableMeasureFields.Clear();
                }

                return result;
            });

            return taskResult;
        }

        private void CalculateMeasureFields(clsAppDocuments document, MeasurementRequest request, List<MeasureFieldCalc> openMeasureFields, List<MeasureFieldCalc> savableMeasureFields, bool closeAll = false)
        {
            foreach (var measureField in request.MeasureFields)
            {
                if (!document.Dict.ContainsKey(measureField.CalculationField.NameField)) continue;
                var openMeasureField = openMeasureFields.FirstOrDefault(f => f.MeasureField.FieldToCheck1.Field.NameField == measureField.FieldToCheck1.Field.NameField);
                var add = (closeAll && openMeasureField != null) ? true : measureField.CheckValue(openMeasureField == null, document.Dict);
                if (add)
                {
                    if (openMeasureField != null)
                    {
                        openMeasureField.EndDocument = document;
                    }
                    else
                    {
                        openMeasureField = new MeasureFieldCalc(measureField);
                        if (measureField.CalculationField.DataType.ToLower() == "datetime")
                        {
                            openMeasureField.Start = (DateTime)document.Dict[measureField.CalculationField.NameField];
                        }
                        openMeasureField.StartDocument = document;
                        savableMeasureFields.Add(openMeasureField);
                    }

                    switch (openMeasureField.MeasureField.AggregationType)
                    {
                        case AggregationType.AVG:
                        case AggregationType.Sum:
                        case AggregationType.Count:
                            openMeasureField.AddValue(document.Dict[measureField.CalculationField.NameField]);
                            if (openMeasureField.EndDocument != null)
                            {
                                openMeasureField.Calculation(measureField.AggregationType);
                            }
                            break;
                        case AggregationType.TimeDiffDays:
                        case AggregationType.TimeDiffHours:
                        case AggregationType.TimeDiffMilliSeconds:
                        case AggregationType.TimeDiffMinutes:
                        case AggregationType.TimeDiffMonths:
                        case AggregationType.TimeDiffSeconds:
                        case AggregationType.TimeDiffYears:
                            if (openMeasureField.EndDocument != null)
                            {
                                openMeasureField.Calculation(openMeasureField.MeasureField.AggregationType, (DateTime)document.Dict[measureField.CalculationField.NameField]);
                            }
                            break;
                        case AggregationType.Occurance:
                            openMeasureField.EndDocument = document;
                            openMeasureField.Set();
                            break;
                    }

                    if (!closeAll &&
                        openMeasureField.EndDocument != null &&
                        openMeasureField.MeasureField.FieldToCheck2 == null &&
                        openMeasureField.MeasureField.AggregationType != AggregationType.Occurance)
                    {
                        openMeasureField = new MeasureFieldCalc(measureField);
                        if (measureField.CalculationField.DataType.ToLower() == "datetime")
                        {
                            openMeasureField.Start = (DateTime)document.Dict[measureField.CalculationField.NameField];
                        }
                        openMeasureField.StartDocument = document;
                        savableMeasureFields.Add(openMeasureField);
                    }
                }



            }
        }

        public TextParserController(Globals globals) : base(globals)
        {
        }

        public TextParserController(Globals globals, object parent) : base(globals)
        {
            sender = parent;
        }
    }


}
