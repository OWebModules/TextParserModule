﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace TextParserModule.Converter
{
    public class MeasureFieldMeasureTypeToAggregationTypeConverter
    {
        public static AggregationType Convert(clsObjectRel measureFieldToMeasureType)
        {
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_AVG.GUID)
            {
                return AggregationType.AVG;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_Sum.GUID)
            {
                return AggregationType.Sum;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_TimeDiffDays.GUID)
            {
                return AggregationType.TimeDiffDays;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_TimeDiffHours.GUID)
            {
                return AggregationType.TimeDiffHours;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_TimeDiffMilliSeconds.GUID)
            {
                return AggregationType.TimeDiffMilliSeconds;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_TimeDiffMinutes.GUID)
            {
                return AggregationType.TimeDiffMinutes;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_TimeDiffMonths.GUID)
            {
                return AggregationType.TimeDiffMonths;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_TimeDiffSeconds.GUID)
            {
                return AggregationType.TimeDiffSeconds;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_TimeDiffYears.GUID)
            {
                return AggregationType.TimeDiffYears;
            }
            if (measureFieldToMeasureType.ID_Other == Measure.Config.LocalData.Object_Occurance.GUID)
            {
                return AggregationType.Occurance;
            }

            return AggregationType.Count;
        }
    }
}
