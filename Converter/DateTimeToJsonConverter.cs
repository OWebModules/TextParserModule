﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Converter
{
    public static class DateTimeToJsonConverter
    {
        public static string Convert(DateTime input)
        {
            var output = input.ToString("yyyy-MM-ddTHH:mm.ss");
            return output;
        }
    }
}
