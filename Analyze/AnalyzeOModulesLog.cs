﻿using ElasticSearchNestConnector;
using Nest;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextParserModule.Converter;
using TextParserModule.Templates;

namespace TextParserModule.Analyze
{
    public class AnalyzeOModulesLogController
    {
        public Globals Globals { get; private set; }

        public clsOntologyItem TextParser => OMOdulesLog.LocalData.Object_OModules_Logs;

        public async Task<clsOntologyItem> Analyze(CancellationToken cancellationToken, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = Globals.LState_Success.Clone();
                var textParserController = new TextParserController(Globals);

                messageOutput?.OutputInfo($"Get the Textparser {OMOdulesLog.LocalData.Object_OModules_Logs.Name}...");
                var textParserResult = await textParserController.GetTextParser(OMOdulesLog.LocalData.Object_OModules_Logs);
                result = textParserResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo("Have the Textparser.");
                messageOutput?.OutputInfo("Get Fields...");

                var textParserFieldsResult = await textParserController.GetParserFields(OMOdulesLog.LocalData.Object_OModules_Logs);
                result = textParserFieldsResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo("Have the Fields.");

                var textParser = textParserResult.Result.First();
                var dbReader = new clsUserAppDBSelector(textParser.NameServer,
                    textParser.Port,
                    textParser.NameIndexElasticSearch,
                    Globals.SearchRange,
                    Globals.Session);

                var dbUpdater = new clsUserAppDBUpdater(dbReader);

                var getNextStart = true;

                long done = 0;

                while (getNextStart)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        result.Additional1 = "User Cancelled!";
                        messageOutput?.OutputInfo(result.Additional1);
                        return result;
                    }
                    var getNextStartResult = GetNextStart(dbReader, textParser.NameIndexElasticSearch);
                    result = getNextStartResult.ResultState;

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    if (getNextStartResult.ResultState.GUID == Globals.LState_Nothing.GUID)
                    {
                        getNextStart = false;
                        break;
                    }
                    
                    var processId = Guid.NewGuid().ToString();
                    var itemsToSave = new List<clsAppDocuments>();
                    itemsToSave.Add(getNextStartResult.Result);

                    

                    var getProcessItemsResult = GetProcessItems(dbReader,
                                textParser.NameIndexElasticSearch,
                                getNextStartResult.Result);

                    result = getProcessItemsResult.ResultState;

                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        messageOutput?.OutputError(result.Additional1);
                        return result;
                    }

                    itemsToSave.AddRange(getProcessItemsResult.Result.OrderBy(doc => doc.Dict["DateTime"]).ThenBy(doc => doc.Dict["Milliseconds"]));

                    double durationMs = 0;
                    var stepCount = itemsToSave.Count;
                    var processError = (itemsToSave.Count == 1);
                    var multisteps = (itemsToSave.Count > 2);
                    DateTime start = DateTime.MinValue;
                    DateTime end = DateTime.MinValue;

                    
                    
                    if (itemsToSave.Any())
                    {
                        var instanceId = string.Empty;
                        var firstItem = itemsToSave.First();
                        var lastItem = itemsToSave.Last();

                        var instanceItems = itemsToSave.Where(itm => itm.Dict.ContainsKey("Instance")).GroupBy(itm => itm.Dict["Instance"].ToString()).ToList();
                        if (instanceItems.Count == 1)
                        {
                            instanceId = instanceItems.First().Key;
                        }

                        start = ((DateTime)firstItem.Dict["DateTime"]).AddMilliseconds((long)firstItem.Dict["Milliseconds"]);
                        end = ((DateTime)lastItem.Dict["DateTime"]).AddMilliseconds((long)lastItem.Dict["Milliseconds"]);
                        durationMs = end.Subtract(start).TotalMilliseconds;
                        DateTime lastTimeStamp = start;
                        for (int i = 0; i < itemsToSave.Count; i++)
                        {
                            var itemToSave = itemsToSave[i];
                            var durationLastStamp = 0d;
                            var dateTime = ((DateTime)lastItem.Dict["DateTime"]).AddMilliseconds((long)lastItem.Dict["Milliseconds"]);
                            if (i>0)
                            {
                                durationLastStamp = dateTime.Subtract(lastTimeStamp).TotalMilliseconds;
                            }

                            var percentage = durationLastStamp == 0 ? 0 : 100.0 / durationMs * durationLastStamp;
                            itemToSave.Dict.Add("ProcessError", processError);
                            itemToSave.Dict.Add("Multistep", multisteps);
                            itemToSave.Dict.Add("StepCount", stepCount);
                            itemToSave.Dict.Add("Duration", durationMs);
                            itemToSave.Dict.Add("DurationLastStamp", durationLastStamp);
                            itemToSave.Dict.Add("StepPercentage", percentage);

                            if (!string.IsNullOrEmpty(instanceId))
                            {
                                if (itemToSave.Dict.ContainsKey("Instance"))
                                {
                                    itemToSave.Dict["Instance"] = instanceId;
                                }
                                else
                                {
                                    itemToSave.Dict.Add("Instance", instanceId);
                                }
                                
                            }
                            itemToSave.Dict.Add("Analyzed", true);
                        }
                    }

                    result = dbUpdater.SaveDoc(itemsToSave, textParser.NameEsType);
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Items with ProcessIds!";
                        messageOutput?.OutputError(result.Additional1);
                        return result;
                    }

                    done += itemsToSave.Count;
                    if (done % 500 == 0)
                    {
                        messageOutput?.OutputInfo($"Saved {done} documents.");
                    }
                }

                messageOutput?.OutputInfo("Analyze finished!");

                return result;
            });

            return taskResult;
        }

        private ResultItem<clsAppDocuments> GetNextStart(clsUserAppDBSelector dbReader, string index)
        {
            var result = new ResultItem<clsAppDocuments>
            {
                ResultState = Globals.LState_Success.Clone()
            };

            var sort = new List<SortField>
                {
                    new SortField
                    {
                        Field = "DateTime",
                        Order = SortOrder.Ascending
                    },
                    new SortField
                    {
                        Field = "Milliseconds",
                        Order = SortOrder.Ascending
                    }
                };
            var query = "Enter:* AND ProcessId:* AND NOT Analyzed:*";
            var searchResult = dbReader.GetData_Documents(1, index, "logs", query, page: 0, sortFields: sort);

            if (!searchResult.IsOK)
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = searchResult.ErrorMessage;
                return result;
            }

            if (searchResult.Documents.Count == 0)
            {
                result.ResultState = Globals.LState_Nothing.Clone();
                return result;
            }

            result.Result = searchResult.Documents.OrderBy(doc => doc.Dict["DateTime"]).ThenBy(doc => doc.Dict["Milliseconds"]).First();

            return result;
        }

        private ResultItem<List<clsAppDocuments>> GetProcessItems(clsUserAppDBSelector dbReader, string index, clsAppDocuments startDoc)
        {
            var result = new ResultItem<List<clsAppDocuments>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<clsAppDocuments>()
            };

            var sort = new List<SortField>
                {
                    new SortField
                    {
                        Field = "DateTime",
                        Order = SortOrder.Ascending
                    },
                    new SortField
                    {
                        Field = "Milliseconds",
                        Order = SortOrder.Ascending
                    }
                };

            var query = $"NOT Enter:* AND ProcessId:\"{startDoc.Dict["ProcessId"]}\"";
            var searchResult = dbReader.GetData_Documents(-1, index, "logs", query, sortFields: sort);

            if (!searchResult.IsOK)
            {
                result.ResultState = Globals.LState_Error.Clone();
                result.ResultState.Additional1 = searchResult.ErrorMessage;
                return result;
            }

            var docs = searchResult.Documents;

            if (docs.Count == 0)
            {
                result.ResultState = Globals.LState_Nothing.Clone();
                return result;
            }

            result.Result = docs;

            return result;
        }

        public AnalyzeOModulesLogController(Globals globals)
        {
            Globals = globals;
        }
    }
    
}
