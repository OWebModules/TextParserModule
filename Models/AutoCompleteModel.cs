﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class AutoCompleteModel
    {
        public List<clsObjectRel> TextParserToPatterns { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> PatternPatterns { get; set; } = new List<clsObjectAtt>();
    }
}
