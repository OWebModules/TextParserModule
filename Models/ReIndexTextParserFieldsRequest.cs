﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ReIndexTextParserFieldsRequest
    {
        public string IdTextParser { get; set; }

        public List<string> IdsFields { get; set; } = new List<string>();

        public string Query { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        public CancellationTokenSource CancellationToken { get; set; }

        public NumberSeperator NumberSeperator { get; private set; }

        public ReIndexTextParserFieldsRequest(string idTextParser, string query, CancellationTokenSource cancellationToken)
        {
            IdTextParser = idTextParser;
            CancellationToken = cancellationToken;
            NumberSeperator = new NumberSeperator();
            var testTxt = "8.4";
            double testDbl = double.Parse(testTxt);
            if (testDbl == 8.4)
            {
                NumberSeperator.Seperator = ".";
                NumberSeperator.SeperatorReplace = ",";
            }
            else
            {
                NumberSeperator.Seperator = ",";
                NumberSeperator.SeperatorReplace = ".";
            }

            Query = string.IsNullOrEmpty(query) ? "*" : query;
        }
    }
}
