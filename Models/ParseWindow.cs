﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ParseWindow
    {
        private Globals globals;
        private ParserField startField;
        public ParserField StartField
        {
            get { return startField; }
            set
            {
                startField = value;
                if (startField != null)
                {
                    SetRegex(startField, true);
                }
            }
        }

        private ParserField endField;
        public ParserField EndField
        {
            get { return endField; }
            set
            {
                endField = value;
                SetRegex(startField, false);
            }
        }

        public string RegexPre_Start { get; set; }
        public string RegexMain_Start { get; set; }
        public string RegexPost_Start { get; set; }

        public string RegexPre_End { get; set; }
        public string RegexMain_End { get; set; }
        public string RegexPost_End { get; set; }

        public bool StartChecked { get; set; }
        public bool EndChecked { get; set; }

        

        public bool CheckStart
        {
            get
            {
                return StartField != null;
            }

        }

        public bool CheckEnd
        {
            get
            {
                return EndField != null;
            }
        }

        public bool DoParsing { get; set; }

        private void SetRegex(ParserField field, bool isStartField)
        {
            string regexPre = field.IdRegexPre != null &&
                                       field.IdRegexPre != Config.LocalData.Object_empty.GUID
                                       ? field.RegexPre : null;
            string regexPost = field.IdRegexPost != null &&
                               field.IdRegexPost != Config.LocalData.Object_empty.GUID
                ? field.RegexPost
                : null;

            string regexMain = field.IdRegexMain != null &&
                               field.IdRegexMain != Config.LocalData.Object_empty.GUID
                               ? field.RegexMain : null;

            if (isStartField)
            {
                RegexPre_Start = regexPre;
                RegexMain_Start = regexMain;
                RegexPost_Start = regexPost;
            }
            else
            {
                RegexPre_End = regexPre;
                RegexMain_End = regexMain;
                RegexPost_End = regexPost;
            }
        }

        public ParseWindow(Globals globals)
        {
            this.globals = globals;
        }
    }
}
