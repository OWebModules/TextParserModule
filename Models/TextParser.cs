﻿using ElasticSearchConfigModule.Models;
using FileResourceModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class TextParser
    {
        public string IdParser { get; set; }
        public string NameParser { get; set; }
        public string IdEntryValueParser { get; set; }
        public string NameEntryValueParser { get; set; }
        public string IdClassEntryValueParser { get; set; }
        public string IdFieldExtractor { get; set; }
        public string NameFieldExtractor { get; set; }
        public string IdClassFieldExtractor { get; set; }
        public string IdFileResource
        {
            get
            {
                return fileResource?.IdFileResource;
            }
        }
        public string NameFileResource
        {
            get
            {
                return fileResource?.NameFileResource;
            }
        }

        public string PatternFileResource
        {
            get
            {
                return fileResource?.Pattern;
            }
        }

        public bool SubitemsFileResource
        {
            get
            {
                if (fileResource != null)
                {
                    return fileResource.SubItems;
                }
                else
                {
                    return false;
                }
            }
        }
        public string IdIndexElasticSearch
        {
            get
            {
                return indexConfig?.IdIndex;
            }
        }
        public string NameIndexElasticSearch
        {
            get
            {
                return indexConfig?.NameIndex;
            }
        }

        public string IdServer
        {
            get
            {
                return indexConfig?.IdServer;
            }
        }

        public string NameServer
        {
            get
            {
                return indexConfig?.NameServer;
            }
        }

        public int Port
        {
            get
            {
                if (indexConfig != null)
                {
                    return indexConfig.Port;
                }
                else
                {
                    return 0;
                }
            }
        }
        public List<TextSeperator> TextSeperators { get; set; }
        public string IdUser { get; set; }
        public string NameUser { get; set; }
        public string IdEsType
        {
            get
            {
                return indexConfig?.IdType;
            }
        }
        public string NameEsType
        {
            get
            {
                return indexConfig?.NameType;
            }
        }
        public List<string> IdFields { get; set; }
        public string IdFieldParent { get; set; }
        public string NameFieldParent { get; set; }
        public List<ConfigurationItem> ConfigurationItems { get; set; }
        public List<TextParser> SubParsers { get; set; }
        public string IdCommandLine { get; set; }
        public string NameCommandLine { get; set; }
        public bool IsCSVParser { get; set; }

        private FileResource fileResource;
        public FileResource FileResource
        {
            get
            {
                return fileResource;
            }
        }
        private ElasticSearchConfig indexConfig;

        public TextParser(FileResource fileResource, ElasticSearchConfig indexConfig)
        {
            this.fileResource = fileResource;
            this.indexConfig = indexConfig;
        }
    }
}
