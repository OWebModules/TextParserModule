﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ReIndexPatternSourceFieldsRequest
    {
        public string IdTextParser { get; private set; }

        public CancellationToken CancellationToken { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public ReIndexPatternSourceFieldsRequest(string idTextParser, CancellationToken cancellationToken)
        {
            IdTextParser = idTextParser;
            CancellationToken = cancellationToken;
        }
    }
}
