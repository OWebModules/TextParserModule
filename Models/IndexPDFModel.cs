﻿using ElasticSearchConfigModule.Models;
using MediaViewerModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class IndexPDFModel
    {
        public clsOntologyItem IndexPDFConfig { get; set; }
        public List<clsObjectRel> SessionPDFs { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SessionPDFsNotExisting { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SessionPDFsError { get; set; } = new List<clsObjectRel>();
        public List<MediaListItem> PDFDocuments { get; set; } = new List<MediaListItem>();
        public ElasticSearchConfig ElasticSearchConfig { get; set; }

        public clsObjectRel ConfigToPath { get; set; }
    }
}
