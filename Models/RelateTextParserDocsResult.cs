﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class RelateTextParserDocsResult
    {
        public List<clsOntologyItem> DocumentReferences { get; set; }
    }

}
