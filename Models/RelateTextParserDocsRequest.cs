﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class RelateTextParserDocsRequest
    {
        public List<string> IdsDocuments { get; set; } = new List<string>();
        public string Query { get; set; }
        public TextParser TextParser { get; private set; }
        public List<ParserField> ParserFields { get; private set; } = new List<ParserField>();

        public IMessageOutput MessageOutput { get; set; }

        public RelateTextParserDocsRequest(TextParser textParser, List<ParserField> parserFields)
        {
            TextParser = textParser;
            ParserFields = parserFields;
        }
    }
}
