﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ExecuteTextParserRequest
    {
        public string IdTextParser { get; set; }
        public TextParser TextParser { get; set; }
        public List<ParserField> TextParserFields { get; set; } = new List<ParserField>();

        public bool DeleteIndex { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        public CancellationToken CancellationToken { get; set; }
    }
}
