﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class MeasureTextparserModel
    {
        public clsOntologyItem Config { get; set; }

        public clsObjectAtt DeleteTextparser { get; set; }

        public clsObjectAtt ExecuteTextparser { get; set; }

        public clsOntologyItem SplitField { get; set; }

        public List<clsObjectRel> ConfigToMeasureFields { get; set; } = new List<clsObjectRel>();

        public clsOntologyItem BasePattern { get; set; }

        public clsObjectAtt PatternText { get; set; }

        public List<clsObjectRel> ConfigToSortFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> SortFieldsToFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> SortFieldsToSort { get; set; } = new List<clsObjectAtt>();

        public clsOntologyItem Textparser { get; set; }

        public List<clsObjectRel> MeasureFieldsToClacFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> MeasureFieldsToDestFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> MeasureFieldsToCheckFieldValues { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> MeasureFieldsToMeasureTypes { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> FieldValuesToFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> FieldValuesValues { get; set; } = new List<clsObjectAtt>();

    }
}
