﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class RegexReplace
    {
        public string IdField { get; set; }
        public string IdFieldReplace { get; set; }
        public string NameFiledReplace { get; set; }
        public string IdAttributeText { get; set; }
        public string IdRegEx { get; set; }
        public string IdAttributeRegex { get; set; }
        public string RegExSearch { get; set; }
        public string ReplaceWith { get; set; }

        public long OrderId { get; set; }
    }
}
