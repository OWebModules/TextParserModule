﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public static class PDFIndex
    {
        public static string FieldId => "PdfId";
        public static string FieldPage => "Page";
        public static string FieldContent => "Content";

    }
}
