﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ReIndexPatternSourceFieldsModel
    {
        public List<ParserField> PatternSourceFields { get; set; } = new List<ParserField>();
        public List<clsObjectRel> PatternSourcesToFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> PatternSourceToPattern { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> PatternPattern { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectAtt> PatternSourceValues { get; set; } = new List<clsObjectAtt>();
    }
}
