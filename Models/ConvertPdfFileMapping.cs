﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ConvertPdfFileMapping
    {
        public string SrcFileName { get; set; }
        public string DstFileName { get; set; }
    }
}
