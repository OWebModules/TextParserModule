﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ReIndexPatternSourceFieldsByTextparserRequest
    {
        public TextParser TextParser { get; private set; }
        public List<ParserField> ParserFields { get; private set; } = new List<ParserField>();

        public CancellationToken CancellationToken { get; private set; }

        public IMessageOutput MessageOutput { get; set; }

        public ReIndexPatternSourceFieldsByTextparserRequest(TextParser textParser, List<ParserField> parserFields, CancellationToken cancellationToken)
        {
            TextParser = textParser;
            ParserFields = parserFields;
            CancellationToken = cancellationToken;
        }
    }
}
