﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using TextParserModule.Helper;
using TextParserModule.Models;

namespace TextParserModule.Models
{
    public enum AggregationType
    {
        TimeDiffMilliSeconds = 0,
        TimeDiffSeconds = 1,
        TimeDiffMinutes = 2,
        TimeDiffHours = 3,
        TimeDiffDays = 4,
        TimeDiffMonths = 5,
        TimeDiffYears = 6,
        Sum = 7,
        AVG = 8,
        Count = 9,
        Occurance = 10,
    }

    public class MeasurementRequest
    {
        public TextParser Parser { get; set; }
        public List<ParserField> Fields { get; set; } = new List<ParserField>();
        public string Query { get; set; }
        public List<SortField> SortFields { get; set; } = new List<SortField>();
        public List<MeasureField> MeasureFields { get; set; } = new List<MeasureField>();
        public ParserField RecordSplitField { get; set; }

        public CancellationToken CancellationToken { get; set; }

        public IMessageOutput MessageOutput { get; set; }
    }

    public enum SortType
    {
        ASC = 0,
        DESC = 1
    }
    public class SortField
    {
        public ParserField Field { get; set; }
        public SortType Sort { get; set; }
    }

    public class MeasureField
    {
        public FieldValue FieldToCheck1 { get; set; }
        public FieldValue FieldToCheck2 { get; set; }
        public ParserField CalculationField { get; set; }
        public ParserField FieldToOutput { get; set; }
        public AggregationType AggregationType { get; set; }

        public long OrderId { get; set; }

        public bool CheckValue(bool start, Dictionary<string, object> document)
        {
            FieldValue fieldValueToCheck = null;
            if (start ||
                (!start && FieldToCheck2 == null))
            {
                fieldValueToCheck = FieldToCheck1;
            }
            else
            {
                fieldValueToCheck = FieldToCheck2;
            }
            if (fieldValueToCheck == null) return false;

            if (!document.ContainsKey(fieldValueToCheck.Field.NameField)) return false;
            var valueChecking = document[fieldValueToCheck.Field.NameField];
            if (valueChecking == null) return false;
            if (fieldValueToCheck.Value == null)
            {
                return true;
            }
            else
            {
                return ValueCompareHelper.Compare(valueChecking, fieldValueToCheck.Value);
            }
        }
    }

    
    public class FieldValue
    {
        public ParserField Field { get; set; }

        public clsObjectAtt Attribute { get; set; }
        public object Value { get; set; }
    }

}
