﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ConfigurationItem
    {
        public string IdConfigurationItem { get; set; }
        public string NameConfigurationItem { get; set; }
    }
}
