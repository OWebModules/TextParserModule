﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class IndexPDFResult
    {
        public clsObjectAtt SessionStamp { get; set; }
        public List<ResultItem<clsOntologyItem>> IndexedResult { get; set; } = new List<ResultItem<clsOntologyItem>>();
    }
}
