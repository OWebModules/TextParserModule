﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ConvertPdfToTextModel
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectAtt FolderConvert { get; set; }
        public clsObjectRel ConfigToSrcPath { get; set; }
        public clsObjectRel ConfigToDstPath { get; set; }
    }
}
