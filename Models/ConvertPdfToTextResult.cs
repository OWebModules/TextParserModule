﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ConvertPdfToTextResult
    {
        public ConvertPdfFileMapping FileMapping { get; set; }
        public clsOntologyItem Result { get; set; }
    }
}
