﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ConvertPdfToTextRequest
    {
        public IMessageOutput MessageOutput { get; set; }
        public string IdConfig { get; private set; }

        public ConvertPdfToTextRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
