﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ParserField
    {
        public long OrderId { get; set; }
        public string IdFieldParser { get; set; }
        public string NameFieldParser { get; set; }
        public string IdField { get; set; }
        public string NameField { get; set; }
        public string IdMetaField { get; set; }
        public string NameMetaField { get; set; }
        public bool IsMeta { get; set; }
        public bool IsInsert { get; set; }
        public string IdRegexPre { get; set; }
        public string IdAttributeRegexPre { get; set; }
        public string RegexPre { get; set; }
        public string IdRegexMain { get; set; }
        public string IdAttributeRegexMain { get; set; }
        public string RegexMain { get; set; }
        public string IdRegexPost { get; set; }
        public string IdAttributeRegexPost { get; set; }
        public string RegexPost { get; set; }
        public string Insert { get; set; }
        public string IdDataType { get; set; }
        public string DataType { get; set; }
        public string IdAttributeUseOrderId { get; set; }
        public bool UseOrderId { get; set; }
        public string IdAttributeRemoveFromSource { get; set; }
        public bool RemoveFromSource { get; set; }
        public string IdAttributeUseLastField { get; set; }
        public bool UseLastValid { get; set; }
        public object LastValid { get; set; }
        public string IdReferenceField { get; set; }
        public string NameReferenceField { get; set; }
        public string LastContent { get; set; }
        public object CustomContent { get; set; }
        public string IdAttributeDoAll { get; set; }
        public bool DoAll { get; set; }
        public List<RegexReplace> ReplaceList { get; set; }
        public List<ParserField> FieldListContained { get; set; }
        public bool Start { get; set; }
        public bool End { get; set; }
        public bool HtmlDecode { get; set; }
        public string IdCSVField { get; set; }
        public string NameCSVField { get; set; }

        public string IdTwoLetterIsoName { get; set; }
        public string NameTwoLetterIsoName { get; set; }

        public CultureInfo CultureInfo { get; set; }
        public MergeField MergeField { get; set; }
        public bool IsMergeField
        {
            get
            {
                return MergeField != null;
            }
        }

        public ParserField Clone()
        {
            return new ParserField
            {
                DataType = DataType,
                RegexMain = RegexMain,
                DoAll = DoAll,
                IdAttributeDoAll = IdAttributeDoAll,
                IdAttributeRegexMain = IdAttributeRegexMain,
                IdAttributeRegexPost = IdAttributeRegexPost,
                IdAttributeRegexPre = IdAttributeRegexPre,
                IdAttributeRemoveFromSource = IdAttributeRemoveFromSource,
                IdAttributeUseLastField = IdAttributeUseLastField,
                IdAttributeUseOrderId = IdAttributeUseOrderId,
                IdDataType = IdDataType,
                IdField = IdField,
                IdFieldParser = IdFieldParser,
                IdMetaField = IdMetaField,
                IdReferenceField = IdReferenceField,
                IdRegexMain = IdRegexMain,
                IdRegexPost = IdRegexPost,
                IdRegexPre = IdRegexPre,
                Insert = Insert,
                IsInsert = IsInsert,
                IsMeta = IsMeta,
                LastContent = LastContent,
                LastValid = LastValid,
                NameField = NameField,
                NameFieldParser = NameFieldParser,
                NameMetaField = NameMetaField,
                NameReferenceField = NameReferenceField,
                OrderId = OrderId,
                RegexPost = RegexPost,
                RegexPre = RegexPre,
                RemoveFromSource = RemoveFromSource,
                UseLastValid = UseLastValid,
                UseOrderId = UseOrderId,
                ReplaceList = ReplaceList,
                FieldListContained = FieldListContained,
                Start = Start,
                End = End,
                HtmlDecode = HtmlDecode,
                IdCSVField = IdCSVField,
                NameCSVField = NameCSVField,
                MergeField = MergeField
            };
        }
    }
}
