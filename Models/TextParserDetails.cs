﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace TextParserModule.Models
{
    public class TextParserDetails
    {
        public List<clsOntologyItem> TextParserDetailsItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> TextParserDetailsAttributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> TextParserDetailsToTextParserFields { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> TextParserDetailsToTextParsers { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> ConfigsToNameTemplate { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> ConfigToTextparser { get; set; } = new List<clsObjectRel>();
    }
}
