﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class MergeField
    {
        public List<ParserField> FieldList { get; set; }
        public string IdSeperator
        {
            get
            {
                return SeperatorRelItem?.ID_Other;
            }
        }
        public string Seperator
        {
            get
            {
                return SeperatorRelItem?.Name_Other;
            }
        }

        public clsObjectRel SeperatorRelItem { get; set; }

        public string Value(Dictionary<string, object> fieldsWithValues)
        {
            var fieldValues = (from field in FieldList
                               join dictField in fieldsWithValues on field.NameField equals dictField.Key
                               select dictField.Value == null ? "" : dictField.Value.ToString()).ToList();
            return string.Join(Seperator, fieldValues);
        }
    }
}
