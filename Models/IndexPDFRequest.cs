﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class IndexPDFRequest
    {
        public string IdConfig { get; private set; }
        public CancellationToken CancellationToken { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public IndexPDFRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
