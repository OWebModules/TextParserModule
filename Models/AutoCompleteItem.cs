﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class AutoCompleteItem
    {
        public string query { get; set; }
        public List<AutoCompleteSubItem> suggestions { get; set; } = new List<AutoCompleteSubItem>();
    }

    public class AutoCompleteSubItem
    {
        public string value { get; set; }
        public string data { get; set; }
    }
}
