﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class FieldReplace
    {
        public string IdField { get; set; }
        public List<RegexReplace> ReplaceList { get; set; }
    }
}
