﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class RegexField
    {
        public string IdField { get; set; }
        public string IdRegex { get; set; }
        public string IdAttribute { get; set; }
        public string Regex { get; set; }
    }
}
