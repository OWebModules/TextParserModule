﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class MeasureFieldCalc
    {
        public MeasureField MeasureField { get; private set; }
        public clsAppDocuments StartDocument { get; set; }
        public clsAppDocuments EndDocument { get; set; }
        public double AggregationValue { get; set; }
        public DateTime Start { get; set; }
        
        public List<double> DoubleValues { get; private set; }
        public List<long> LongValues { get; private set; }

        public void AddValue(object value)
        {
            if (value is long)
            {
                LongValues.Add((long)value);
            }
            else if (value is double)
            {
                DoubleValues.Add((double)value);
            }
        }

        public void Calculation(AggregationType aggregationType, DateTime calcValue)
        {
            switch (aggregationType)
            {
                case AggregationType.TimeDiffMilliSeconds:
                    AggregationValue = (calcValue - Start).TotalMilliseconds;
                    break;
                case AggregationType.TimeDiffSeconds:
                    AggregationValue = (calcValue - Start).TotalSeconds;
                    break;
                case AggregationType.TimeDiffMinutes:
                    AggregationValue = (calcValue - Start).TotalMinutes;
                    break;
                case AggregationType.TimeDiffHours:
                    AggregationValue = (calcValue - Start).TotalHours;
                    break;
                case AggregationType.TimeDiffDays:
                    AggregationValue = (calcValue - Start).TotalDays;
                    break;
                case AggregationType.TimeDiffYears:
                    AggregationValue = (calcValue - Start).TotalDays / 365;
                    break;
            }
            if (EndDocument.Dict.ContainsKey(MeasureField.FieldToOutput.NameField))
            {
                EndDocument.Dict[MeasureField.FieldToOutput.NameField] = AggregationValue;
            }
            else
            {
                EndDocument.Dict.Add(MeasureField.FieldToOutput.NameField, AggregationValue);
            }
        }

        public void Calculation(AggregationType aggregationType)
        {
            switch(aggregationType)
            {
                case AggregationType.AVG:
                    if (LongValues.Any())
                    {
                        AggregationValue = LongValues.Average(lng => (long)lng);
                    }
                    else
                    {
                        AggregationValue = DoubleValues.Average(lng => (double)lng);
                    }
                    break;
                case AggregationType.Count:
                    if (LongValues.Any())
                    {
                        AggregationValue = LongValues.Count();
                    }
                    else
                    {
                        AggregationValue = DoubleValues.Count();
                    }
                    break;
                case AggregationType.Sum:
                    if (LongValues.Any())
                    {
                        AggregationValue = LongValues.Sum(val => (long)val);
                    }
                    else
                    {
                        AggregationValue = DoubleValues.Sum(val => (double)val);
                    }
                    break;
            }

            if (EndDocument.Dict.ContainsKey(MeasureField.FieldToOutput.NameField))
            {
                EndDocument.Dict[MeasureField.FieldToOutput.NameField] = AggregationValue;
            }
            else
            {
                EndDocument.Dict.Add(MeasureField.FieldToOutput.NameField, AggregationValue);
            }
        }


        public void Set()
        {
            if (EndDocument.Dict.ContainsKey(MeasureField.FieldToOutput.NameField))
            {
                EndDocument.Dict[MeasureField.FieldToOutput.NameField] = true;
            }
            else
            {
                EndDocument.Dict.Add(MeasureField.FieldToOutput.NameField, true);
            }
        }
        public MeasureFieldCalc(MeasureField measureField)
        {
            MeasureField = measureField;
        }
    }
}
