﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TextParserModule.Models
{
    public class ParseTextRequest
    {
        public ParseResult ParseResult { get; set; } = new ParseResult();
        public TextParser TextParser { get; set; }
        public List<ParserField> ParserFields { get; set; } = new List<ParserField>();

        public ParseWindow ParseWindow { get; set; }
        public long DocCount { get; set; }
        public List<clsAppDocuments> DocList { get; set; } = new List<clsAppDocuments>();
        public string Text { get; set; }
        public bool ReplaceNewLine { get; set; }
        public long FileLine { get; set; }

        public clsOntologyItem ParentField { get; set; }

        public FileMeta FileMeta { get; set; }

        public List<ParserField> UserFields { get; set; } = new List<ParserField>();

        public CancellationToken CancellationToken { get; set; }

        public IMessageOutput MessageOutput { get; set; }

        private NumberSeperator numberSeperator;
        public NumberSeperator NumberSeperator
        {
            get
            {
                if (numberSeperator == null)
                {
                    numberSeperator = new NumberSeperator();
                    var testTxt = "8.4";
                    double testDbl = double.Parse(testTxt);
                    if (testDbl == 8.4)
                    {
                        numberSeperator.Seperator = ".";
                        numberSeperator.SeperatorReplace = ",";
                    }
                    else
                    {
                        numberSeperator.Seperator = ",";
                        numberSeperator.SeperatorReplace = ".";
                    }
                }
                return numberSeperator;
            }
            

        }
    }

    public class NumberSeperator
    {
        public string Seperator { get; set; }
        public string SeperatorReplace { get; set; }
    }
}
