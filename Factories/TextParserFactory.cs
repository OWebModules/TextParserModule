﻿using ElasticSearchConfigModule.Models;
using FileResourceModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;
using TextParserModule.Services;

namespace TextParserModule.Factories
{
    public class TextParserFactory
    {
        public async Task<ResultItem<List<TextParser>>> CreateParserList(TextParserRaw textParserRaw, FileResource fileResourceRaw, ElasticSearchConfig indexConfigRaw, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<TextParser>>>(() =>
            {
                var result = new ResultItem<List<TextParser>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<TextParser>()
                };

                

                result.Result = (from textParser in textParserRaw.TextParserList
                                 join entryValueParser in textParserRaw.TextParserLeftRight.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Entry_Value_Parser.GUID) on textParser.GUID equals entryValueParser.ID_Object into entryValueParsers
                                 from entryValueParser in entryValueParsers.DefaultIfEmpty()
                                 join fieldExtractorParser in textParserRaw.TextParserLeftRight.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Field_Extractor_Parser.GUID) on textParser.GUID equals fieldExtractorParser.ID_Object into fieldExtractorParsers
                                 from fieldExtractorParser in fieldExtractorParsers.DefaultIfEmpty()
                                 join isCSVParserAttribute in textParserRaw.TextParsersAtt.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_IsCSV_Importer.GUID) on textParser.GUID equals isCSVParserAttribute.ID_Object into isCSVParserAttributes
                                 from isCSVParserAttribute in isCSVParserAttributes.DefaultIfEmpty()
                                 select new TextParser(fileResourceRaw, indexConfigRaw)
                                 {
                                     IdParser = textParser.GUID,
                                     NameParser = textParser.Name,
                                     IdEntryValueParser = entryValueParser?.ID_Other,
                                     NameEntryValueParser = entryValueParser?.Name_Other,
                                     IdClassEntryValueParser = entryValueParser != null ? Config.LocalData.Class_Entry_Value_Parser.GUID : null,
                                     IdFieldExtractor = fieldExtractorParser?.ID_Other,
                                     NameFieldExtractor = fieldExtractorParser?.Name_Other,
                                     IdClassFieldExtractor = fieldExtractorParser != null ? Config.LocalData.Class_Field_Extractor_Parser.GUID : null,
                                     IsCSVParser = isCSVParserAttribute != null ? isCSVParserAttribute.Val_Bit.Value: false
                                 }).ToList();

                foreach (var textParser in result.Result)
                {
                    textParser.TextSeperators = textParserRaw.TextParserLeftRight.Where(rel => rel.ID_RelationType == Config.LocalData.ClassRel_Textparser_Line_seperator_Text_Seperators.ID_RelationType &&
                        rel.ID_Parent_Other == Config.LocalData.ClassRel_Textparser_Line_seperator_Text_Seperators.ID_Class_Right && rel.ID_Object == textParser.IdParser)
                        .Select(rel => new TextSeperator { IdTextSeperator = rel.ID_Other, NameTextSeperator = rel.Name_Other }).ToList();
                }
                return result;
            });

            return taskResult;
        }
    }
}
