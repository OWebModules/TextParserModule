﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace TextParserModule.Factories
{
    public static class MeasureRequestFactory
    {
        public static MeasurementRequest CreateMeasurementRequest(MeasureTextparserModel model, 
            List<ParserField> parserFields, 
            TextParser textParser, 
            Globals globals,
            CancellationToken cancellationToken,
            IMessageOutput messageOutput)
        {

            var measureFields = (from measureField in model.ConfigToMeasureFields
                                 join measureFieldToCalcField in model.MeasureFieldsToClacFields on measureField.ID_Other equals measureFieldToCalcField.ID_Object
                                 join calcField in parserFields on measureFieldToCalcField.ID_Other equals calcField.IdField
                                 join measureFieldToDestField in model.MeasureFieldsToDestFields on measureField.ID_Other equals measureFieldToDestField.ID_Object
                                 join destField in parserFields on measureFieldToDestField.ID_Other equals destField.IdField
                                 join measureFieldToMeasureType in model.MeasureFieldsToMeasureTypes on measureField.ID_Other equals measureFieldToMeasureType.ID_Object
                                 select new MeasureField
                                 {
                                     CalculationField = calcField,
                                     FieldToOutput = destField,
                                     AggregationType = Converter.MeasureFieldMeasureTypeToAggregationTypeConverter.Convert(measureFieldToMeasureType),
                                     FieldToCheck1 = (from fieldValue in model.MeasureFieldsToCheckFieldValues.Where(fieldValue => fieldValue.ID_Object == measureField.ID_Other && fieldValue.OrderID == 1)
                                                      join fieldValueToField in model.FieldValuesToFields on fieldValue.ID_Other equals fieldValueToField.ID_Object
                                                      join field in parserFields on fieldValueToField.ID_Other equals field.IdField
                                                      join attribute in model.FieldValuesValues on fieldValue.ID_Other equals attribute.ID_Object into attributes
                                                      from attribute in attributes.DefaultIfEmpty()
                                                      select new FieldValue { Field = field, Value = null, Attribute = attribute }).First(),
                                     FieldToCheck2 = (from fieldValue in model.MeasureFieldsToCheckFieldValues.Where(fieldValue => fieldValue.ID_Object == measureField.ID_Other && fieldValue.OrderID == 2)
                                                      join fieldValueToField in model.FieldValuesToFields on fieldValue.ID_Other equals fieldValueToField.ID_Object
                                                      join field in parserFields on fieldValueToField.ID_Other equals field.IdField
                                                      join attribute in model.FieldValuesValues on fieldValue.ID_Other equals attribute.ID_Object
                                                      select new FieldValue { Field = field, Attribute = attribute }).FirstOrDefault(),
                                     OrderId = measureField.OrderID.Value

                                 }).ToList();

            foreach (var measureField in measureFields)
            {
                if (measureField.FieldToCheck1.Attribute != null && measureField.FieldToCheck1.Attribute.ID_DataType == globals.DType_Bool.GUID)
                {
                    measureField.FieldToCheck1.Value = measureField.FieldToCheck1.Attribute.Val_Bit;
                }
                else if (measureField.FieldToCheck1.Attribute != null && measureField.FieldToCheck1.Attribute.ID_DataType == globals.DType_DateTime.GUID)
                {
                    measureField.FieldToCheck1.Value = measureField.FieldToCheck1.Attribute.Val_Datetime;
                }
                else if (measureField.FieldToCheck1.Attribute != null && measureField.FieldToCheck1.Attribute.ID_DataType == globals.DType_Int.GUID)
                {
                    measureField.FieldToCheck1.Value = measureField.FieldToCheck1.Attribute.Val_Lng;
                }
                else if (measureField.FieldToCheck1.Attribute != null && measureField.FieldToCheck1.Attribute.ID_DataType == globals.DType_Real.GUID)
                {
                    measureField.FieldToCheck1.Value = measureField.FieldToCheck1.Attribute.Val_Double;
                }
                else if (measureField.FieldToCheck1.Attribute != null)
                {
                    measureField.FieldToCheck1.Value = measureField.FieldToCheck1.Attribute.Val_String;
                }

                if (measureField.FieldToCheck2 != null && measureField.FieldToCheck2.Attribute != null && measureField.FieldToCheck2.Attribute.ID_DataType == globals.DType_Bool.GUID)
                {
                    measureField.FieldToCheck2.Value = measureField.FieldToCheck2.Attribute.Val_Bit;
                }
                else if (measureField.FieldToCheck2 != null && measureField.FieldToCheck2.Attribute != null && measureField.FieldToCheck2.Attribute.ID_DataType == globals.DType_DateTime.GUID)
                {
                    measureField.FieldToCheck2.Value = measureField.FieldToCheck2.Attribute.Val_Datetime;
                }
                else if (measureField.FieldToCheck2 != null && measureField.FieldToCheck2.Attribute != null && measureField.FieldToCheck2.Attribute.ID_DataType == globals.DType_Int.GUID)
                {
                    measureField.FieldToCheck2.Value = measureField.FieldToCheck2.Attribute.Val_Lng;
                }
                else if (measureField.FieldToCheck2 != null && measureField.FieldToCheck2.Attribute != null && measureField.FieldToCheck2.Attribute.ID_DataType == globals.DType_Real.GUID)
                {
                    measureField.FieldToCheck2.Value = measureField.FieldToCheck2.Attribute.Val_Double;
                }
                else if (measureField.FieldToCheck2 != null && measureField.FieldToCheck2.Attribute != null)
                {
                    measureField.FieldToCheck2.Value = measureField.FieldToCheck2.Attribute.Val_String;
                }
            }


            var measurementRequest = new MeasurementRequest
            {
                Fields = parserFields,
                Parser = textParser,
                Query = model.PatternText.Val_String,
                SortFields = (from sortField in model.ConfigToSortFields.OrderBy(cTSf => cTSf.OrderID)
                              join sortFieldToField in model.SortFieldsToFields on sortField.ID_Other equals sortFieldToField.ID_Object
                              join field in parserFields on sortFieldToField.ID_Other equals field.IdField
                              join sort in model.SortFieldsToSort on sortField.ID_Other equals sort.ID_Object
                              select new Models.SortField { Field = field, Sort = sort.Val_Bit.Value ? SortType.ASC : SortType.DESC }).ToList(),
                MeasureFields = measureFields,
                RecordSplitField = parserFields.First(field => field.IdField == model.SplitField.GUID),
                CancellationToken = cancellationToken,
                MessageOutput = messageOutput
            };

            return measurementRequest;
        }
    }
}
