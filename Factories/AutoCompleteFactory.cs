﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace TextParserModule.Factories
{
    public class AutoCompleteFactory
    {
        private Globals globals;
        public async Task<ResultItem<AutoCompleteItem>> GetAutoComplete(AutoCompleteModel model, List<ParserField> parserFields, string query)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<AutoCompleteItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = new AutoCompleteItem
                {
                    query = query,
                    suggestions = (from pattern in model.TextParserToPatterns
                                   join patternRegex in model.PatternPatterns on pattern.ID_Other equals patternRegex.ID_Object
                                   where patternRegex.Val_String.ToLower().Contains(query.ToLower())
                                   select new AutoCompleteSubItem
                                   {
                                       value = patternRegex.Val_String,
                                       data = pattern.ID_Other
                                   }).ToList()
                };

                result.Result.suggestions.AddRange(parserFields.Where(field => field.NameField.ToLower().Contains(query.ToLower())).Select(field => new AutoCompleteSubItem
                {
                    value = $"{field.NameField}:",
                    data = field.IdField
                }));

                return result;
            });

            return taskResult;
        }

        public AutoCompleteFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
