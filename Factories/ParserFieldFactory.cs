﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;
using TextParserModule.Services;

namespace TextParserModule.Factories
{
    public class ParserFieldFactory
    {
        public async Task<ResultItem<List<ParserField>>> CreateParserFieldList(ParserFieldsRaw parserFieldsRaw, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<ParserField>>>(() =>
            {
                var result = new ResultItem<List<ParserField>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<ParserField>()
                };


                var csvFields = parserFieldsRaw.FieldRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_CSV_Field.GUID).ToList();

                var regexMain = (from objRegExMain in parserFieldsRaw.FieldRels.
                                                                Where(
                                                                    r =>
                                                                    r.ID_RelationType == Config.LocalData.RelationType_Main.GUID &&
                                                                    r.ID_Parent_Other == Config.LocalData.Class_Regular_Expressions.GUID).ToList()
                                 join objRegExValMain in parserFieldsRaw.RegexAtt on
                                     objRegExMain.ID_Other equals objRegExValMain.ID_Object
                                 select new RegexField
                                 {
                                     IdField = objRegExMain.ID_Object,
                                     IdRegex = objRegExMain.ID_Other,
                                     IdAttribute = objRegExValMain.ID_Attribute,
                                     Regex = objRegExValMain.Val_String
                                 }).ToList();

                var regexPre = (from objRegExMain in parserFieldsRaw.FieldRels.
                                                                Where(
                                                                    r =>
                                                                    r.ID_RelationType == Config.LocalData.RelationType_Pre.GUID &&
                                                                    r.ID_Parent_Other == Config.LocalData.Class_Regular_Expressions.GUID).ToList()
                                 join objRegExValMain in parserFieldsRaw.RegexAtt on
                                     objRegExMain.ID_Other equals objRegExValMain.ID_Object
                                 select new RegexField
                                 {
                                     IdField = objRegExMain.ID_Object,
                                     IdRegex = objRegExMain.ID_Other,
                                     IdAttribute = objRegExValMain.ID_Attribute,
                                     Regex = objRegExValMain.Val_String
                                 }).ToList();

                var regexPost = (from objRegExMain in parserFieldsRaw.FieldRels.
                                                                Where(
                                                                    r =>
                                                                    r.ID_RelationType == Config.LocalData.RelationType_Posts.GUID &&
                                                                    r.ID_Parent_Other == Config.LocalData.Class_Regular_Expressions.GUID).ToList()
                                join objRegExValMain in parserFieldsRaw.RegexAtt on
                                    objRegExMain.ID_Other equals objRegExValMain.ID_Object
                                select new RegexField
                                {
                                    IdField = objRegExMain.ID_Object,
                                    IdRegex = objRegExMain.ID_Other,
                                    IdAttribute = objRegExValMain.ID_Attribute,
                                    Regex = objRegExValMain.Val_String
                                }).ToList();

                var replaceList = new List<FieldReplace>();

                parserFieldsRaw.RegexReplaceRaw.ReplaceFields.ForEach(replaceField =>
                {
                    var replacePreList = (from objFieldReplace in parserFieldsRaw.RegexReplaceRaw.ReplaceRel
                                          where objFieldReplace.ID_Object == replaceField.GUID
                                          join objFieldText in parserFieldsRaw.RegexReplaceRaw.ReplaceWith
                                              on objFieldReplace.ID_Other equals objFieldText.ID_Object
                                              into objFieldTexts
                                          from objFieldText in objFieldTexts.DefaultIfEmpty()
                                          join objRegExOfReplace in
                                              parserFieldsRaw.RegexReplaceRaw.RegexOfReplace on
                                              objFieldReplace.ID_Other equals objRegExOfReplace.ID_Object
                                          join objRegExOfRegEx in parserFieldsRaw.RegexReplaceRaw.RegexRegex on
                                              objRegExOfReplace.ID_Other equals objRegExOfRegEx.ID_Object
                                          select new RegexReplace()
                                          {
                                              IdField = replaceField.GUID,
                                              IdFieldReplace = objFieldReplace.ID_Other,
                                              NameFiledReplace = objFieldReplace.Name_Other,
                                              IdAttributeText =
                                                  objFieldText != null ? objFieldText.ID_Attribute : null,
                                              ReplaceWith =
                                                  objFieldText != null
                                                      ? objFieldText.Val_String ?? ""
                                                      : "",
                                              IdRegEx = objRegExOfReplace.ID_Other,
                                              IdAttributeRegex = objRegExOfRegEx.ID_Attribute,
                                              RegExSearch = objRegExOfRegEx.Val_String,
                                              OrderId = objFieldReplace.OrderID.Value
                                          }).ToList();

                    var fieldReplace = new FieldReplace
                    {
                        IdField = replaceField.GUID,
                        ReplaceList = replacePreList.OrderBy(repl => repl.OrderId).ToList()
                    };
                    replaceList.Add(fieldReplace);

                });
                result.Result = (from objField in parserFieldsRaw.FieldList
                             join objFieldParser in parserFieldsRaw.FieldParserToField on
                                 objField.GUID equals objFieldParser.ID_Other
                             join objRemoveFromSource
                                 in parserFieldsRaw.FieldAtts.
                                                         Where(
                                                             at =>
                                                             at.ID_AttributeType == Config.LocalData.AttributeType_Remove_from_source.GUID).ToList()
                                 on objField.GUID equals objRemoveFromSource.ID_Object
                             join objUseOrderId
                                 in parserFieldsRaw.FieldAtts.
                                                         Where(
                                                             at =>
                                                             at.ID_AttributeType == Config.LocalData.AttributeType_UseOrderId.GUID)
                                                         .ToList()
                                 on objField.GUID equals objUseOrderId.ID_Object
                             join objHtmlDecode
                                 in parserFieldsRaw.FieldAtts.
                                                         Where(
                                                             at =>
                                                             at.ID_AttributeType == Config.LocalData.AttributeType_HTMLDecode.GUID)
                                                         .ToList()
                                 on objField.GUID equals objHtmlDecode.ID_Object into objHtmlDecodes
                             from objHtmlDecode in objHtmlDecodes.DefaultIfEmpty()
                             join objDataType
                                 in parserFieldsRaw.FieldRels.
                                                         Where(
                                                             dt =>
                                                             dt.ID_RelationType == Config.LocalData.RelationType_Value_Type.GUID &&
                                                             dt.ID_Parent_Other == Config.LocalData.Class_DataTypes.GUID)
                                                         .ToList()
                                 on objField.GUID equals objDataType.ID_Object
                             join objMeta in parserFieldsRaw.FieldRels.
                                                                 Where(
                                                                     m =>
                                                                     m.ID_RelationType == Config.LocalData.RelationType_is.GUID &&
                                                                     m.ID_Parent_Other == Config.LocalData.Class_Metadata__Parser_.GUID).ToList()
                                 on objField.GUID equals objMeta.ID_Object into objMetas
                             from objMeta in objMetas.DefaultIfEmpty()
                             join objUseLastValid in parserFieldsRaw.FieldAtts.
                                                                 Where(
                                                                     at =>
                                                                     at.ID_AttributeType == Config.LocalData.AttributeType_UseLastValid.GUID)
                                                                 .ToList() on objField.GUID equals objUseLastValid.ID_Object into UseLastValidItems
                             from objUseLastValid in UseLastValidItems.DefaultIfEmpty()
                             join objTwoLetterIsoName in parserFieldsRaw.FieldsToTwoLetterIsoNames on objField.GUID equals objTwoLetterIsoName.ID_Object into objTwoLetterIsoNames
                             from objTwoLetterIsoName in objTwoLetterIsoNames.DefaultIfEmpty()
                             join objRegExMain in regexMain on objField.GUID equals objRegExMain.IdField into objRegExMains
                             from objRegExMain in objRegExMains.DefaultIfEmpty()
                             join objRegExPre in regexPre on objField.GUID equals objRegExPre.IdField into objRegExPres
                             from objRegExPre in objRegExPres.DefaultIfEmpty()
                             join objRegExPost in regexPost on objField.GUID equals objRegExPost.IdField into objRegExPosts
                             from objRegExPost in objRegExPosts.DefaultIfEmpty()
                             join objReplace in parserFieldsRaw.ReplaceWith  on objField.GUID equals objReplace.ID_Object into objReplaces
                             from objReplace in objReplaces.DefaultIfEmpty()
                             join objReferenceField in parserFieldsRaw.UserContentFields on objField.GUID equals objReferenceField.ID_Object into objReferenceFields
                             from objReferenceField in objReferenceFields.DefaultIfEmpty()
                             join objDoAll in parserFieldsRaw.DoAll on objField.GUID equals objDoAll.ID_Object into objDoAlls
                             from objDoAll in objDoAlls.DefaultIfEmpty()
                             join objFieldReplace in replaceList on objField.GUID equals objFieldReplace.IdField into objFieldReplaces
                             from objFieldReplace in objFieldReplaces.DefaultIfEmpty()
                             join objCSVField in csvFields on objField.GUID equals objCSVField.ID_Object into objCSVFields
                             from objCSVField in objCSVFields.DefaultIfEmpty()
                             select new ParserField
                             {
                                 IdFieldParser = objFieldParser.ID_Object,
                                 NameFieldParser = objFieldParser.Name_Object,
                                 IdField = objField.GUID,
                                 NameField = objField.Name,
                                 IdDataType = objDataType.ID_Other,
                                 DataType = objDataType.Name_Other,
                                 IdAttributeRemoveFromSource = objRemoveFromSource.ID_Attribute,
                                 RemoveFromSource = objRemoveFromSource.Val_Bit ?? false,
                                 IdAttributeUseOrderId = objUseOrderId.ID_Attribute,
                                 UseOrderId = objUseOrderId.Val_Bit ?? false,
                                 IdMetaField = objMeta != null ? objMeta.ID_Other : null,
                                 NameMetaField = objMeta != null ? objMeta.Name_Other : null,
                                 IsMeta = objMeta != null,
                                 IdRegexPre = objRegExPre != null ? objRegExPre.IdRegex : null,
                                 IdAttributeRegexPre = objRegExPre != null ? objRegExPre.IdAttribute : null,
                                 RegexPre = objRegExPre != null ? objRegExPre.Regex : null,
                                 IdRegexMain = objRegExMain != null ? objRegExMain.IdRegex : null,
                                 IdAttributeRegexMain = objRegExMain != null ? objRegExMain.IdAttribute : null,
                                 RegexMain = objRegExMain != null ? objRegExMain.Regex : null,
                                 IdRegexPost = objRegExPost != null ? objRegExPost.IdRegex : null,
                                 IdAttributeRegexPost = objRegExPost != null ? objRegExPost.IdAttribute : null,
                                 RegexPost = objRegExPost != null ? objRegExPost.Regex : null,
                                 OrderId = objFieldParser.ID_RelationType == Config.LocalData.RelationType_starts_with.GUID ? -2 :
                                         objFieldParser.ID_RelationType == Config.LocalData.RelationType_ends_with.GUID ? -1 : objFieldParser.OrderID ?? 0,
                                 Insert = objReplace != null ? objReplace.Name_Other : null,
                                 IsInsert = objReplace != null ? true : false,
                                 IdAttributeUseLastField = objUseLastValid != null ? objUseLastValid.ID_Attribute : null,
                                 UseLastValid = objUseLastValid != null ? objUseLastValid.Val_Bit != null ? (bool)objUseLastValid.Val_Bit : false : false,
                                 IdReferenceField = objReferenceField != null ? objReferenceField.ID_Other : null,
                                 NameReferenceField = objReferenceField != null ? objReferenceField.Name_Other : null,
                                 IdAttributeDoAll = objDoAll != null ? objDoAll.ID_Attribute : null,
                                 DoAll = objDoAll != null ? (bool)objDoAll.Val_Bool : false,
                                 ReplaceList = objFieldReplace != null ? objFieldReplace.ReplaceList : null,
                                 Start = objFieldParser.ID_RelationType == Config.LocalData.RelationType_starts_with.GUID ? true : false,
                                 End = objFieldParser.ID_RelationType == Config.LocalData.RelationType_ends_with.GUID ? true : false,
                                 HtmlDecode = objHtmlDecode != null ? objHtmlDecode.Val_Bit != null ? objHtmlDecode.Val_Bit.Value : false : false,
                                 IdCSVField = objCSVField != null ? objCSVField.ID_Other : null,
                                 NameCSVField = objCSVField != null ? objCSVField.Name_Other : null,
                                 IdTwoLetterIsoName = objTwoLetterIsoName != null  ? objTwoLetterIsoName.ID_Other : null,
                                 NameTwoLetterIsoName = objTwoLetterIsoName != null ? objTwoLetterIsoName.Name_Other : null,
                                 CultureInfo = System.Globalization.CultureInfo.InvariantCulture
                             }).ToList();

                foreach (var parserField in result.Result)
                {
                    if (!string.IsNullOrEmpty(parserField.NameTwoLetterIsoName))
                    {
                        var cultInfo = CultureInfo.CreateSpecificCulture(parserField.NameTwoLetterIsoName);
                        parserField.CultureInfo = cultInfo;
                    }

                    parserField.FieldListContained = (from field in parserFieldsRaw.FieldRels.Where(field => field.ID_Object == parserField.IdField).OrderBy(field => field.OrderID).ThenBy(field => field.Name_Other)
                                                      join subField in result.Result on field.ID_Other equals subField.IdField
                                                      select subField).ToList();

                    var mergeFields = parserFieldsRaw.MergeFields.Where(field => field.ID_Object == parserField.IdField).ToList();
                    if (mergeFields.Any())
                    {
                        parserField.MergeField = new MergeField();
                        parserField.MergeField.FieldList = (from mergeField in mergeFields
                                                            join fieldRel in parserFieldsRaw.MergeFieldsRef.Where(mergeFRel => mergeFRel.ID_Parent_Other == Config.LocalData.Class_Field.GUID).OrderBy(fieldRel => fieldRel.OrderID) on mergeField.ID_Other equals fieldRel.ID_Object
                                                            join fieldItem in result.Result on fieldRel.ID_Other equals fieldItem.IdField
                                                            select fieldItem).ToList();
                        parserField.MergeField.SeperatorRelItem = (from mergeField in mergeFields
                                                                   join seperatorItem in parserFieldsRaw.MergeFieldsRef on mergeField.ID_Other equals seperatorItem.ID_Object
                                                                   select seperatorItem).FirstOrDefault();
                    }
                }
                
                return result;
            });

            return taskResult;
        }
    }
}
