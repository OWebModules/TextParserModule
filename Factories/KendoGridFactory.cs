﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace TextParserModule.Factories
{
    public class KendoGridFactory
    {
        public async Task<ResultItem<Dictionary<string, object>>> CrateGridConfig(List<ParserField> parserFields, string getDataUrl, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<Dictionary<string, object>>>(() =>
            {
                var result = new ResultItem<Dictionary<string, object>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new Dictionary<string, object>()
                };

                result.Result.Add("groupable", false);

                var sortableDict = new Dictionary<string, object>();
                sortableDict.Add("mode", "multiple");
                sortableDict.Add("allowUnsort", true);
                sortableDict.Add("showIndexes", true);
                result.Result.Add("sortable", sortableDict);
                result.Result.Add("autoBind", false);
                result.Result.Add("height", "100%");
                result.Result.Add("scrollable", true);
                result.Result.Add("resizable", true);
                result.Result.Add("selectable", "multiple cell");
                result.Result.Add("allowCopy", true);
                result.Result.Add("columnMenu", true);

                var pageConfig = new Dictionary<string, object>();
                pageConfig.Add("refresh", true);

                var pageSizes = new List<object>
                {
                    5,10,50,100,1000,10000,100000,1000000
                };
                pageConfig.Add("pageSizes", pageSizes);
                pageConfig.Add("numeric", false);

                var messagesConfig = new Dictionary<string, object>();
                messagesConfig.Add("display", "Showing {0}-{1} from {2} data items");

                pageConfig.Add("messages", messagesConfig);

                result.Result.Add("pageable", pageConfig);

                var filterConfig = new Dictionary<string, object>();
                filterConfig.Add("extra", false);

                var operatorsConfig = new Dictionary<string, object>();
                var stringConfig = new Dictionary<string, object>();
                stringConfig.Add("contains", "Contains");
                stringConfig.Add("startswith", "Starts with");
                stringConfig.Add("eq", "Is equal to");
                stringConfig.Add("neq", "Is not equal to");
                operatorsConfig.Add("string", stringConfig);

                filterConfig.Add("operators", operatorsConfig);

                result.Result.Add("filterable", false);

                var dictSchema = new Dictionary<string, object>();
                var dictModel = new Dictionary<string, object>();
                var dictFields = new Dictionary<string, object>();

                dictSchema.Add("model", dictModel);
                dictSchema.Add("data", "data"); 
                dictSchema.Add("total", "total");
                dictModel.Add("fields", dictFields);

                var columnList = parserFields.OrderBy(field => field.OrderId).ThenBy(field => field.NameField).Select(field =>
                {
                    var dictField = new Dictionary<string, object>();
                    dictFields.Add(field.NameField, dictField);
                    dictField.Add("editable", false);

                    var columnAttribute = new KendoColumnAttribute()
                    {
                        field = field.NameField,
                        title = field.NameField,
                        Order = ((int)field.OrderId) + 1,
                        width = "150px",
                        filterable = true
                    };

                    if (field.DataType.ToLower() == "DateTime".ToLower())
                    {
                        columnAttribute.type = ColType.DateType;
                        //columnAttribute.template = "#=kendo.toString(kendo.parseDate(" + field.NameField + ", 'dd.MM.yy HH:mm:ss.fff'),'dd.MM.yyyy HH:mm:ss.fff')#";
                        columnAttribute.template =
                            $"#= (typeof {field.NameField} === 'undefined') ? '' : kendo.toString(kendo.parseDate({field.NameField}, 'yyyy-MM-ddTHH:mm:ss.fff'),'dd.MM.yyyy HH:mm:ss.fff')#";
                        //columnAttribute.template = "#=kendo.toString(kendo.parseDate(" + field.NameField + ", 'yyyy-MM-ddTHH:mm:ss'),'dd.MM.yyyy HH:mm:ss')#";
                        dictField.Add("type", "date");
                    }
                    else if (field.DataType.ToLower() == "int")
                    {
                        columnAttribute.type = ColType.NumberType;
                        dictField.Add("type", "number");
                    }
                    else if (field.DataType.ToLower() == "bit")
                    {
                        columnAttribute.type = ColType.BooleanType;
                        dictField.Add("type", "boolean");
                    }
                    else if (field.DataType.ToLower() == "double")
                    {
                        columnAttribute.type = ColType.NumberType;
                        dictField.Add("type", "number");
                    }
                    else
                    {
                        columnAttribute.type = ColType.StringType;
                        dictField.Add("type", "string");
                    }

                    columnAttribute.attributes.Add("class", "value-cell");
                    return columnAttribute;
                }).ToList();
                var editColumn = new KendoColumnAttribute
                {
                    field = "EditTextParserRow",
                    title = "Edit",
                    Order = 0,
                    filterable = false,
                    width = "80px",
                    template = "<button type='button' class='button-row-edit'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button>"
                };
                editColumn.attributes.Add("class", "edit-cell");
                columnList.Insert(0, editColumn);

                result.Result.Add("columns", columnList);
                var dataSourceConfig = new Dictionary<string, object>();
                var transportConfig = new Dictionary<string, object>();
                var readConfig = new Dictionary<string, object>();
                readConfig.Add("url", getDataUrl);
                readConfig.Add("dataType", "json");
                transportConfig.Add("read", readConfig);
                dataSourceConfig.Add("transport", transportConfig);
                dataSourceConfig.Add("pageSize", 20);
                dataSourceConfig.Add("serverPaging", true);
                dataSourceConfig.Add("serverFiltering", true);
                dataSourceConfig.Add("serverSorting", true);
                dataSourceConfig.Add("type", "aspnetmvc-ajax");
                dataSourceConfig.Add("schema", dictSchema);

                result.Result.Add("dataSource", dataSourceConfig);

                return result;
            });

            return taskResult;
        }
    }
}
