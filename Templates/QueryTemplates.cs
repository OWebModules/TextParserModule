﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Templates
{
    public static class QueryTemplates
    {
        public static string DateRange => "[{0} TO {1}]";
    }
}
