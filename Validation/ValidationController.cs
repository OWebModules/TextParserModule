﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;

namespace TextParserModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateConvertPdfToTextRequest(ConvertPdfToTextRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateIndexPDFRequest(IndexPDFRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateIndexPDFModel(IndexPDFModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || (propertyName == nameof(model.IndexPDFConfig)))
            {
                if (model.IndexPDFConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Config is not found!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || (propertyName == nameof(model.ElasticSearchConfig)))
            {
                if (model.ElasticSearchConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Es-Index of Config is not found!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateConvertPdfToTextModel(ConvertPdfToTextModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (!string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.Config))
            {
                if (model.Config == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Config is not found!";
                    return result;
                }
            }

            if (!string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.FolderConvert))
            {

            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigToSrcPath))
            {
                if (model.ConfigToSrcPath == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Src-Path is not set!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(model.ConfigToDstPath))
            {
                if (model.ConfigToSrcPath == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Dst-Path is not set!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateExecuteTextParserRequest(ExecuteTextParserRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdTextParser) &&
                (request.TextParser == null || !request.TextParserFields.Any()))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide the id for a TextParser or the TextParser and its fields!";
                return result;
            }

            if (!string.IsNullOrEmpty(request.IdTextParser) && !globals.is_GUID(request.IdTextParser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Id for a TextParser is not valid!";
                return result;
            }
            
            return result;
        }


        public static clsOntologyItem ValidateRelateTextParserDocsRequest(RelateTextParserDocsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!request.IdsDocuments.Any() && string.IsNullOrEmpty(request.Query))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You need to provide a list of Ids or a Query!";
                return result;
            }

            if (request.IdsDocuments.Any() && !string.IsNullOrEmpty(request.Query))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You need to provide a list of Ids or a Query, not both!";
                return result;
            }

            if (request.TextParser == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No Textparser provided!";
                return result;
            }

            if (request.ParserFields == null || !request.ParserFields.Any())
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No Fields provided!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateMeasureTextParserRequest(MeasureTextParserRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetMeasureTextparserModel(MeasureTextparserModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(MeasureTextparserModel.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "There are more than one Configs!";
                    return result;
                }
                model.Config = dbReader.Objects1.First();
            }
            else if (propertyName == nameof(MeasureTextparserModel.DeleteTextparser))
            {
                if (dbReader.ObjAtts.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one DeleteTextparser Attribute!";
                    return result;
                }

                model.DeleteTextparser = dbReader.ObjAtts.First();
            }
            else if (propertyName == nameof(MeasureTextparserModel.ExecuteTextparser))
            {
                if (dbReader.ObjAtts.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one ExecuteTextparser Attribute!";
                    return result;
                }

                model.ExecuteTextparser = dbReader.ObjAtts.First();
            }
            else if (propertyName == nameof(MeasureTextparserModel.SplitField))
            {
                var splitFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Textparser_Measurement_Split_Field.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Textparser_Measurement_Split_Field.ID_RelationType).ToList();
                if (splitFields.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exactly one Split-Field, but the count is {splitFields.Count}!";
                    return result;
                }

                var splitFieldRaw = splitFields.First();
                model.SplitField = new clsOntologyItem
                {
                    GUID = splitFieldRaw.ID_Other,
                    Name = splitFieldRaw.Name_Other,
                    GUID_Parent = splitFieldRaw.ID_Parent_Other,
                    Type = splitFieldRaw.Ontology
                };
            }
            else if (propertyName == nameof(MeasureTextparserModel.ConfigToMeasureFields))
            {
                var measureFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Measure_Field.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Measure_Field.ID_RelationType).ToList();

                if (!measureFields.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need min one Measure-Field!";
                    return result;
                }

                model.ConfigToMeasureFields = measureFields;
            }
            else if (propertyName == nameof(MeasureTextparserModel.MeasureFieldsToClacFields))
            {
                var calcFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Measure_Field_Calc_Field.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Measure_Field_Calc_Field.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Measure_Field_Calc_Field.ID_RelationType).ToList();

                if (calcFields.Count != model.ConfigToMeasureFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need for every Measure-Field a Count-Field!";
                    return result;
                }

                model.MeasureFieldsToClacFields = calcFields;
            }
            else if (propertyName == nameof(MeasureTextparserModel.MeasureFieldsToDestFields))
            {
                var destFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Measure_Field_dst_Field.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Measure_Field_dst_Field.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Measure_Field_dst_Field.ID_RelationType).ToList();

                if (destFields.Count != model.ConfigToMeasureFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need for every Measure-Field a Dest-Field!";
                    return result;
                }

                model.MeasureFieldsToDestFields = destFields;
            }
            else if (propertyName == nameof(MeasureTextparserModel.MeasureFieldsToCheckFieldValues))
            {
                var checkFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Measure_Field_Check_Field_with_Value.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Measure_Field_Check_Field_with_Value.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Measure_Field_Check_Field_with_Value.ID_RelationType).ToList();

                if (checkFields.Count > model.ConfigToMeasureFields.Count * 2)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need for every Measure-Field a Dest-Field!";
                    return result;
                }

                model.MeasureFieldsToCheckFieldValues = checkFields;
            }
            else if (propertyName == nameof(MeasureTextparserModel.FieldValuesToFields))
            {
                var fieldValueFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Field_with_Value_belonging_Field.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Field_with_Value_belonging_Field.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Field_with_Value_belonging_Field.ID_RelationType).ToList();

                if (fieldValueFields.Count != model.MeasureFieldsToCheckFieldValues.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need for every Field-Value a Field!";
                    return result;
                }

                model.FieldValuesToFields = fieldValueFields;
            }
            else if (propertyName == nameof(MeasureTextparserModel.FieldValuesValues))
            {
                var fieldValueValues = dbReader.ObjAtts.Where(att => att.ID_Class == Measure.Config.LocalData.ClassAtt_Field_with_Value_Bitvalue.ID_Class &&
                                                                     att.ID_AttributeType == Measure.Config.LocalData.ClassAtt_Field_with_Value_Bitvalue.ID_AttributeType).ToList();

                if (fieldValueValues.Count > model.MeasureFieldsToCheckFieldValues.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need for every Field-Value max one Value!";
                    return result;
                }

                model.FieldValuesValues = fieldValueValues;
            }
            else if (propertyName == nameof(MeasureTextparserModel.MeasureFieldsToMeasureTypes))
            {
                var measureTypes = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Measure_Field_is_of_Type_Measure_Type.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Measure_Field_is_of_Type_Measure_Type.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Measure_Field_is_of_Type_Measure_Type.ID_RelationType).ToList();

                if (measureTypes.Count != model.ConfigToMeasureFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need for every Measure-Field a Measure-Type!";
                    return result;
                }

                model.MeasureFieldsToMeasureTypes = measureTypes;
            }
            else if (propertyName == nameof(MeasureTextparserModel.ConfigToSortFields))
            {
                var sortFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Sort_Field.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Sort_Field.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Sort_Field.ID_RelationType).ToList();

                if (!sortFields.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need at least one Sort-Field!";
                    return result;
                }

                model.ConfigToSortFields = sortFields;
            }
            else if (propertyName == nameof(MeasureTextparserModel.SortFieldsToFields))
            {
                var sortFieldsFields = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Sort_Field_belonging_Field.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Sort_Field_belonging_Field.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Sort_Field_belonging_Field.ID_RelationType).ToList();

                if (sortFieldsFields.Count != model.ConfigToSortFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need {model.ConfigToSortFields.Count} Sort-Field Fields, but you have only {sortFieldsFields.Count} Fields!";
                    return result;
                }

                model.SortFieldsToFields = sortFieldsFields;
            }
            else if (propertyName == nameof(MeasureTextparserModel.SortFieldsToSort))
            {
                var sortFieldsFieldsSort = dbReader.ObjAtts.Where(att => att.ID_Class == Measure.Config.LocalData.ClassAtt_Sort_Field_ASC.ID_Class &&
                                                                     att.ID_AttributeType == Measure.Config.LocalData.ClassAtt_Sort_Field_ASC.ID_AttributeType).ToList();

                if (sortFieldsFieldsSort.Count != model.SortFieldsToFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need {model.SortFieldsToFields.Count} Sorts, but you have only {sortFieldsFieldsSort.Count}!";
                    return result;
                }

                model.SortFieldsToSort = sortFieldsFieldsSort;
            }
            else if (propertyName == nameof(MeasureTextparserModel.BasePattern))
            {
                var basePattern = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Textparser_Measurement_base_Pattern.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Textparser_Measurement_base_Pattern.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Textparser_Measurement_base_Pattern.ID_RelationType).ToList();

                if (basePattern.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact on Base-Pattern!";
                    return result;
                }

                var basePatternRaw = basePattern.First();
                model.BasePattern = new clsOntologyItem
                {
                    GUID = basePatternRaw.ID_Other,
                    Name = basePatternRaw.Name_Other,
                    GUID_Parent = basePatternRaw.ID_Parent_Other,
                    Type = basePatternRaw.Ontology
                };
            }
            else if (propertyName == nameof(MeasureTextparserModel.PatternText))
            {
                var basePatternText = dbReader.ObjAtts.Where(att => att.ID_Class == Measure.Config.LocalData.ClassAtt_Pattern_Pattern.ID_Class &&
                                                                     att.ID_AttributeType == Measure.Config.LocalData.ClassAtt_Pattern_Pattern.ID_AttributeType).ToList();

                if (basePatternText.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one Pattern-Text!";
                    return result;
                }

                model.PatternText = basePatternText.First();
            }
            else if (propertyName == nameof(MeasureTextparserModel.Textparser))
            {
                var textParsers = dbReader.ObjectRels.Where(rel => rel.ID_Parent_Object == Measure.Config.LocalData.ClassRel_Textparser_Measurement_belonging_Textparser.ID_Class_Left &&
                                                                    rel.ID_Parent_Other == Measure.Config.LocalData.ClassRel_Textparser_Measurement_belonging_Textparser.ID_Class_Right &&
                                                                   rel.ID_RelationType == Measure.Config.LocalData.ClassRel_Textparser_Measurement_belonging_Textparser.ID_RelationType).ToList();

                if (textParsers.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one Textparser!";
                    return result;
                }

                var textParserRaw = textParsers.First();
                model.Textparser = new clsOntologyItem
                {
                    GUID = textParserRaw.ID_Other,
                    Name = textParserRaw.Name_Other,
                    GUID_Parent = textParserRaw.ID_Parent_Other,
                    Type = textParserRaw.Ontology
                };
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetReIndexPatternSourceFieldsModel(List<ParserField> parserFields, ReIndexPatternSourceFieldsModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(ReIndexPatternSourceFieldsModel.PatternSourceFields))
            {
                model.PatternSourceFields = (from parserField in parserFields
                                             join parserSourceToField in dbReader.ObjectRels on parserField.IdField equals parserSourceToField.ID_Other
                                             select parserField).ToList();
                model.PatternSourcesToFields = dbReader.ObjectRels;

                var patternSourcesCount = model.PatternSourcesToFields.GroupBy(patternSourceToField => patternSourceToField.ID_Object).Count();
                if (patternSourcesCount != model.PatternSourceFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {patternSourcesCount} Pattern-Sources and {model.PatternSourceFields.Count} Fields. They must be equal!";
                    return result;
                }
            }
            else if (propertyName == nameof(ReIndexPatternSourceFieldsModel.PatternSourceToPattern))
            {
                if (dbReader.ObjectRels.Count != model.PatternSourcesToFields.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {dbReader.ObjectRels.Count} Pattern-Sources, but {dbReader.ObjectRels.Count} Pattern-Items! They must be equal!";
                    return result;
                }

                model.PatternSourceToPattern = dbReader.ObjectRels;
            }
            else if (propertyName == nameof(ReIndexPatternSourceFieldsModel.PatternPattern))
            {
                var patternCount = model.PatternSourceToPattern.GroupBy(patSourceToPat => patSourceToPat.ID_Other).Count();

                if (dbReader.ObjAtts.Count != patternCount)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {patternCount} Pattern-Items, but {dbReader.ObjAtts.Count} Pattern! They must be equal!";
                    return result;
                }

                model.PatternPattern = dbReader.ObjAtts;
            }
            else if (propertyName == nameof(ReIndexPatternSourceFieldsModel.PatternSourceValues))
            {
                var patternIds = model.PatternSourcesToFields.GroupBy(patSourceToField => patSourceToField.ID_Object).Select(grp => grp.Key).ToList();
                foreach (var patSourceToField in patternIds)
                {
                    var countAtts = dbReader.ObjAtts.Where(objAtt => objAtt.ID_Object == patSourceToField).Count();
                    if (countAtts != 1)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"You have {patternIds.Count} Pattern-Items, but {countAtts} Values! They must be equal!";
                        return result;
                    }
                }

                model.PatternSourceValues = dbReader.ObjAtts;
            }

            return result;
        }

        public static clsOntologyItem ValidateReIndexPatternSourceFieldsRequest(ReIndexPatternSourceFieldsByTextparserRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();


            return result;
        }
    }
}
