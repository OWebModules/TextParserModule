﻿using ElasticSearchConfigModule;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule.Models;
using TextParserModule.Validation;

namespace TextParserModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {

        public async Task<ResultItem<IndexPDFModel>> GetIndexPDFModel(IndexPDFRequest request)
        {

            var taskResult = await Task.Run<ResultItem<IndexPDFModel>>(async () =>
            {
                var result = new ResultItem<IndexPDFModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new IndexPDFModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = IndexPdf.Config.LocalData.Class_Index_PDF_Files.GUID
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Index-Config!";
                    return result;
                }

                result.Result.IndexPDFConfig = dbReaderConfig.Objects1.FirstOrDefault();

                result.ResultState = ValidationController.ValidateIndexPDFModel(result.Result, globals, nameof(result.Result.IndexPDFConfig));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPaths = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.IndexPDFConfig.GUID,
                        ID_RelationType = IndexPdf.Config.LocalData.ClassRel_Index_PDF_Files_belonging_Source_Path.ID_RelationType,
                        ID_Parent_Other = IndexPdf.Config.LocalData.ClassRel_Index_PDF_Files_belonging_Source_Path.ID_Class_Right
                    }
                };

                var dbReaderPaths = new OntologyModDBConnector(globals);

                if (searchPaths.Any())
                {
                    result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Path!";
                        return result;
                    }

                    result.Result.ConfigToPath = dbReaderPaths.ObjectRels.FirstOrDefault();
                }

                var esConfigConnector = new ElasticSearchConfigController(globals);

                var getEsIndexResult = await esConfigConnector.GetConfig(result.Result.IndexPDFConfig, IndexPdf.Config.LocalData.RelationType_belonging_Destination, IndexPdf.Config.LocalData.RelationType_uses, globals.Direction_LeftRight);

                result.ResultState = getEsIndexResult.ResultState;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Es-Index and the Es-Type!";
                    return result;
                }

                result.Result.ElasticSearchConfig = getEsIndexResult.Result;

                result.ResultState = ValidationController.ValidateIndexPDFModel(result.Result, globals, nameof(result.Result.ElasticSearchConfig));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                if (result.Result.ConfigToPath == null)
                {
                    var mediaViewerController = new MediaViewerModule.Connectors.MediaViewerConnector(globals);

                    var pdfDocumentsResult = await mediaViewerController.GetPDFDocuments(new List<clsOntologyItem>(), request.MessageOutput);

                    result.ResultState = pdfDocumentsResult.Result;

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.PDFDocuments = pdfDocumentsResult.MediaListItems;

                    var searchSessionPDFs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__contains_PDF_Documents.ID_Class_Left,
                        ID_RelationType = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__contains_PDF_Documents.ID_RelationType,
                        ID_Parent_Other = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__contains_PDF_Documents.ID_Class_Right
                    }
                };

                    var dbReaderSessionPDFs = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderSessionPDFs.GetDataObjectRel(searchSessionPDFs);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Session-PDFs!";
                        return result;
                    }

                    result.Result.SessionPDFs = dbReaderSessionPDFs.ObjectRels;

                    var searchSessionPDFsNotExisting = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__not_existing_PDF_Documents.ID_Class_Left,
                        ID_RelationType = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__not_existing_PDF_Documents.ID_RelationType,
                        ID_Parent_Other = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__not_existing_PDF_Documents.ID_Class_Right
                    }
                };

                    var dbReaderNotExisting = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderNotExisting.GetDataObjectRel(searchSessionPDFsNotExisting);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the not-existing PDFs!";
                        return result;
                    }

                    result.Result.SessionPDFsNotExisting = dbReaderNotExisting.ObjectRels;
                    var searchSessionPDFsError = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__error_PDF_Documents.ID_Class_Left,
                        ID_RelationType = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__error_PDF_Documents.ID_RelationType,
                        ID_Parent_Other = IndexPdf.Config.LocalData.ClassRel_Index_Session__Index_PDF_Files__error_PDF_Documents.ID_Class_Right
                    }
                };

                    var dbReaderError = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderError.GetDataObjectRel(searchSessionPDFsError);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Error-PDFs!";
                        return result;
                    }

                    result.Result.SessionPDFsError = dbReaderError.ObjectRels;
                }


                return result;
            });

            return taskResult;

        }

        public async Task<ResultItem<ConvertPdfToTextModel>> GetConvertPdfToTextModel(ConvertPdfToTextRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ConvertPdfToTextModel>>(() =>
           {
               var result = new ResultItem<ConvertPdfToTextModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ConvertPdfToTextModel()
               };

               result.ResultState = Validation.ValidationController.ValidateConvertPdfToTextRequest(request, globals);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the config!";
                   return result;
               }

               result.Result.Config = dbReaderConfig.Objects1.FirstOrDefault();

               result.ResultState = ValidationController.ValidateConvertPdfToTextModel(result.Result, globals, nameof(result.Result.Config));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchConfigToSrc = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Convert.PDF.Config.LocalData.ClassRel_PDF_Convert_src_Path.ID_RelationType,
                       ID_Parent_Other = Convert.PDF.Config.LocalData.ClassRel_PDF_Convert_src_Path.ID_Class_Right
                   }
               };

               var dbReaderConfigToSrc = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigToSrc.GetDataObjectRel(searchConfigToSrc);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between the config and the src-path!";
                   return result;
               }

               result.Result.ConfigToSrcPath = dbReaderConfigToSrc.ObjectRels.FirstOrDefault();

               result.ResultState = ValidationController.ValidateConvertPdfToTextModel(result.Result, globals, nameof(result.Result.ConfigToSrcPath));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }


               var searchConfigToDst = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_RelationType = Convert.PDF.Config.LocalData.ClassRel_PDF_Convert_dst_Path.ID_RelationType,
                       ID_Parent_Other = Convert.PDF.Config.LocalData.ClassRel_PDF_Convert_dst_Path.ID_Class_Right
                   }
               };

               var dbReaderConfigToDst = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfigToDst.GetDataObjectRel(searchConfigToDst);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between the config and the Dst-path!";
                   return result;
               }

               result.Result.ConfigToDstPath = dbReaderConfigToDst.ObjectRels.FirstOrDefault();

               result.ResultState = ValidationController.ValidateConvertPdfToTextModel(result.Result, globals, nameof(result.Result.ConfigToDstPath));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchFolder = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_AttributeType = Convert.PDF.Config.LocalData.ClassAtt_PDF_Convert_Folder_Convert.ID_AttributeType
                   }
               };

               var dbReaderFolder = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderFolder.GetDataObjectAtt(searchFolder);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the folder-attribute!";
                   return result;
               }

               result.Result.FolderConvert = dbReaderFolder.ObjAtts.FirstOrDefault();

               result.ResultState = ValidationController.ValidateConvertPdfToTextModel(result.Result, globals, nameof(result.Result.FolderConvert));

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               return result;
           });

            return taskResult;
        }
        public async Task<ResultItem<TextParserRaw>> GetTextParser(clsOntologyItem parser = null)
        {
            var taskResult = await Task.Run<ResultItem<TextParserRaw>>(() =>
            {
                var result = new ResultItem<TextParserRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TextParserRaw()
                };

                var searchTextParsers = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                        {
                            GUID = parser != null ? parser.GUID: null,
                            GUID_Parent = Config.LocalData.Class_Textparser.GUID
                        }
                };

                var dbReaderTextParser = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderTextParser.GetDataObjects(searchTextParsers);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                result.Result.TextParserList = dbReaderTextParser.Objects1;

                var searchTextParserRels = dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_ToDo_Entry_Value_Parser.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_ToDo_Entry_Value_Parser.ID_Class_Right
                }).ToList();

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_ToDo_Field_Extractor_Parser.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_ToDo_Field_Extractor_Parser.ID_Class_Right

                }));

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_belonging_Resource_Fileresource.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_belonging_Resource_Fileresource.ID_Class_Right

                }));

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_located_at_Indexes__Elastic_Search_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_located_at_Indexes__Elastic_Search_.ID_Class_Right

                }));

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_Line_seperator_Text_Seperators.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_Line_seperator_Text_Seperators.ID_Class_Right,

                }));

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_belongs_to_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_belongs_to_user.ID_Class_Right
                }));

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_belonging_Types__Elastic_Search_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_belonging_Types__Elastic_Search_.ID_Class_Right

                }));

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_belonging_Source_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_belonging_Source_Field.ID_Class_Right

                }));

                searchTextParserRels.AddRange(dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_copy_from_parent_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_copy_from_parent_Field.ID_Class_Right

                }));

                var dbReaderTextParserLeftRight = new OntologyModDBConnector(globals);
                if (searchTextParserRels.Any())
                {
                    result.ResultState = dbReaderTextParserLeftRight.GetDataObjectRel(searchTextParserRels);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                }

                result.Result.TextParserLeftRight = dbReaderTextParserLeftRight.ObjectRels;

                var searchConfigurationItems = dbReaderTextParser.Objects1.Select(p => new clsObjectRel
                {
                    ID_Object = p.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_Config_Textparser_Configurationitem.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_Config_Textparser_Configurationitem.ID_Class_Right

                }).ToList();

                var dbReaderConfigurationItems = new OntologyModDBConnector(globals);
                if (searchConfigurationItems.Any())
                {
                    result.ResultState = dbReaderConfigurationItems.GetDataObjectRel(searchConfigurationItems);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ConfigurationItems = dbReaderConfigurationItems.ObjectRels;

                var searchSubParsers = dbReaderTextParser.Objects1.Select(parserItm => new clsObjectRel
                {
                    ID_Object = parserItm.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Textparser_contains_Textparser.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Textparser_contains_Textparser.ID_Class_Right
                }).ToList();

                var dbReaderSubParsers = new OntologyModDBConnector(globals);
                if (searchSubParsers.Any())
                {
                    result.ResultState = dbReaderSubParsers.GetDataObjectRel(searchSubParsers);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.SubParsers = dbReaderSubParsers.ObjectRels;

                var searchCommandLineRun = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = parser?.GUID,
                            ID_Parent_Object = Config.LocalData.ClassRel_Textparser_belonging_Resource_Comand_Line__Run_.ID_Class_Left,
                            ID_RelationType = Config.LocalData.ClassRel_Textparser_belonging_Resource_Comand_Line__Run_.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_Textparser_belonging_Resource_Comand_Line__Run_.ID_Class_Right
                        }
                    };

                var dbReaderCommandLine = new OntologyModDBConnector(globals);
                if (searchCommandLineRun.Any())
                {
                    result.ResultState = dbReaderCommandLine.GetDataObjectRel(searchCommandLineRun);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.CommandLineRun = dbReaderCommandLine.ObjectRels;

                var searchAttributes = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_Object = parser?.GUID,
                            ID_Class = Config.LocalData.ClassAtt_Textparser_IsCSV_Importer.ID_Class,
                            ID_AttributeType = Config.LocalData.ClassAtt_Textparser_IsCSV_Importer.ID_AttributeType
                        }
                    };

                var dbReaderParserAtts = new OntologyModDBConnector(globals);
                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderParserAtts.GetDataObjectAtt(searchAttributes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.TextParsersAtt = dbReaderParserAtts.ObjAtts;



                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ParserFieldsRaw>> GetParserFields(clsOntologyItem parser = null)
        {
            var taskResult = await Task.Run<ResultItem<ParserFieldsRaw>>(() =>
            {
                var result = new ResultItem<ParserFieldsRaw>()
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ParserFieldsRaw()
                };

                var searchFieldList = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Field.GUID
                    }
                };

                var dbReaderFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFields.GetDataObjects(searchFieldList);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchFieldParserToField = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Object = parser != null ? parser.GUID : null,
                            ID_Parent_Object = parser == null ? Config.LocalData.ClassRel_Field_Extractor_Parser_Entry_Field.ID_Class_Left : null,
                            ID_RelationType = Config.LocalData.ClassRel_Field_Extractor_Parser_Entry_Field.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_Field_Extractor_Parser_Entry_Field.ID_Class_Right
                        },
                    new clsObjectRel
                    {
                        ID_Object = parser != null ? parser.GUID : null,
                            ID_Parent_Object = parser == null ? Config.LocalData.ClassRel_Field_Extractor_Parser_starts_with_Field.ID_Class_Left : null,
                            ID_RelationType = Config.LocalData.ClassRel_Field_Extractor_Parser_starts_with_Field.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_Field_Extractor_Parser_starts_with_Field.ID_Class_Right
                    },
                    new clsObjectRel
                    {
                        ID_Object = parser != null ? parser.GUID : null,
                            ID_Parent_Object = parser == null ? Config.LocalData.ClassRel_Field_Extractor_Parser_ends_with_Field.ID_Class_Left : null,
                            ID_RelationType = Config.LocalData.ClassRel_Field_Extractor_Parser_ends_with_Field.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_Field_Extractor_Parser_ends_with_Field.ID_Class_Right
                    }
                };

                var dbReaderFieldParserToField = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderFieldParserToField.GetDataObjectRel(searchFieldParserToField);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchContentField = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.ClassRel_Field_ParseSource_Field.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_Field_ParseSource_Field.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_ParseSource_Field.ID_Class_Right
                    }
                };

                var dbReaderContentField = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderContentField.GetDataObjectRel(searchContentField);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchFieldsAtt = new List<clsObjectAtt>();
                var searchFieldsRel = new List<clsObjectRel>();
                var searchFilterAtt = new List<clsObjectAtt>();
                var searchRegexAtt = new List<clsObjectAtt>();
                var searchReplaceWith = new List<clsObjectRel>();

                if (parser == null || !dbReaderFieldParserToField.ObjectRels.Any())
                {
                    searchFieldsAtt = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_AttributeType = Config.LocalData.ClassAtt_Field_Remove_from_source.ID_AttributeType,
                            ID_Class = Config.LocalData.ClassAtt_Field_Remove_from_source.ID_Class
                        },
                        new clsObjectAtt
                        {
                            ID_AttributeType = Config.LocalData.ClassAtt_Field_UseOrderId.ID_AttributeType,
                            ID_Class = Config.LocalData.ClassAtt_Field_UseOrderId.ID_Class
                        },
                        new clsObjectAtt
                        {
                            ID_AttributeType = Config.LocalData.ClassAtt_Field_HTMLDecode.ID_AttributeType,
                            ID_Class = Config.LocalData.ClassAtt_Field_HTMLDecode.ID_Class
                        },
                        new clsObjectAtt
                        {
                            ID_AttributeType = Config.LocalData.ClassAtt_Field_UseLastValid.ID_AttributeType,
                            ID_Class = Config.LocalData.ClassAtt_Field_UseLastValid.ID_Class
                        }
                    };

                    searchFieldsRel = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = Config.LocalData.ClassRel_Field_Value_Type_DataTypes.ID_Class_Left,
                            ID_RelationType = Config.LocalData.ClassRel_Field_Value_Type_DataTypes.ID_RelationType,
                            ID_Parent_Other = Config.LocalData.ClassRel_Field_Value_Type_DataTypes.ID_Class_Right
                        },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_contains_Field.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_contains_Field.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_contains_Field.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_is_Metadata__Parser_.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_is_Metadata__Parser_.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_is_Metadata__Parser_.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_Main_RegEx_Field_Filter.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_Main_RegEx_Field_Filter.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Main_RegEx_Field_Filter.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_Pre_RegEx_Field_Filter.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_Pre_RegEx_Field_Filter.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Pre_RegEx_Field_Filter.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_Posts_RegEx_Field_Filter.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_Posts_RegEx_Field_Filter.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Posts_RegEx_Field_Filter.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_Main_Regular_Expressions.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_Main_Regular_Expressions.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Main_Regular_Expressions.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_Pre_Regular_Expressions.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_Pre_Regular_Expressions.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Pre_Regular_Expressions.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_Posts_Regular_Expressions.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_Posts_Regular_Expressions.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_Posts_Regular_Expressions.ID_Class_Right
                                },
                        new clsObjectRel
                                {
                                    ID_Parent_Object = Config.LocalData.ClassRel_Field_is_CSV_Field.ID_Class_Left,
                                    ID_RelationType = Config.LocalData.ClassRel_Field_is_CSV_Field.ID_RelationType,
                                    ID_Parent_Other = Config.LocalData.ClassRel_Field_is_CSV_Field.ID_Class_Right
                                }
                    };

                    searchFilterAtt = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                            {
                                ID_AttributeType = Config.LocalData.ClassAtt_RegEx_Field_Filter_Equal.ID_AttributeType,
                                ID_Class = Config.LocalData.ClassAtt_RegEx_Field_Filter_Equal.ID_Class
                            },
                        new clsObjectAtt
                            {
                                ID_AttributeType = Config.LocalData.ClassAtt_RegEx_Field_Filter_Pattern.ID_AttributeType,
                                ID_Class = Config.LocalData.ClassAtt_RegEx_Field_Filter_Pattern.ID_Class
                            }
                    };

                    searchRegexAtt = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                            {
                                ID_AttributeType = Config.LocalData.ClassAtt_Regular_Expressions_RegEx.ID_AttributeType,
                                ID_Class = Config.LocalData.ClassAtt_Regular_Expressions_RegEx.ID_Class
                            }
                    };

                    searchReplaceWith = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = Config.LocalData.Class_Field.GUID,
                            ID_RelationType = Config.LocalData.RelationType_replace_with.GUID
                        }
                    };
                }
                else
                {
                    searchFieldsAtt = dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectAtt
                    {
                        ID_Object = f.ID_Other,
                        ID_AttributeType = Config.LocalData.ClassAtt_Field_Remove_from_source.ID_AttributeType,
                        ID_Class = Config.LocalData.ClassAtt_Field_Remove_from_source.ID_Class
                    }).ToList();
                    searchFieldsAtt.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectAtt
                    {
                        ID_Object = f.ID_Other,
                        ID_AttributeType = Config.LocalData.ClassAtt_Field_UseOrderId.ID_AttributeType,
                        ID_Class = Config.LocalData.ClassAtt_Field_UseOrderId.ID_Class
                    }));
                    searchFieldsAtt.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectAtt
                    {
                        ID_Object = f.ID_Other,
                        ID_AttributeType = Config.LocalData.ClassAtt_Field_HTMLDecode.ID_AttributeType,
                        ID_Class = Config.LocalData.ClassAtt_Field_HTMLDecode.ID_Class
                    }));
                    searchFieldsAtt.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectAtt
                    {
                        ID_Object = f.ID_Other,
                        ID_AttributeType = Config.LocalData.ClassAtt_Field_UseLastValid.ID_AttributeType,
                        ID_Class = Config.LocalData.ClassAtt_Field_UseLastValid.ID_Class
                    }));


                    searchFieldsRel = dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Value_Type_DataTypes.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Value_Type_DataTypes.ID_Class_Right
                    }).ToList();


                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_contains_Field.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_contains_Field.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_is_Metadata__Parser_.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_is_Metadata__Parser_.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Main_RegEx_Field_Filter.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Main_RegEx_Field_Filter.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Pre_RegEx_Field_Filter.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Pre_RegEx_Field_Filter.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Posts_RegEx_Field_Filter.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Posts_RegEx_Field_Filter.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Main_Regular_Expressions.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Main_Regular_Expressions.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Pre_Regular_Expressions.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Pre_Regular_Expressions.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Posts_Regular_Expressions.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Posts_Regular_Expressions.ID_Class_Right
                    }));

                    searchFieldsRel.AddRange(dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.ClassRel_Field_is_CSV_Field.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_is_CSV_Field.ID_Class_Right
                    }));

                    searchReplaceWith = dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                    {
                        ID_Object = f.ID_Other,
                        ID_RelationType = Config.LocalData.RelationType_replace_with.GUID
                    }).ToList();

                    searchRegexAtt = new List<clsObjectAtt>
                        {
                            new clsObjectAtt
                                {
                                    ID_AttributeType = Config.LocalData.ClassAtt_Regular_Expressions_RegEx.ID_AttributeType,
                                    ID_Class = Config.LocalData.ClassAtt_Regular_Expressions_RegEx.ID_Class
                                }
                        };
                }

                var dbReaderFieldsAtt = new OntologyModDBConnector(globals);
                if (searchFieldsAtt.Any())
                {
                    result.ResultState = dbReaderFieldsAtt.GetDataObjectAtt(searchFieldsAtt);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                var dbReaderFieldsRel = new OntologyModDBConnector(globals);
                if (searchFieldsRel.Any())
                {
                    result.ResultState = dbReaderFieldsRel.GetDataObjectRel(searchFieldsRel);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                var dbReaderFilterAtt = new OntologyModDBConnector(globals);
                if (searchFilterAtt.Any())
                {
                    result.ResultState = dbReaderFilterAtt.GetDataObjectAtt(searchFilterAtt, doIds: false);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                var dbReaderRegexAtt = new OntologyModDBConnector(globals);
                if (searchRegexAtt.Any())
                {
                    result.ResultState = dbReaderRegexAtt.GetDataObjectAtt(searchRegexAtt, doIds: false);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                var dbReaderUserContentField = new OntologyModDBConnector(globals);
                if (searchContentField.Any())
                {
                    result.ResultState = dbReaderUserContentField.GetDataObjectRel(searchContentField, doIds: false);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                var dbReaderReplaceWith = new OntologyModDBConnector(globals);
                if (searchReplaceWith.Any())
                {
                    result.ResultState = dbReaderReplaceWith.GetDataObjectRel(searchReplaceWith, doIds: false);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                var resultRegexReplace = GetRegexReplace();
                result.ResultState = resultRegexReplace.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.RegexReplaceRaw = resultRegexReplace.Result;

                var searchContainFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.ClassRel_Field_contains_Field.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_Field_contains_Field.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_contains_Field.ID_Class_Right
                    }
                };

                var dbReaderContainedFields = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderContainedFields.GetDataObjectRel(searchContainFields);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchDoAll = dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectAtt
                {
                    ID_Object = f.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Field_DoAll.ID_AttributeType
                }).ToList();

                var dbReaderDoAll = new OntologyModDBConnector(globals);
                if (searchDoAll.Any())
                {
                    result.ResultState = dbReaderDoAll.GetDataObjectAtt(searchDoAll);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                var searchMergeFields = dbReaderFieldParserToField.ObjectRels.Select(f => new clsObjectRel
                {
                    ID_Object = f.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Field_is_MergeField.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Field_is_MergeField.ID_Class_Right
                }).ToList();

                var dbReaderMergeFields = new OntologyModDBConnector(globals);
                if (searchMergeFields.Any())
                {
                    result.ResultState = dbReaderMergeFields.GetDataObjectRel(searchMergeFields);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                var searchMergeFieldsRel = dbReaderMergeFields.ObjectRels.Select(mergedField => new clsObjectRel
                {
                    ID_Object = mergedField.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_MergeField_contains_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_MergeField_contains_Field.ID_Class_Right
                }).ToList();

                searchMergeFieldsRel.AddRange(dbReaderMergeFields.ObjectRels.Select(mergedField => new clsObjectRel
                {
                    ID_Object = mergedField.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_MergeField_uses_Text_Seperators.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_MergeField_uses_Text_Seperators.ID_Class_Right
                }));

                var dbReaderMergeFieldsRef = new OntologyModDBConnector(globals);
                if (searchMergeFieldsRel.Any())
                {
                    result.ResultState = dbReaderMergeFieldsRef.GetDataObjectRel(searchMergeFieldsRel);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                var searchTwoLetterIsos = dbReaderFieldParserToField.ObjectRels.Where(fieldRel => fieldRel.ID_Parent_Other == Config.LocalData.Class_Field.GUID).Select(field => new clsObjectRel
                {
                    ID_Object = field.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Field_is_of_Type_TwoLetterISOLanguageName.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Field_is_of_Type_TwoLetterISOLanguageName.ID_Class_Right
                }).ToList();

                var dbReaderTwoLetterIsos = new OntologyModDBConnector(globals);

                if (searchTwoLetterIsos.Any())
                {
                    result.ResultState = dbReaderTwoLetterIsos.GetDataObjectRel(searchTwoLetterIsos);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Two Letter Isos of CSVFields!";
                        return result;
                    }
                }



                result.Result.FieldList = dbReaderFields.Objects1;
                result.Result.FieldParserToField = dbReaderFieldParserToField.ObjectRels;
                result.Result.ContentFields = dbReaderContentField.ObjectRels;
                result.Result.FieldAtts = dbReaderFieldsAtt.ObjAtts;
                result.Result.FieldRels = dbReaderFieldsRel.ObjectRels;
                result.Result.FilterAtts = dbReaderFilterAtt.ObjAtts;
                result.Result.RegexAtt = dbReaderRegexAtt.ObjAtts;
                result.Result.UserContentFields = dbReaderUserContentField.ObjectRels;
                result.Result.ReplaceWith = dbReaderReplaceWith.ObjectRels;
                result.Result.ContainedFields = dbReaderContainedFields.ObjectRels;
                result.Result.DoAll = dbReaderDoAll.ObjAtts;
                result.Result.MergeFields = dbReaderMergeFields.ObjectRels;
                result.Result.MergeFieldsRef = dbReaderMergeFieldsRef.ObjectRels;
                result.Result.FieldsToTwoLetterIsoNames = dbReaderTwoLetterIsos.ObjectRels;

                return result;
            });

            return taskResult;
        }

        private ResultItem<RegexReplaceRaw> GetRegexReplace()
        {
            var result = new ResultItem<RegexReplaceRaw>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new RegexReplaceRaw()
            };

            var searchReplace = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = Config.LocalData.ClassRel_Field_contains_Field_Replace__TextParser_.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_Field_contains_Field_Replace__TextParser_.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Field_contains_Field_Replace__TextParser_.ID_Class_Right
                }
            };

            var dbReaderRegexReplace = new OntologyModDBConnector(globals);
            result.ResultState = dbReaderRegexReplace.GetDataObjectRel(searchReplace, doIds: false);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var dbReaderRegexReplaceWith = new OntologyModDBConnector(globals);
            var searchText = dbReaderRegexReplace.ObjectRels.Select(rep => new clsObjectAtt
            {
                ID_AttributeType = Config.LocalData.ClassAtt_Field_Replace__TextParser__Text.ID_AttributeType,
                ID_Object = rep.ID_Other
            }).ToList();

            if (searchText.Any())
            {
                result.ResultState = dbReaderRegexReplaceWith.GetDataObjectAtt(searchText, doIds: false);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

            }

            var dbReaderRegexOfReplace = new OntologyModDBConnector(globals);
            var searchRegEx = dbReaderRegexReplace.ObjectRels.Select(rep => new clsObjectRel
            {
                ID_Object = rep.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Field_Replace__TextParser__find_Regular_Expressions.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Field_Replace__TextParser__find_Regular_Expressions.ID_Class_Right
            }).ToList();

            if (searchRegEx.Any())
            {
                result.ResultState = dbReaderRegexOfReplace.GetDataObjectRel(searchRegEx, doIds: false);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

            }

            var dbReaderRegexRegex = new OntologyModDBConnector(globals);
            var searchRegExRegEx = dbReaderRegexOfReplace.ObjectRels.Select(reg => new clsObjectAtt
            {
                ID_Object = reg.ID_Other,
                ID_AttributeType = Config.LocalData.ClassAtt_Regular_Expressions_RegEx.ID_AttributeType
            }).ToList();

            if (searchRegExRegEx.Any())
            {
                result.ResultState = dbReaderRegexRegex.GetDataObjectAtt(searchRegExRegEx, doIds: false);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }


            result.Result.ReplaceFields = dbReaderRegexReplace.ObjectRels.GroupBy(field => field.ID_Object)
                        .Select(field => new clsOntologyItem { GUID = field.Key })
                        .ToList();

            result.Result.ReplaceRel = dbReaderRegexReplace.ObjectRels;
            result.Result.ReplaceWith = dbReaderRegexReplaceWith.ObjAtts;
            result.Result.RegexOfReplace = dbReaderRegexOfReplace.ObjectRels;
            result.Result.RegexRegex = dbReaderRegexRegex.ObjAtts;

            return result;
        }

        public async Task<ResultItem<clsOntologyItem>> GetOItem(string id, string type)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);
                var resultItem = dbReader.GetOItem(id, type);

                if (resultItem.GUID_Related == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    return result;
                };

                result.Result = resultItem;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<TextParserDetails>> GetTextParserDetails(
            List<clsOntologyItem> textParserDetailsOItems)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<TextParserDetails>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TextParserDetails()
                };

                result.Result.TextParserDetailsItems = textParserDetailsOItems;
                var searchAttributes = result.Result.TextParserDetailsItems.Select(oItem => new clsObjectAtt
                {
                    ID_Object = oItem.GUID
                }).ToList();

                var dbReaderAttributes = new OntologyModDBConnector(globals);

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Attributes!";
                        return result;
                    }
                }

                result.Result.TextParserDetailsAttributes = dbReaderAttributes.ObjAtts;

                var searchReportFiels = result.Result.TextParserDetailsItems.Select(oItem => new clsObjectRel()
                {
                    ID_Object = oItem.GUID,
                    ID_RelationType = Relate.Config.LocalData.ClassRel_Textparser_Details_contains_Field.ID_RelationType,
                    ID_Parent_Other = Relate.Config.LocalData.ClassRel_Textparser_Details_contains_Field.ID_Class_Right,
                }).ToList();

                var dbReaderFields = new OntologyModDBConnector(globals);

                if (searchReportFiels.Any())
                {
                    result.ResultState = dbReaderFields.GetDataObjectRel(searchReportFiels);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the report-fields!";
                        return result;
                    }
                }

                result.Result.TextParserDetailsToTextParserFields = dbReaderFields.ObjectRels;

                var searchTextParsers = result.Result.TextParserDetailsItems.Select(oItem => new clsObjectRel()
                {
                    ID_Object = oItem.GUID,
                    ID_RelationType = Relate.Config.LocalData.ClassRel_Textparser_Details_belongs_to_Textparser.ID_RelationType,
                    ID_Parent_Other = Relate.Config.LocalData.ClassRel_Textparser_Details_belongs_to_Textparser.ID_Class_Right,
                }).ToList();

                var dbReaderTextParsers = new OntologyModDBConnector(globals);

                if (searchTextParsers.Any())
                {
                    result.ResultState = dbReaderTextParsers.GetDataObjectRel(searchTextParsers);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the textparsers!";
                        return result;
                    }
                }

                result.Result.TextParserDetailsToTextParsers = dbReaderTextParsers.ObjectRels;


                result.ResultState = EnrichTextParserDetailsWithConfig(result.Result);

                return result;
            });
            return taskResult;
        }

        private clsOntologyItem EnrichTextParserDetailsWithConfig(TextParserDetails details)
        {
            var result = globals.LState_Success.Clone();
            var idTextparsers = details.TextParserDetailsToTextParsers.GroupBy(rel => rel.ID_Other).Select(grp => grp.Key).ToList();

            var searchConfigs = idTextparsers.Select(idTextParser => new clsObjectRel
            {
                ID_Other = idTextParser,
                ID_RelationType = NameTransform.TextparserDetails.Config.LocalData.ClassRel_Textparser_Details_Transform_Config_belongs_to_Textparser.ID_RelationType,
                ID_Parent_Object = NameTransform.TextparserDetails.Config.LocalData.ClassRel_Textparser_Details_Transform_Config_belongs_to_Textparser.ID_Class_Left
            }).ToList();

            var dbReaderConfigs = new OntologyModDBConnector(globals);

            if (searchConfigs.Any())
            {
                result = dbReaderConfigs.GetDataObjectRel(searchConfigs);
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the Configs of Textparser!";
                    return result;
                }
            }

            details.ConfigToTextparser = dbReaderConfigs.ObjectRels;

            var searchTemplates = details.ConfigToTextparser.Select(cnfg => new clsObjectAtt
            {
                ID_Object = cnfg.ID_Object,
                ID_AttributeType = NameTransform.TextparserDetails.Config.LocalData.AttributeType_Name_Template.GUID
            }).ToList();

            var dbReaderTemplates = new OntologyModDBConnector(globals);

            if (searchTemplates.Any())
            {
                result = dbReaderTemplates.GetDataObjectAtt(searchTemplates);
                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the Name-Templates!";
                    return result;
                }
            }

            details.ConfigsToNameTemplate = dbReaderTemplates.ObjAtts;




            return result;
        }

        public async Task<ResultItem<AutoCompleteModel>> GetAutoCompleteModel(string idTextParser)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<AutoCompleteModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new AutoCompleteModel()
                };

                var searchPattern = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idTextParser,
                        ID_RelationType = Config.LocalData.ClassRel_Textparser_contains_Pattern.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Textparser_contains_Pattern.ID_Class_Right
                    }
                };

                var dbReaderTextParserToPattern = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextParserToPattern.GetDataObjectRel(searchPattern);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Errro while getting the Pattern-Objects!";
                    return result;
                }

                result.Result.TextParserToPatterns = dbReaderTextParserToPattern.ObjectRels;

                var searchPatternPattern = result.Result.TextParserToPatterns.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Pattern_Pattern.ID_AttributeType
                }).ToList();

                var dbReaderTextParserPatternPattern = new OntologyModDBConnector(globals);

                if (searchPatternPattern.Any())
                {
                    result.ResultState = dbReaderTextParserPatternPattern.GetDataObjectAtt(searchPatternPattern);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Errro while getting the Pattern!";
                        return result;
                    }
                }

                result.Result.PatternPatterns = dbReaderTextParserPatternPattern.ObjAtts;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MeasureTextparserModel>> GetMeasureTextparserModel(MeasureTextParserRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<MeasureTextparserModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new MeasureTextparserModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderConfig, globals, nameof(MeasureTextparserModel.Config));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchDeleteTextparser = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_AttributeType = Measure.Config.LocalData.ClassAtt_Textparser_Measurement_Delete_Textparser.ID_AttributeType,
                        ID_Object = result.Result.Config.GUID
                    }
                };

                var dbReaderDeleteTextparser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderDeleteTextparser.GetDataObjectAtt(searchDeleteTextparser);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Delete-Textparser attributes!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderDeleteTextparser, globals, nameof(MeasureTextparserModel.DeleteTextparser));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchExecuteTextparser = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_AttributeType = Measure.Config.LocalData.ClassAtt_Textparser_Measurement_Execute_Textparser.ID_AttributeType,
                        ID_Object = result.Result.Config.GUID
                    }
                };

                var dbReaderExecuteTextparser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderExecuteTextparser.GetDataObjectAtt(searchExecuteTextparser);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Delete-Textparser attributes!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderExecuteTextparser, globals, nameof(MeasureTextparserModel.ExecuteTextparser));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchSplitFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Measure.Config.LocalData.ClassRel_Textparser_Measurement_Split_Field.ID_RelationType,
                        ID_Parent_Other = Measure.Config.LocalData.ClassRel_Textparser_Measurement_Split_Field.ID_Class_Right
                    }
                };

                var dbReaderSplitFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSplitFields.GetDataObjectRel(searchSplitFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Split-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderSplitFields, globals, nameof(MeasureTextparserModel.SplitField));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchMeasureFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Measure_Field.ID_RelationType,
                        ID_Parent_Other = Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Measure_Field.ID_Class_Right
                    }
                };

                var dbReaderMeasureFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMeasureFields.GetDataObjectRel(searchMeasureFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Measure-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderMeasureFields, globals, nameof(MeasureTextparserModel.ConfigToMeasureFields));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchMeasureFieldsToCalcFiels = result.Result.ConfigToMeasureFields.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Measure.Config.LocalData.ClassRel_Measure_Field_Calc_Field.ID_RelationType,
                    ID_Parent_Other = Measure.Config.LocalData.ClassRel_Measure_Field_Calc_Field.ID_Class_Right
                }).ToList();

                var dbReaderMeasureFieldsToCalcFiels = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMeasureFieldsToCalcFiels.GetDataObjectRel(searchMeasureFieldsToCalcFiels);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Calc-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderMeasureFieldsToCalcFiels, globals, nameof(MeasureTextparserModel.MeasureFieldsToClacFields));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchMeasureFieldsToDestFields = result.Result.ConfigToMeasureFields.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Measure.Config.LocalData.ClassRel_Measure_Field_dst_Field.ID_RelationType,
                    ID_Parent_Other = Measure.Config.LocalData.ClassRel_Measure_Field_dst_Field.ID_Class_Right
                }).ToList();

                var dbReaderMeasureFieldsToDestFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMeasureFieldsToDestFields.GetDataObjectRel(searchMeasureFieldsToDestFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Dest-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderMeasureFieldsToDestFields, globals, nameof(MeasureTextparserModel.MeasureFieldsToDestFields));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchMeasureFieldsToFieldsWithValues = result.Result.ConfigToMeasureFields.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Measure.Config.LocalData.ClassRel_Measure_Field_Check_Field_with_Value.ID_RelationType,
                    ID_Parent_Other = Measure.Config.LocalData.ClassRel_Measure_Field_Check_Field_with_Value.ID_Class_Right
                }).ToList();

                var dbReaderMeasureFieldsToFieldsWithValues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMeasureFieldsToFieldsWithValues.GetDataObjectRel(searchMeasureFieldsToFieldsWithValues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Fields with Values!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderMeasureFieldsToFieldsWithValues, globals, nameof(MeasureTextparserModel.MeasureFieldsToCheckFieldValues));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                
                var searchMeasureFieldsToMeasureTypes = result.Result.ConfigToMeasureFields.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Measure.Config.LocalData.ClassRel_Measure_Field_is_of_Type_Measure_Type.ID_RelationType,
                    ID_Parent_Other = Measure.Config.LocalData.ClassRel_Measure_Field_is_of_Type_Measure_Type.ID_Class_Right
                }).ToList();

                var dbReaderMeasureFieldsToMeasureTypes = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMeasureFieldsToMeasureTypes.GetDataObjectRel(searchMeasureFieldsToMeasureTypes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Measure-Types!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderMeasureFieldsToMeasureTypes, globals, nameof(MeasureTextparserModel.MeasureFieldsToMeasureTypes));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchFieldWithValuesToFields = result.Result.MeasureFieldsToCheckFieldValues.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Measure.Config.LocalData.ClassRel_Field_with_Value_belonging_Field.ID_RelationType,
                    ID_Parent_Other = Measure.Config.LocalData.ClassRel_Field_with_Value_belonging_Field.ID_Class_Right
                }).ToList();

                var dbReaderFieldWithValuesToFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFieldWithValuesToFields.GetDataObjectRel(searchFieldWithValuesToFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Fields of Fields with Values!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderFieldWithValuesToFields, globals, nameof(MeasureTextparserModel.FieldValuesToFields));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchFieldWithValuesToValues = result.Result.MeasureFieldsToCheckFieldValues.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Measure.Config.LocalData.ClassAtt_Field_with_Value_Bitvalue.ID_AttributeType
                }).ToList();
                searchFieldWithValuesToValues.AddRange(result.Result.MeasureFieldsToCheckFieldValues.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Measure.Config.LocalData.ClassAtt_Field_with_Value_Datetimevalue.ID_AttributeType
                }));
                searchFieldWithValuesToValues.AddRange(result.Result.MeasureFieldsToCheckFieldValues.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Measure.Config.LocalData.ClassAtt_Field_with_Value_Doublevalue.ID_AttributeType
                }));
                searchFieldWithValuesToValues.AddRange(result.Result.MeasureFieldsToCheckFieldValues.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Measure.Config.LocalData.ClassAtt_Field_with_Value_Longvalue.ID_AttributeType
                }));
                searchFieldWithValuesToValues.AddRange(result.Result.MeasureFieldsToCheckFieldValues.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Measure.Config.LocalData.ClassAtt_Field_with_Value_Stringvalue.ID_AttributeType
                }));

                var dbReaderFieldWithValuesToValues = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFieldWithValuesToValues.GetDataObjectAtt(searchFieldWithValuesToValues);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Values of Fields with Values!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderFieldWithValuesToValues, globals, nameof(MeasureTextparserModel.FieldValuesValues));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchTextparserMeasureToSortFieldss = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Sort_Field.ID_RelationType,
                        ID_Parent_Other = Measure.Config.LocalData.ClassRel_Textparser_Measurement_contains_Sort_Field.ID_Class_Right
                    }
                };

                var dbReaderTextparserMeasureToSortFieldss = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextparserMeasureToSortFieldss.GetDataObjectRel(searchTextparserMeasureToSortFieldss);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Sort-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderTextparserMeasureToSortFieldss, globals, nameof(MeasureTextparserModel.ConfigToSortFields));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchSortFieldsToFields = result.Result.ConfigToSortFields.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Measure.Config.LocalData.ClassRel_Sort_Field_belonging_Field.ID_RelationType,
                    ID_Parent_Other = Measure.Config.LocalData.ClassRel_Sort_Field_belonging_Field.ID_Class_Right
                }).ToList();

                var dbReaderSortFieldsToFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSortFieldsToFields.GetDataObjectRel(searchSortFieldsToFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Fields of Sort-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderSortFieldsToFields, globals, nameof(MeasureTextparserModel.SortFieldsToFields));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchSortFieldsToSort = result.Result.ConfigToSortFields.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = Measure.Config.LocalData.ClassAtt_Sort_Field_ASC.ID_AttributeType
                }).ToList();

                var dbReaderSortFieldsToSort = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSortFieldsToSort.GetDataObjectAtt(searchSortFieldsToSort);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Sort of Sort-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderSortFieldsToSort, globals, nameof(MeasureTextparserModel.SortFieldsToSort));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchTextparserMeasureToPattern = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Measure.Config.LocalData.ClassRel_Textparser_Measurement_base_Pattern.ID_RelationType,
                        ID_Parent_Other = Measure.Config.LocalData.ClassRel_Textparser_Measurement_base_Pattern.ID_Class_Right
                    }
                };

                var dbReaderTextparserMeasureToPattern = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextparserMeasureToPattern.GetDataObjectRel(searchTextparserMeasureToPattern);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Pattern!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderTextparserMeasureToPattern, globals, nameof(MeasureTextparserModel.BasePattern));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchPatternToPatternText = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.BasePattern.GUID,
                        ID_AttributeType = Measure.Config.LocalData.ClassAtt_Pattern_Pattern.ID_AttributeType
                    }
                };

                var dbReaderPatternToPatternText = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPatternToPatternText.GetDataObjectAtt(searchPatternToPatternText);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Pattern-Text!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderPatternToPatternText, globals, nameof(MeasureTextparserModel.PatternText));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchTextparserMeasureToTextparser = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = Measure.Config.LocalData.ClassRel_Textparser_Measurement_belonging_Textparser.ID_RelationType,
                        ID_Parent_Other = Measure.Config.LocalData.ClassRel_Textparser_Measurement_belonging_Textparser.ID_Class_Right
                    }
                };

                var dbReaderTextparserMeasureToTextparser = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderTextparserMeasureToTextparser.GetDataObjectRel(searchTextparserMeasureToTextparser);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Pattern!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMeasureTextparserModel(result.Result, dbReaderTextparserMeasureToTextparser, globals, nameof(MeasureTextparserModel.Textparser));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<ReIndexPatternSourceFieldsModel>> GetReIndexPatternSourceFieldsModel(ReIndexPatternSourceFieldsByTextparserRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ReIndexPatternSourceFieldsModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ReIndexPatternSourceFieldsModel()
                };

                var searchPatternSources = request.ParserFields.Select(field => new clsObjectRel
                {
                    ID_Other = field.IdField,
                    ID_RelationType = Config.LocalData.ClassRel_Pattern_Source_belongs_to_Field.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Pattern_Source_belongs_to_Field.ID_Class_Left,
                }).ToList();

                var dbReaderPatternSourcesToFields = new OntologyModDBConnector(globals);

                if (searchPatternSources.Any())
                {
                    result.ResultState = dbReaderPatternSourcesToFields.GetDataObjectRel(searchPatternSources);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Pattern-Sources!";
                        return result;
                    }
                }

                result.ResultState = ValidationController.ValidateAndSetReIndexPatternSourceFieldsModel(request.ParserFields, result.Result, dbReaderPatternSourcesToFields, globals, nameof(ReIndexPatternSourceFieldsModel.PatternSourceFields));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                if (!result.Result.PatternSourceFields.Any())
                {
                    return result;
                }

                var searchPatternSourceToPatern = result.Result.PatternSourcesToFields.Select(ps => new clsObjectRel
                {
                    ID_Object = ps.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Pattern_Source_ParseSource_Pattern.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Pattern_Source_ParseSource_Pattern.ID_Class_Right
                }).ToList();

                var dbReaderPatternSourceToPattern = new OntologyModDBConnector(globals);

                if (searchPatternSourceToPatern.Any())
                {
                    result.ResultState = dbReaderPatternSourceToPattern.GetDataObjectRel(searchPatternSourceToPatern);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Pattern-Items of Pattern-Sources!";
                        return result;
                    }
                }

                result.ResultState = ValidationController.ValidateAndSetReIndexPatternSourceFieldsModel(request.ParserFields, result.Result, dbReaderPatternSourceToPattern, globals, nameof(ReIndexPatternSourceFieldsModel.PatternSourceToPattern));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPatternPattern = result.Result.PatternSourceToPattern.Select(patSourceToPat => new clsObjectAtt
                {
                    ID_Object = patSourceToPat.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Pattern_Pattern.ID_AttributeType
                }).ToList();

                var dbReaderPatternPattern = new OntologyModDBConnector(globals);

                if (searchPatternPattern.Any())
                {
                    result.ResultState = dbReaderPatternPattern.GetDataObjectAtt(searchPatternPattern);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Patterns of Pattern-Items!";
                        return result;
                    }
                }

                result.ResultState = ValidationController.ValidateAndSetReIndexPatternSourceFieldsModel(request.ParserFields, result.Result, dbReaderPatternPattern, globals, nameof(ReIndexPatternSourceFieldsModel.PatternPattern));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPatternSourcesValues = result.Result.PatternSourcesToFields.Select(patSourceToField => new clsObjectAtt
                {
                    ID_Object = patSourceToField.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_Documentitem_Bitvalue.ID_AttributeType
                }).ToList();

                searchPatternSourcesValues.AddRange(result.Result.PatternSourcesToFields.Select(patSourceToField => new clsObjectAtt
                {
                    ID_Object = patSourceToField.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_Documentitem_Datetimevalue.ID_AttributeType
                }));

                searchPatternSourcesValues.AddRange(result.Result.PatternSourcesToFields.Select(patSourceToField => new clsObjectAtt
                {
                    ID_Object = patSourceToField.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_Documentitem_Doublevalue.ID_AttributeType
                }));

                searchPatternSourcesValues.AddRange(result.Result.PatternSourcesToFields.Select(patSourceToField => new clsObjectAtt
                {
                    ID_Object = patSourceToField.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_Documentitem_Longvalue.ID_AttributeType
                }));

                searchPatternSourcesValues.AddRange(result.Result.PatternSourcesToFields.Select(patSourceToField => new clsObjectAtt
                {
                    ID_Object = patSourceToField.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_Documentitem_Stringvalue.ID_AttributeType
                }));

                var dbReaderPatternSourcesValues = new OntologyModDBConnector(globals);

                if (searchPatternSourcesValues.Any())
                {
                    result.ResultState = dbReaderPatternSourcesValues.GetDataObjectAtt(searchPatternSourcesValues);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Field-Values to be set!";
                        return result;
                    }
                }

                result.ResultState = ValidationController.ValidateAndSetReIndexPatternSourceFieldsModel(request.ParserFields, result.Result, dbReaderPatternSourcesValues, globals, nameof(ReIndexPatternSourceFieldsModel.PatternSourceValues));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {

        }
    }

    public class ParserFieldsRaw
    {
        public List<clsOntologyItem> FieldList { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> FieldParserToField { get; set; }
        public List<clsObjectAtt> FieldAtts { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> FieldRels { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> FilterAtts { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> ContentFields { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> UserContentFields { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ReplaceWith { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> RegexAtt { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> MergeFields { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> MergeFieldsRef { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ContainedFields { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> DoAll { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> FieldsToTwoLetterIsoNames { get; set; } = new List<clsObjectRel>();
        public RegexReplaceRaw RegexReplaceRaw { get; set; }
    }

    public class RegexReplaceRaw
    {
        public List<clsOntologyItem> ReplaceFields { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ReplaceRel { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> ReplaceWith { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> RegexOfReplace { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> RegexRegex { get; set; } = new List<clsObjectAtt>();
    }

    public class TextParserRaw
    {
        public List<clsOntologyItem> TextParserList { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> TextParserLeftRight { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigurationItems { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SubParsers { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> CommandLineRun { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> TextParsersAtt { get; set; } = new List<clsObjectAtt>();
    }
}
