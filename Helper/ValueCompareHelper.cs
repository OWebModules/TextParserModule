﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserModule.Helper
{
    public static class ValueCompareHelper
    {
        public static bool Compare(object value1, object value2)
        {
            var result = true;

            if (value1.GetType() != value2.GetType()) return false;
            if (value1 is bool)
            {
                return (bool)value1 == (bool)value2;
            }
            if (value1 is DateTime)
            {
                return (DateTime)value1 == (DateTime)value2;
            }
            if (value1 is double)
            {
                return (double)value1 == (double)value2;
            }
            if (value1 is long)
            {
                return (long)value1 == (long)value2;
            }
            return (string)value1 == (string)value2;

            return result;
        }
    }
}
